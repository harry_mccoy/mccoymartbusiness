package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created on 22-05-2019.
 */
public class ProductInfoResponseBean implements Serializable {
    /**
     * status : true
     * message : Success.
     * statusCode : 200
     * data : {"objAttributes":[{"id":4,"category_id":23,"name":"Size","slug":"size-in-height","type":"text","attributes":[{"type":"text","name":"Height/Length  (mm)","value":[]},{"type":"text","name":"Width (mm)","value":"55mm"}]},{"id":13,"category_id":23,"name":"Door Open Style","slug":"door-open-style","type":"checkbox","attributes":[{"type":"checkbox","name":"Sliding Door","value":[]},{"type":"checkbox","name":"Casement Door","value":[]},{"type":"checkbox","name":"Slide & Fold Door","value":[]},{"type":"checkbox","name":"Lift & Slide Door","value":[]},{"type":"checkbox","name":"Tilt & Slide Door","value":[]},{"type":"checkbox","name":"Swing","value":[]},{"type":"checkbox","name":"Rolling","value":[]},{"type":"checkbox","name":"Automatic","value":[]},{"type":"checkbox","name":"Folding","value":[]},{"type":"checkbox","name":"Other","value":[]}]},{"id":14,"category_id":23,"name":"Door Application","slug":"door-application","type":"checkbox","attributes":[{"type":"checkbox","name":"Balcony","value":[]},{"type":"checkbox","name":"Internal","value":[]},{"type":"checkbox","name":"Exterior","value":[]},{"type":"checkbox","name":"Garden","value":[]},{"type":"checkbox","name":"Room Partition","value":[]},{"type":"checkbox","name":"Main Door","value":[]},{"type":"checkbox","name":"Internal Door","value":[]},{"type":"checkbox","name":"Bathroom Door","value":[]},{"type":"checkbox","name":"Entry Door","value":[]},{"type":"checkbox","name":"Security Door","value":[]},{"type":"checkbox","name":"Other","value":[]}]},{"id":19,"category_id":23,"name":"Color","slug":"color-1","type":"text","attributes":[{"type":"text","name":"Colour","value":[]}]},{"id":20,"category_id":23,"name":"Brand","slug":"brand","type":"text","attributes":[{"type":"text","name":"Brand","value":[]}]},{"id":27,"category_id":23,"name":"Warranty","slug":"warranty","type":"text","attributes":[{"type":"text","name":"Warranty","value":[]}]},{"id":37,"category_id":23,"name":"Thickness","slug":"thickness","type":"text","attributes":[{"type":"text","name":"Thickness","value":[]}]},{"id":40,"category_id":23,"name":"Material","slug":"material","type":"text","attributes":[{"type":"text","name":"Material","value":[]}]},{"id":41,"category_id":23,"name":"Finishing","slug":"finishing","type":"text","attributes":[{"type":"text","name":"Finishing","value":[]}]},{"id":42,"category_id":23,"name":"Water Resistance","slug":"water-resistance","type":"text","attributes":[{"type":"text","name":"Water Resistance","value":[]}]},{"id":43,"category_id":23,"name":"With Lock","slug":"with-lock","type":"text","attributes":[{"type":"text","name":"With Lock","value":[]}]},{"id":2,"category_id":22,"name":"Frame Color","slug":"color","type":"text","attributes":[{"type":"text","name":"Frame Color","value":[]}]},{"id":3,"category_id":22,"name":"Profile Brand","slug":"profile-brand","type":"checkbox","attributes":[{"type":"checkbox","name":"Aluplast","value":[]},{"type":"checkbox","name":"Kommerling","value":[]},{"type":"checkbox","name":"Veka","value":[]},{"type":"checkbox","name":"Rehau","value":[]},{"type":"checkbox","name":"Encraft","value":[]},{"type":"checkbox","name":"Okotech","value":[]},{"type":"checkbox","name":"LG Hausys","value":[]},{"type":"checkbox","name":"Deceuninck","value":[]},{"type":"checkbox","name":"Lesso","value":[]},{"type":"checkbox","name":"Simta","value":[]},{"type":"checkbox","name":"Prominance","value":[]},{"type":"checkbox","name":"Pluga","value":[]},{"type":"checkbox","name":"Kaka","value":[]},{"type":"checkbox","name":"Other","value":[]}]},{"id":4,"category_id":22,"name":"Size","slug":"size-in-height","type":"text","attributes":[{"type":"text","name":"Height/Length  (mm)","value":[]},{"type":"text","name":"Width (mm)","value":"55mm"}]},{"id":6,"category_id":22,"name":"Profile Thickness","slug":"profile-thickness","type":"text","attributes":[{"type":"text","name":"Profile Thickness (mm)","value":""}]},{"id":8,"category_id":22,"name":"Frame Material","slug":"frame-material","type":"radio","attributes":[{"type":"radio","name":"uPVC","value":[]},{"type":"radio","name":"Aluminium","value":[]},{"type":"radio","name":"Wooden","value":[]},{"type":"radio","name":"WPC","value":[]},{"type":"radio","name":"Steel","value":[]},{"type":"radio","name":"Other","value":[]}]}],"objProductData":{"product_id":9640,"company_id":14373,"product_name":"Mccoy Product22","product_slug":"mccoy-product22","price":"","model":"","sku":"","description":"","quantity":0,"weight":"","metric":""},"ObjMetric":[{"id":24,"key":"sq_foot","value":"Sq. Foot","status":0},{"id":25,"key":"sq_meter","value":"Sq. Meter","status":0},{"id":26,"key":"foot","value":"Foot","status":0},{"id":27,"key":"meter","value":"Meter","status":0},{"id":28,"key":"pieces","value":"Pieces","status":0},{"id":29,"key":"boxes","value":"Boxes","status":0},{"id":38,"key":"kilogram","value":"Kilogram","status":0},{"id":45,"key":"unit","value":"Unit","status":0},{"id":46,"key":"set","value":"Set","status":0},{"id":47,"key":"ton","value":"Ton","status":0},{"id":50,"key":"KW","value":"KW (Kilowatt)","status":0},{"id":93,"key":"watt","value":"Watt","status":0}]}
     */

    private boolean status;
    private String message;
    private int statusCode;


    @SerializedName("data")
    private List<ProductDataDashboardShopBean> productData;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<ProductDataDashboardShopBean> getProductData() {
        return productData;
    }

    public void setProductData(List<ProductDataDashboardShopBean> productData) {
        this.productData = productData;
    }


    public static class DataBean {
        /**
         * objAttributes : [{"id":4,"category_id":23,"name":"Size","slug":"size-in-height","type":"text","attributes":[{"type":"text","name":"Height/Length  (mm)","value":[]},{"type":"text","name":"Width (mm)","value":"55mm"}]},{"id":13,"category_id":23,"name":"Door Open Style","slug":"door-open-style","type":"checkbox","attributes":[{"type":"checkbox","name":"Sliding Door","value":[]},{"type":"checkbox","name":"Casement Door","value":[]},{"type":"checkbox","name":"Slide & Fold Door","value":[]},{"type":"checkbox","name":"Lift & Slide Door","value":[]},{"type":"checkbox","name":"Tilt & Slide Door","value":[]},{"type":"checkbox","name":"Swing","value":[]},{"type":"checkbox","name":"Rolling","value":[]},{"type":"checkbox","name":"Automatic","value":[]},{"type":"checkbox","name":"Folding","value":[]},{"type":"checkbox","name":"Other","value":[]}]},{"id":14,"category_id":23,"name":"Door Application","slug":"door-application","type":"checkbox","attributes":[{"type":"checkbox","name":"Balcony","value":[]},{"type":"checkbox","name":"Internal","value":[]},{"type":"checkbox","name":"Exterior","value":[]},{"type":"checkbox","name":"Garden","value":[]},{"type":"checkbox","name":"Room Partition","value":[]},{"type":"checkbox","name":"Main Door","value":[]},{"type":"checkbox","name":"Internal Door","value":[]},{"type":"checkbox","name":"Bathroom Door","value":[]},{"type":"checkbox","name":"Entry Door","value":[]},{"type":"checkbox","name":"Security Door","value":[]},{"type":"checkbox","name":"Other","value":[]}]},{"id":19,"category_id":23,"name":"Color","slug":"color-1","type":"text","attributes":[{"type":"text","name":"Colour","value":[]}]},{"id":20,"category_id":23,"name":"Brand","slug":"brand","type":"text","attributes":[{"type":"text","name":"Brand","value":[]}]},{"id":27,"category_id":23,"name":"Warranty","slug":"warranty","type":"text","attributes":[{"type":"text","name":"Warranty","value":[]}]},{"id":37,"category_id":23,"name":"Thickness","slug":"thickness","type":"text","attributes":[{"type":"text","name":"Thickness","value":[]}]},{"id":40,"category_id":23,"name":"Material","slug":"material","type":"text","attributes":[{"type":"text","name":"Material","value":[]}]},{"id":41,"category_id":23,"name":"Finishing","slug":"finishing","type":"text","attributes":[{"type":"text","name":"Finishing","value":[]}]},{"id":42,"category_id":23,"name":"Water Resistance","slug":"water-resistance","type":"text","attributes":[{"type":"text","name":"Water Resistance","value":[]}]},{"id":43,"category_id":23,"name":"With Lock","slug":"with-lock","type":"text","attributes":[{"type":"text","name":"With Lock","value":[]}]},{"id":2,"category_id":22,"name":"Frame Color","slug":"color","type":"text","attributes":[{"type":"text","name":"Frame Color","value":[]}]},{"id":3,"category_id":22,"name":"Profile Brand","slug":"profile-brand","type":"checkbox","attributes":[{"type":"checkbox","name":"Aluplast","value":[]},{"type":"checkbox","name":"Kommerling","value":[]},{"type":"checkbox","name":"Veka","value":[]},{"type":"checkbox","name":"Rehau","value":[]},{"type":"checkbox","name":"Encraft","value":[]},{"type":"checkbox","name":"Okotech","value":[]},{"type":"checkbox","name":"LG Hausys","value":[]},{"type":"checkbox","name":"Deceuninck","value":[]},{"type":"checkbox","name":"Lesso","value":[]},{"type":"checkbox","name":"Simta","value":[]},{"type":"checkbox","name":"Prominance","value":[]},{"type":"checkbox","name":"Pluga","value":[]},{"type":"checkbox","name":"Kaka","value":[]},{"type":"checkbox","name":"Other","value":[]}]},{"id":4,"category_id":22,"name":"Size","slug":"size-in-height","type":"text","attributes":[{"type":"text","name":"Height/Length  (mm)","value":[]},{"type":"text","name":"Width (mm)","value":"55mm"}]},{"id":6,"category_id":22,"name":"Profile Thickness","slug":"profile-thickness","type":"text","attributes":[{"type":"text","name":"Profile Thickness (mm)","value":""}]},{"id":8,"category_id":22,"name":"Frame Material","slug":"frame-material","type":"radio","attributes":[{"type":"radio","name":"uPVC","value":[]},{"type":"radio","name":"Aluminium","value":[]},{"type":"radio","name":"Wooden","value":[]},{"type":"radio","name":"WPC","value":[]},{"type":"radio","name":"Steel","value":[]},{"type":"radio","name":"Other","value":[]}]}]
         * objProductData : {"product_id":9640,"company_id":14373,"product_name":"Mccoy Product22","product_slug":"mccoy-product22","price":"","model":"","sku":"","description":"","quantity":0,"weight":"","metric":""}
         * ObjMetric : [{"id":24,"key":"sq_foot","value":"Sq. Foot","status":0},{"id":25,"key":"sq_meter","value":"Sq. Meter","status":0},{"id":26,"key":"foot","value":"Foot","status":0},{"id":27,"key":"meter","value":"Meter","status":0},{"id":28,"key":"pieces","value":"Pieces","status":0},{"id":29,"key":"boxes","value":"Boxes","status":0},{"id":38,"key":"kilogram","value":"Kilogram","status":0},{"id":45,"key":"unit","value":"Unit","status":0},{"id":46,"key":"set","value":"Set","status":0},{"id":47,"key":"ton","value":"Ton","status":0},{"id":50,"key":"KW","value":"KW (Kilowatt)","status":0},{"id":93,"key":"watt","value":"Watt","status":0}]
         */

        private ObjProductDataBean objProductData;
        private List<ObjAttributesBean> objAttributes;
        private List<ObjMetricBean> ObjMetric;
        private List<ObjImagesBean> objImages;

        public ObjProductDataBean getObjProductData() {
            return objProductData;
        }

        public void setObjProductData(ObjProductDataBean objProductData) {
            this.objProductData = objProductData;
        }

        public List<ObjAttributesBean> getObjAttributes() {
            return objAttributes;
        }

        public void setObjAttributes(List<ObjAttributesBean> objAttributes) {
            this.objAttributes = objAttributes;
        }

        public List<ObjMetricBean> getObjMetric() {
            return ObjMetric;
        }

        public void setObjMetric(List<ObjMetricBean> ObjMetric) {
            this.ObjMetric = ObjMetric;
        }

        public List<ObjImagesBean> getObjImages() {
            return objImages;
        }

        public void setObjImages(List<ObjImagesBean> objImages) {
            this.objImages = objImages;
        }

        public static class ObjProductDataBean {
            /**
             * product_id : 9640
             * company_id : 14373
             * product_name : Mccoy Product22
             * product_slug : mccoy-product22
             * price :
             * model :
             * sku :
             * description :
             * quantity : 0
             * weight :
             * metric :
             */

            private int product_id;
            private int company_id;
            private String product_name;
            private String product_slug;
            private String price;
            private String model;
            private String sku;
            private String description;
            private int quantity;
            private String weight;
            private String metric;

            public int getProduct_id() {
                return product_id;
            }

            public void setProduct_id(int product_id) {
                this.product_id = product_id;
            }

            public int getCompany_id() {
                return company_id;
            }

            public void setCompany_id(int company_id) {
                this.company_id = company_id;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getProduct_slug() {
                return product_slug;
            }

            public void setProduct_slug(String product_slug) {
                this.product_slug = product_slug;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public String getWeight() {
                return weight;
            }

            public void setWeight(String weight) {
                this.weight = weight;
            }

            public String getMetric() {
                return metric;
            }

            public void setMetric(String metric) {
                this.metric = metric;
            }
        }

        public static class ObjAttributesBean {
            /**
             * id : 4
             * category_id : 23
             * name : Size
             * slug : size-in-height
             * type : text
             * attributes : [{"type":"text","name":"Height/Length  (mm)","value":[]},{"type":"text","name":"Width (mm)","value":"55mm"}]
             */

            private int id;
            private int category_id;
            private String name;
            private String slug;
            private String type;
            private List<AttributesBean> attributes;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCategory_id() {
                return category_id;
            }

            public void setCategory_id(int category_id) {
                this.category_id = category_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public List<AttributesBean> getAttributes() {
                return attributes;
            }

            public void setAttributes(List<AttributesBean> attributes) {
                this.attributes = attributes;
            }

            public static class AttributesBean {
                /**
                 * type : text
                 * name : Height/Length  (mm)
                 * value : []
                 */

                private String type;
                private String name;
                private Object value;
                private boolean isChecked;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public Object getValue() {
                    return value;
                }

                public void setValue(Object value) {
                    this.value = value;
                }

                public boolean isChecked() {
                    return isChecked;
                }

                public void setChecked(boolean checked) {
                    isChecked = checked;
                }
            }
        }

        public static class ObjMetricBean {
            /**
             * id : 24
             * key : sq_foot
             * value : Sq. Foot
             * status : 0
             */

            private int id;
            private String key;
            private String value;
            private int status;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }

        public static class ObjImagesBean {
            /**
             * id : 24
             * key : sq_foot
             * value : Sq. Foot
             * status : 0
             */

            private int product_id;
            private int product_image_id;
            private String image_path;
            private int default_image;
            private int status;


            public int getProduct_id() {
                return product_id;
            }

            public void setProduct_id(int product_id) {
                this.product_id = product_id;
            }

            public int getProduct_image_id() {
                return product_image_id;
            }

            public void setProduct_image_id(int product_image_id) {
                this.product_image_id = product_image_id;
            }

            public String getImage_path() {
                return image_path;
            }

            public void setImage_path(String image_path) {
                this.image_path = image_path;
            }

            public int getDefault_image() {
                return default_image;
            }

            public void setDefault_image(int default_image) {
                this.default_image = default_image;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }
    }
}
