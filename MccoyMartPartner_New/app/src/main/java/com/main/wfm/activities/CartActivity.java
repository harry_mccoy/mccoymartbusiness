package com.main.wfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.CartListAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apiresponsemodel.CartListDataResponse;
import com.main.wfm.apiresponsemodel.CartListResponse;
import com.main.wfm.apiresponsemodel.CartTotalResponse;
import com.main.wfm.models.AddressListItembean;
import com.main.wfm.utils.AppConstants;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity {

    private AppSession appSession;
    private CartListAdapter adapter;
    private List<CartListDataResponse> cartListData;
    private List<CartListDataResponse> cartTotalList;
    private ListView rvCartList;
    private LinearLayout llTotal,llAddress;
    private AddressListItembean default_address;
    public static AddressListItembean shippingAddress;
    public static AddressListItembean billingAddress;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        getIds();

    }

    private void getIds() {
        rvCartList = findViewById(R.id.rvCartList);

        //Adding Header To List Vie;w
        LayoutInflater inflaterHeader = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflaterHeader.inflate(R.layout.cart_header, rvCartList, false);
        rvCartList.addHeaderView(header);
        llAddress=header.findViewById(R.id.llAddress);
        //Adding Footer To List Vie;w
        LayoutInflater inflaterFooter = getLayoutInflater();
        ViewGroup footer = (ViewGroup) inflaterFooter.inflate(R.layout.cart_footer, rvCartList, false);
        rvCartList.addFooterView(footer);

        appSession = new AppSession(CartActivity.this);
        ((ImageView) findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (appSession.getCartCount() > 0) {
            ((TextView) findViewById(R.id.tvCartCount)).setText("" + appSession.getCartCount());
            ((TextView) findViewById(R.id.tvCartCountValue)).setText(appSession.getCartCount()+" Items in your bag");
        }


        ((TextView)header.findViewById(R.id.tvChangeAddress)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartActivity.this, ManageAddressActivity.class);
                intent.putExtra("addressType","changeAddress");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        ((TextView)findViewById(R.id.tvCartContinue)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(default_address==null){
                    if(shippingAddress!=null&&billingAddress!=null){
                        Intent intent = new Intent(CartActivity.this, PaymentActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }else{
                        Intent intent = new Intent(CartActivity.this, ManageAddressActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("addressType","changeAddress");
                        startActivity(intent);

                }


                }
//                Intent intent = new Intent(CartActivity.this, PaymentActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);

            }
        });

        cartListData =new ArrayList<CartListDataResponse>();
        adapter=new CartListAdapter(CartActivity.this, cartListData);
        rvCartList.setAdapter(adapter);


        getCartListApi();
        llTotal=footer.findViewById(R.id.llTotal);
    }

    public void getCartListApi() {
        final ViewDialog viewDialog = new ViewDialog(CartActivity.this);
        viewDialog.showDialog();

        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setSource("2");
        request.setDevice_id(appSession.getDeviceId());
        NetworkAPI.getCartListApi(CartActivity.this, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<CartListResponse>() {
                    @Override
                    public void apiSuccess(CartListResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getCartData().getCart_list() != null && body.getCartData().getCart_list().size() > 0) {
                                cartListData = body.getCartData().getCart_list();
                                ((LinearLayout) findViewById(R.id.llEmptyCartFrame)).setVisibility(View.GONE);
                                rvCartList.setVisibility(View.VISIBLE);
                                ((TextView) findViewById(R.id.tvCartCount)).setText(""+cartListData.size());
                                ((TextView) findViewById(R.id.tvCartCountValue)).setText(cartListData.size()+" Items in your bag");
                                appSession.setCartCount(cartListData.size());

                                adapter=new CartListAdapter(CartActivity.this, cartListData);
                                rvCartList.setAdapter(adapter);


                                getCartTotalApi();
                            } else {
                                ((LinearLayout) findViewById(R.id.llEmptyCartFrame)).setVisibility(View.VISIBLE);
                                rvCartList.setVisibility(View.GONE);
                            }
                            if(body.getCartData().getDefault_address()!=null&&shippingAddress!=null||billingAddress!=null){
                                llAddress.setVisibility(View.VISIBLE);
                                default_address=body.getCartData().getDefault_address();
                            }else{
                                llAddress.setVisibility(View.GONE);
                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(CartActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    public void getCartTotalApi() {
        final ViewDialog viewDialog = new ViewDialog(CartActivity.this);
        viewDialog.showDialog();

        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setSource("2");
        request.setDevice_id(appSession.getDeviceId());
        NetworkAPI.getCartTotalApi(CartActivity.this, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<CartTotalResponse>() {
                    @Override
                    public void apiSuccess(CartTotalResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getCartData().getCart_total()!= null && body.getCartData().getCart_total().size() > 0) {
                                cartTotalList = body.getCartData().getCart_total();
                               setCartTotalList();
                            } else {
                                ((LinearLayout) findViewById(R.id.llEmptyCartFrame)).setVisibility(View.VISIBLE);
                                rvCartList.setVisibility(View.GONE);
                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(CartActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    private void setCartTotalList() {

        for(int i=0;i<cartTotalList.size();i++){
            FrameLayout llPersonal = new FrameLayout(this);
            //llPersonal.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams paramsPersonalValue = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            paramsPersonalValue.setMargins(10 , 10, 10, 10);
            llPersonal.setLayoutParams(paramsPersonalValue);

            TextView tvName = new TextView(this);
            tvName.setText(cartTotalList.get(i).getTitle());
            tvName.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvName.setTextColor(getResources().getColor(R.color.text_color));
            tvName.setGravity(Gravity.LEFT|Gravity.CENTER);
            tvName.setPadding(20, 20, 20, 20);
            tvName.setTypeface(CommonUtils.setCustomFont(CartActivity.this, AppConstants.REGULAR_FONT));
            tvName.setTextSize(12);
            tvName.setId(i);


            TextView tvValue = new TextView(this);
            tvValue.setText(cartTotalList.get(i).getValue());
            tvValue.setTextColor(getResources().getColor(R.color.text_color));
            //tvValue.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvValue.setGravity(Gravity.RIGHT|Gravity.CENTER);
            tvValue.setTypeface(CommonUtils.setCustomFont(CartActivity.this, AppConstants.REGULAR_FONT));
            tvValue.setPadding(20, 20, 20, 20);
            tvValue.setTextSize(12);
            tvValue.setId(i);


            if(cartTotalList.get(i).getCode().equalsIgnoreCase("total")){
                View view=new View(this);
                view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2));
                view.setBackgroundColor(getResources().getColor(R.color.grey));
                tvValue.setTextSize(14);
                tvName.setTextSize(14);
                tvValue.setTypeface(CommonUtils.setCustomFont(CartActivity.this, AppConstants.BOLD_FONT));
                tvName.setTypeface(CommonUtils.setCustomFont(CartActivity.this, AppConstants.BOLD_FONT));
                llPersonal.addView(view);
            }

            llPersonal.addView(tvName);
            llPersonal.addView(tvValue);
            llTotal.addView(llPersonal);
        }
    }

}
