package com.main.wfm.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.main.wfm.R;
import com.main.wfm.apiresponsemodel.ProductImagesResponse;

import java.util.List;

public class ThumbImageAdapter extends RecyclerView.Adapter<ThumbImageAdapter.MyViewHolder> {
    private List<ProductImagesResponse> productImages;
    private Context mContext;
    private ImageView ivProductImge;
    public ThumbImageAdapter(Context mContext, List<ProductImagesResponse> productOptionValues, ImageView ivProductImage) {
        this.productImages = productOptionValues;
        this.mContext=mContext;
        this.ivProductImge=ivProductImage;
    }

    @Override
    public ThumbImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_thumb, parent, false);
        ThumbImageAdapter.MyViewHolder holder = new ThumbImageAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ThumbImageAdapter.MyViewHolder holder, final int position) {
        Glide.with(mContext)
                .load(productImages.get(position).getImage())
                .into(holder.ivThumb);
        holder.ivThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(mContext)
                        .load(productImages.get(position).getImage())
                        .into(ivProductImge);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productImages.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivThumb;

        public MyViewHolder(View v) {
            super(v);
            ivThumb = v.findViewById(R.id.ivThumb);
        }
    }
}