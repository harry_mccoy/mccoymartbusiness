package com.main.wfm.retrofitApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Asad on 27/08/2018.
 */
    public class ApiErrorExecutor {
        //private static Retrofit retrofit;
        private static String baseUrl = RequestUrl.BASE_URL;
        private static Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create());
        public static Retrofit retrofit = builder.build();
        // private static Retrofit retrofit = builder.build();
        // retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        final OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder()
                        .readTimeout(60, TimeUnit.SECONDS)
                        .connectTimeout(60, TimeUnit.SECONDS);
        public static <S> S createService (
                Class< S > serviceClass) {
            return retrofit.create(serviceClass);
        }
    }
