package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class CompanyDataBean {
    @SerializedName("id")
    private int companyId;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("slug")
    private String companySlug;
    @SerializedName("status")
    private String companyStatus;
    @SerializedName("company_logo")
    private String companyLogo;
    @SerializedName("is_completed")
    private int isCompleted;
    @SerializedName("business_type")
    private String businessType;


    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanySlug() {
        return companySlug;
    }

    public void setCompanySlug(String companySlug) {
        this.companySlug = companySlug;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public int getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(int isCompleted) {
        this.isCompleted = isCompleted;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }
}
