package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashBoardShopRequestBean {
    @SerializedName("source")
    private String source;
    @SerializedName("module_type")
    private List<String> modulesTypeList;



    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }



    public List<String> getModulesTypeList() {
        return modulesTypeList;
    }

    public void setModulesTypeList(List<String> modulesTypeList) {
        this.modulesTypeList = modulesTypeList;
    }



}
