package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class GetGraphDetailsRequestBean {
    @SerializedName("company_id")
    private String companyId;
    @SerializedName("year")
    private String year;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
