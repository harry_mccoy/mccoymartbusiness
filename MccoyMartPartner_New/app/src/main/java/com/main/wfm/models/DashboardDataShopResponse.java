package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashboardDataShopResponse {
    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;


    @SerializedName("productData")
    private List<ProductDataDashboardShopBean> productData;



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public List<ProductDataDashboardShopBean> getProductData() {
        return productData;
    }

    public void setProductData(List<ProductDataDashboardShopBean> productData) {
        this.productData = productData;
    }
}
