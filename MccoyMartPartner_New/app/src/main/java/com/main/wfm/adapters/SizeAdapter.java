package com.main.wfm.adapters;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.main.wfm.R;
import com.main.wfm.apiresponsemodel.ProductDetailOptionValueResponse;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.MyViewHolder> {
    private List<ProductDetailOptionValueResponse> productOptionValues;
    private Context mContext;
    public SizeAdapter(Context mContext, List<ProductDetailOptionValueResponse> productOptionValues) {
        this.productOptionValues = productOptionValues;
        this.mContext=mContext;
    }

    @Override
    public SizeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_size, parent, false);
        SizeAdapter.MyViewHolder holder = new SizeAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final SizeAdapter.MyViewHolder holder, int position) {
        holder.tvSize.setText(productOptionValues.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return productOptionValues.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvSize;


        public MyViewHolder(View v) {
            super(v);
            tvSize = (TextView) v.findViewById(R.id.tvSize);

        }
    }
}