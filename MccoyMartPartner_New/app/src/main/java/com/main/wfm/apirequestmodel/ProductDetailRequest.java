package com.main.wfm.apirequestmodel;

import com.google.gson.annotations.SerializedName;

public class ProductDetailRequest {

    @SerializedName("source")
    private String source;

    @SerializedName("product_id")
    private String product_id;
    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
