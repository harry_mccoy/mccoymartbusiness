package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCategoryParentInfoDataBean {

    @SerializedName("categories")
    private List<CategoryParentDataBean> categoryParentDataBeanList;

    @SerializedName("registered")
    private List<CategoryChildServerBean> selectedChildDataBeanList;

    public List<CategoryParentDataBean> getCategoryParentDataBeanList() {
        return categoryParentDataBeanList;
    }

    public void setCategoryParentDataBeanList(List<CategoryParentDataBean> categoryParentDataBeanList) {
        this.categoryParentDataBeanList = categoryParentDataBeanList;
    }

    public List<CategoryChildServerBean> getSelectedChildDataBeanList() {
        return selectedChildDataBeanList;
    }

    public void setSelectedChildDataBeanList(List<CategoryChildServerBean> selectedChildDataBeanList) {
        this.selectedChildDataBeanList = selectedChildDataBeanList;
    }
}
