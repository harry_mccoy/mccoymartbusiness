package com.main.wfm.adapters;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.main.wfm.R;
import com.main.wfm.apiresponsemodel.ProductDetailRelatedProductResponse;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RelatedProductAdapter extends RecyclerView.Adapter<RelatedProductAdapter.MyViewHolder> {
    private List<ProductDetailRelatedProductResponse> relatedProduct;
    private Context mContext;
    public RelatedProductAdapter(Context mContext, List<ProductDetailRelatedProductResponse> relatedProduct) {
        this.relatedProduct = relatedProduct;
        this.mContext=mContext;
    }

    @Override
    public RelatedProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lv_todaysdeal_row, parent, false);
        RelatedProductAdapter.MyViewHolder holder = new RelatedProductAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RelatedProductAdapter.MyViewHolder holder, int position) {

            holder.tvBrandName.setVisibility(View.VISIBLE);

            if(relatedProduct.get(position).getPrice()!=null&&relatedProduct.get(position).getSpecial()!=null){
                holder.tvNewPrice.setVisibility(View.VISIBLE);
                holder.tvOldPrice.setVisibility(View.VISIBLE);
                holder.tvOldPrice.setText("Rs. "+relatedProduct.get(position).getPrice());
                holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.tvNewPrice.setText("Rs. "+relatedProduct.get(position).getSpecial());
            }else if(relatedProduct.get(position).getPrice()!=null){
                holder.tvOldPrice.setVisibility(View.VISIBLE);
                holder.tvOldPrice.setText("Rs. "+relatedProduct.get(position).getPrice());

            }else if(relatedProduct.get(position).getSpecial()!=null){
                holder.tvNewPrice.setVisibility(View.VISIBLE);
                holder.tvNewPrice.setText("Rs. "+relatedProduct.get(position).getSpecial());
            }
            else {
                holder.tvNewPrice.setVisibility(View.GONE);
                holder.tvOldPrice.setVisibility(View.GONE);
            }

            holder.tvname.setText(relatedProduct.get(position).getName());
            holder.tvBrandName.setText(relatedProduct.get(position).getManufacturer());


        //holder.iv.setImageResource(imageModelArrayList.get(position).getImage_drawable());

        Picasso.with(mContext).load(relatedProduct.get(position).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.product_image)).resize(15,150).into(holder.iv);


    }

    @Override
    public int getItemCount() {
        return relatedProduct.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvname,tvBrandName,tvOldPrice,tvNewPrice;
        public ImageView iv;


        public MyViewHolder(View v) {
            super(v);
            tvname = (TextView) v.findViewById(R.id.name);
            iv = (ImageView) v.findViewById(R.id.imgView);
            tvBrandName=v.findViewById(R.id.tvBrandName);
            tvOldPrice=v.findViewById(R.id.tvOldPrice);
            tvNewPrice=v.findViewById(R.id.tvNewPrice);
        }
    }
}