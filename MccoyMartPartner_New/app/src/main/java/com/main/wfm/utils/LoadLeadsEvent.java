package com.main.wfm.utils;

public class LoadLeadsEvent {


    String type,search,filter;
    public LoadLeadsEvent(String type,String search,String filter){
        this.type=type;
        this.search=search;
        this.filter=filter;
    }
    public String getSearch() {
        return search;
    }
    public String getLeadType() {
        return type;
    }
    public String getFilter() {
        return filter;
    }

}
