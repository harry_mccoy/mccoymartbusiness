package com.main.wfm.apimodel;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ErrorDTO implements Serializable {

    private String error;

    public String getMessage() {
        return message;
    }

    private String message;

    private String error_description;

    public String getError ()
    {
        return error;
    }

    public void setError (String error)
    {
        this.error = error;
    }

    public String getError_description ()
    {
        return error_description;
    }

    public void setError_description (String error_description)
    {
        this.error_description = error_description;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [error = "+error+", error_description = "+error_description+"]";
    }


}
