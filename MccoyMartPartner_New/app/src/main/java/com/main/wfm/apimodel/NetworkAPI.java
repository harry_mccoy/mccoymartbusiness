package com.main.wfm.apimodel;

import android.content.Context;

import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apirequestmodel.AddEditAddressRequest;
import com.main.wfm.apirequestmodel.AddToCartRequest;
import com.main.wfm.apirequestmodel.LeadsRequestModel;
import com.main.wfm.apirequestmodel.ProductDetailRequest;
import com.main.wfm.apiresponsemodel.CartListResponse;
import com.main.wfm.apiresponsemodel.CartTotalResponse;
import com.main.wfm.apiresponsemodel.CategoryProductResponse;
import com.main.wfm.apiresponsemodel.LeadsFilterResponse;
import com.main.wfm.apiresponsemodel.LeadsResponse;
import com.main.wfm.apiresponsemodel.ManageAddressResponse;
import com.main.wfm.apiresponsemodel.MyOrderDataResponse;
import com.main.wfm.apiresponsemodel.NotificationListResponse;
import com.main.wfm.apiresponsemodel.PaymentMethodResponse;
import com.main.wfm.apiresponsemodel.ProductCommonResponse;
import com.main.wfm.apiresponsemodel.ProductDetailResponse;
import com.main.wfm.apiresponsemodel.RatingReviewResponse;
import com.main.wfm.apiresponsemodel.WishListResponse;
import com.main.wfm.models.GetProductOfferRequestBean;
import com.main.wfm.models.ProductInfoResponseBean;
import com.main.wfm.retrofitApi.JsonResponse;


public class NetworkAPI {


    public static void getLeadsFilters(Context mContext, String authorization,  final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getLeadsFilters(authorization)
                .enqueue(new RequestCallback<LeadsFilterResponse>(dispatch));
    }

    public static void getSortOrder(Context mContext, String authorization,  final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getSortOrder(authorization)
                .enqueue(new RequestCallback<LeadsFilterResponse>(dispatch));
    }

    public static void getTodaysProducts(Context mContext, String authorization, GetProductOfferRequestBean obGetProductOfferRequestBean
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getTodaysProductApi(authorization,obGetProductOfferRequestBean)
                .enqueue(new RequestCallback<ProductCommonResponse>(dispatch));
    }

    public static void getProductByCategory(Context mContext, String authorization, GetProductOfferRequestBean obGetProductOfferRequestBean
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getProductApi(authorization,obGetProductOfferRequestBean)
                .enqueue(new RequestCallback<ProductCommonResponse>(dispatch));
    }

    public static void getLatestProducts(Context mContext, String authorization, GetProductOfferRequestBean obGetProductOfferRequestBean
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getLatestProductApi(authorization,obGetProductOfferRequestBean)
                .enqueue(new RequestCallback<ProductCommonResponse>(dispatch));
    }

    public static void getTopBrandsProducts(Context mContext, String authorization, GetProductOfferRequestBean obGetProductOfferRequestBean
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getTopBrandsProductApi(authorization,obGetProductOfferRequestBean)
                .enqueue(new RequestCallback<ProductInfoResponseBean>(dispatch));
    }

    public static void getLeadsApi(Context mContext, String authorization, LeadsRequestModel obGetProductOfferRequestBean
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getLeadsApi(authorization,obGetProductOfferRequestBean)
                .enqueue(new RequestCallback<LeadsResponse>(dispatch));
    }
    public static void getCategoryProductsApi(Context mContext, String authorization, String category_id
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getCategoryProductsApi(authorization,category_id)
                .enqueue(new RequestCallback<CategoryProductResponse>(dispatch));
    }

    public static void getMyOrderApi(Context mContext, String authorization, int page
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getMyOrderApi(authorization,page)
                .enqueue(new RequestCallback<MyOrderDataResponse>(dispatch));
    }


    public static void getProductReviewAndRatingApi(Context mContext, String authorization, String product_id
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getProductReviewAndRatingApi(authorization,product_id)
                .enqueue(new RequestCallback<RatingReviewResponse>(dispatch));
    }

    public static void getAddressApi(Context mContext, String authorization
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getAddressApi(authorization)
                .enqueue(new RequestCallback<ManageAddressResponse>(dispatch));
    }

    public static void getProductDetailApi(Context mContext, String authorization, ProductDetailRequest obProductDetailRequest
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getProductDetailApi(authorization,obProductDetailRequest)
                .enqueue(new RequestCallback<ProductDetailResponse>(dispatch));
    }


    public static void deleteAddItemToWishListApi(Context mContext, String authorization, AddDeleteWishListRequest obAddDeleteWishListRequest
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).deleteAddItemToWishListApi(authorization,obAddDeleteWishListRequest)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }


    public static void addToCartApi(Context mContext, String authorization, AddToCartRequest obAddDeleteWishListRequest
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).addToCartApi(authorization,obAddDeleteWishListRequest)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }
    public static void removeFromCartApi(Context mContext, String authorization, AddToCartRequest obAddDeleteWishListRequest
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).removeFromCartApi(authorization,obAddDeleteWishListRequest)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }





    public static void getCartListApi(Context mContext, String authorization, AddDeleteWishListRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getCartListApi(authorization,body)
                .enqueue(new RequestCallback<CartListResponse>(dispatch));
    }

    public static void getCartTotalApi(Context mContext, String authorization, AddDeleteWishListRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getCartTotalApi(authorization,body)
                .enqueue(new RequestCallback<CartTotalResponse>(dispatch));
    }

    public static void addEditAddressApi(Context mContext, String authorization, AddEditAddressRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).addEditAddressApi(authorization,body)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }
    public static void deleteAddressApi(Context mContext, String authorization, AddDeleteWishListRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).deleteAddressApi(authorization,body)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }
    public static void saveForLaterApi(Context mContext, String authorization, AddDeleteWishListRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).saveForLaterApi(authorization,body)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }
    public static void addPaymentMethodApi(Context mContext, String authorization, AddDeleteWishListRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).addPaymentMethodApi(authorization,body)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }

    public static void addOrderApi(Context mContext, String authorization, AddDeleteWishListRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).addOrderApi(authorization,body)
                .enqueue(new RequestCallback<JsonResponse>(dispatch));
    }



        public static void paymentMethodsApi(Context mContext, String authorization, AddToCartRequest body
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).paymentMethodsApi(authorization,body)
                .enqueue(new RequestCallback<PaymentMethodResponse>(dispatch));
    }


    public static void getNotificationListResponse(Context mContext, String authorization
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getNotificationListResponse(authorization)
                .enqueue(new RequestCallback<NotificationListResponse>(dispatch));
    }
    public static void getOfferListResponse(Context mContext, String authorization
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getOfferListResponse(authorization)
                .enqueue(new RequestCallback<NotificationListResponse>(dispatch));
    }


    public static void getWishListApi(Context mContext, String authorization
            , final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).getWishListApi(authorization)
                .enqueue(new RequestCallback<WishListResponse>(dispatch));
    }
    /*public static void getRestaurantList(Context mContext, String devicetype, String deviceid,
                                  String latitude, String longitude,
                                  String resturanttype, String cuisineids,
                                  String todaysoffer, String resturantname,
                                  String specialoffer, int currentPage, int pageSize, final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).apiRestaurantList(devicetype, deviceid,
                latitude, longitude,
                resturanttype, cuisineids,
                todaysoffer, resturantname,
                specialoffer, currentPage, pageSize).enqueue(new RequestCallback<JsonResponse>(dispatch));
    }

    public static void getOfferList(Context mContext, String resturantid, String restaurantType, String offerType,  final Dispatch dispatch) {
        BaseNetworkService.getRequestEndPoints(mContext).apiOfferList(resturantid, restaurantType, offerType).enqueue(new RequestCallback<JsonResponse>(dispatch));
    }*/




}
