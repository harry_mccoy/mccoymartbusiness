package com.main.wfm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.graphics.Color.GRAY;
import static android.graphics.Color.RED;

public class ProductCommonGridAdapter extends BaseAdapter {

    private static final String TAG = ProductCommonGridAdapter.class.getSimpleName();
    private Context context;
    private List<ProductDataDashboardShopBean> arlProductInfoResponseBeans;
    private LayoutInflater infalter;
    private String moduleType;
    private boolean []isSelected;
    private AppSession appSession;

    int selectedPosition = 0;

    public ProductCommonGridAdapter(Context context, List<ProductDataDashboardShopBean> arlProductInfoResponseBeans, String moduleType) {
        this.context = context;
        this.arlProductInfoResponseBeans = arlProductInfoResponseBeans;
        this.moduleType = moduleType;
        appSession=new AppSession(this.context);
        isSelected=new boolean[this.arlProductInfoResponseBeans.size()];
        for (int i=0;i<this.arlProductInfoResponseBeans.size();i++){
            isSelected[i]=true;
        }
        infalter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public int getCount() {

        return arlProductInfoResponseBeans.size();
    }

    @Override
    public Object getItem(int position) {

        return arlProductInfoResponseBeans.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = infalter.inflate(R.layout.lv_todaysdeal_row, null);
            holder = new ViewHolder();
            holder.tvItemName = (TextView) convertView.findViewById(R.id.name);
            holder.tvNewRs = (TextView) convertView.findViewById(R.id.tvNewPrice);
            holder.tvOldRs = (TextView) convertView.findViewById(R.id.tvOldPrice);
            holder.tvBrandName = (TextView) convertView.findViewById(R.id.tvBrandName);
            holder.ivProductPic = (ImageView) convertView.findViewById(R.id.imgView);
            holder.llParent=convertView.findViewById(R.id.llParent);
            holder.ivAddToWishList=convertView.findViewById(R.id.ivAddToWishList);
            holder.tvOff=convertView.findViewById(R.id.tvOff);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(moduleType.equalsIgnoreCase("topBrands")){
            if (arlProductInfoResponseBeans.get(position).getImage() != null) {
                holder.tvItemName.setVisibility(View.GONE);
                holder.tvOldRs.setVisibility(View.GONE);
                holder.tvNewRs.setVisibility(View.GONE);
                holder.tvBrandName.setVisibility(View.GONE);
                holder.ivAddToWishList.setVisibility(View.GONE);
                holder.tvOff.setVisibility(View.GONE);
                Picasso.with(context).load(arlProductInfoResponseBeans.get(position).getImage())
                        .placeholder(context.getResources().getDrawable(R.drawable.upvc_hardware)).into(holder.ivProductPic);

            }
        }else{

            holder.tvItemName.setVisibility(View.VISIBLE);
            holder.tvOldRs.setVisibility(View.VISIBLE);
            holder.tvBrandName.setVisibility(View.VISIBLE);

            holder.tvBrandName.setText(arlProductInfoResponseBeans.get(position).getManufacturer());
            holder.tvItemName.setText(arlProductInfoResponseBeans.get(position).getName());
            if (arlProductInfoResponseBeans.get(position).getSpecial() != null) {
                holder.tvNewRs.setVisibility(View.VISIBLE);
                holder.tvNewRs.setText("Rs. "+arlProductInfoResponseBeans.get(position).getSpecial());
            } else {
                holder.tvNewRs.setVisibility(View.GONE);
            }
            if (arlProductInfoResponseBeans.get(position).getPrice() != null) {
                holder.tvOldRs.setText("Rs. "+arlProductInfoResponseBeans.get(position).getPrice());
                if(arlProductInfoResponseBeans.get(position).getSpecial()!=null&&
                        arlProductInfoResponseBeans.get(position).getSpecial().length()>0){
                    holder.tvOldRs.setPaintFlags(holder.tvOldRs.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

            }
            if (arlProductInfoResponseBeans.get(position).getImage() != null) {

                Picasso.with(context).load(arlProductInfoResponseBeans.get(position).getImage())
                        .placeholder(context.getResources().getDrawable(R.drawable.upvc_hardware)).into(holder.ivProductPic);

            }

            holder.llParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("product_id",arlProductInfoResponseBeans.get(position).getProduct_id());
                    intent.putExtra("product_name",arlProductInfoResponseBeans.get(position).getManufacturer());
                     context.startActivity(intent);
                }
            });

            if(arlProductInfoResponseBeans.get(position).getDiscount_off()!=null&&arlProductInfoResponseBeans.get(position).getDiscount_off().length()>0){
                //holder.tvOff.setVisibility(View.VISIBLE);
                holder.tvOff.setText(arlProductInfoResponseBeans.get(position).getDiscount_off());
            }else{
                //holder.tvOff.setVisibility(View.GONE);
            }

        }
        holder.ivAddToWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelected[position]){
                    addItemToWishListApi(arlProductInfoResponseBeans.get(position).getProduct_id(),"add",holder);
                    isSelected[position]=false;
                    notifyDataSetChanged();
                }else{
                    addItemToWishListApi(arlProductInfoResponseBeans.get(position).getProduct_id(),"delete",holder);
                    isSelected[position]=true;
                    notifyDataSetChanged();
                }

            }
        });

        return convertView;
    }

    public class ViewHolder {
        ImageView ivProductPic,ivAddToWishList;
        TextView tvItemName, tvOldRs, tvNewRs,tvBrandName,tvOff;
        private LinearLayout llParent;
    }

    public void addItemToWishListApi(String productId, final String action, final ViewHolder holder) {
        final ViewDialog viewDialog = new ViewDialog((Activity) context);
        viewDialog.showDialog();
        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setAction(action);
        request.setProduct_id(productId);
        NetworkAPI.deleteAddItemToWishListApi(context, "Bearer " + appSession.getAuthToken(),request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if(body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.showToast(context,body.message);
                                if(action.equalsIgnoreCase("add")){
                                    holder.ivAddToWishList.setColorFilter(RED, PorterDuff.Mode.SRC_IN);
                                }else{
                                    holder.ivAddToWishList.setColorFilter(GRAY, PorterDuff.Mode.SRC_IN);
                                }

                                //DrawableCompat.setTint(holder.ivAddToWishList.getDrawable(), ContextCompat.getColor(mContext, R.color.red_google_btn));

                                notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(context, context.getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}
