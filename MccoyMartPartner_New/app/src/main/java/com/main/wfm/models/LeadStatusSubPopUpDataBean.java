package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class LeadStatusSubPopUpDataBean {

    @SerializedName("text")
    private String text;

    @SerializedName("value")
    private String value;

    @SerializedName("Action")
    private String action;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


}
