package com.main.wfm.models;

/**
 * Created on 10-05-2019.
 */
public class AddVideoBean {

    private String company_id;
    private String video_url;
    private String old_url;
    private String type;
    private String action;


    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getOld_url() {
        return old_url;
    }

    public void setOld_url(String old_url) {
        this.old_url = old_url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
