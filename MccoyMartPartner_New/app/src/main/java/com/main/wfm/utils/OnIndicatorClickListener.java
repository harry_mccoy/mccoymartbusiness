package com.main.wfm.utils;

public interface OnIndicatorClickListener {

    void onIndicatorClick(int position);
}
