package com.main.wfm.retrofitApi;


import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static JsonError parseError(Response<?> response) {
        Converter<ResponseBody, JsonError> converter =
                ApiErrorExecutor.retrofit
                        .responseBodyConverter(JsonError.class, new Annotation[0]);

        JsonError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            e.printStackTrace();
            return new JsonError();
        }

        return error;
    }
}