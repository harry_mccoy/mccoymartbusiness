package com.main.wfm.models;

/**
 * Created on 10-05-2019.
 */
public class AddPDFBean {

    private String company_id;
    private String pdf_key;

    private String pdf;
    private String action;


    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getPdf_key() {
        return pdf_key;
    }

    public void setPdf_key(String pdf_key) {
        this.pdf_key = pdf_key;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
