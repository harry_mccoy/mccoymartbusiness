package com.main.wfm.apirequestmodel;

import com.google.gson.annotations.SerializedName;

public class AddToCartRequest {

    @SerializedName("source")
    private String source;

    @SerializedName("pin_code")
    private String pin_code;

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @SerializedName("weight")
    private String weight;

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("qty")
    private String qty  ;

    @SerializedName("device_id")
    private String device_id;

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
