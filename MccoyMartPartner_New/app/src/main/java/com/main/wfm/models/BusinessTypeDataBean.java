package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class BusinessTypeDataBean {
    @SerializedName("id")
    private int businessId;
    @SerializedName("name")
    private String businessTitle;
    @SerializedName("text")
    private String businessSlug;
    @SerializedName("select_image")
    private String selectImage;
    @SerializedName("unselect_image")
    private String unselectImage;
    private boolean isActive;

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public String getBusinessTitle() {
        return businessTitle;
    }

    public void setBusinessTitle(String businessTitle) {
        this.businessTitle = businessTitle;
    }

    public String getBusinessSlug() {
        return businessSlug;
    }

    public void setBusinessSlug(String businessSlug) {
        this.businessSlug = businessSlug;
    }

    public String getSelectImage() {
        return selectImage;
    }

    public void setSelectImage(String selectImage) {
        this.selectImage = selectImage;
    }

    public String getUnselectImage() {
        return unselectImage;
    }

    public void setUnselectImage(String unselectImage) {
        this.unselectImage = unselectImage;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
