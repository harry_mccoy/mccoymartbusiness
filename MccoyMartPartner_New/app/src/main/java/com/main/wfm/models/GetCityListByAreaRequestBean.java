package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class GetCityListByAreaRequestBean {
    @SerializedName("area_id")
    private String areaId;
    @SerializedName("state_id")
    private String stateId;

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }
}
