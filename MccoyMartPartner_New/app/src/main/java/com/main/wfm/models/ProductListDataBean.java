package com.main.wfm.models;

/**
 * Created on 06-05-2019.
 */
public class ProductListDataBean {
    /**
     * id : 8052
     * company_id : 14373
     * status : 0
     * status_code : draft
     * status_color : #FF0000
     * product_name : Mccoy Product
     * product_slug : mccoy-product
     * product_logo :
     * video_status : 0
     * browser_status : 0
     * views : 0
     */

    private int id;
    private int company_id;
    private int status;
    private String status_code;
    private String status_color;
    private String product_name;
    private String product_slug;
    private String product_logo;
    private int video_status;
    private int browser_status;
    private int views;
    private String product_price;
    private String company_name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus_color() {
        return status_color;
    }

    public void setStatus_color(String status_color) {
        this.status_color = status_color;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_slug() {
        return product_slug;
    }

    public void setProduct_slug(String product_slug) {
        this.product_slug = product_slug;
    }

    public String getProduct_logo() {
        return product_logo;
    }

    public void setProduct_logo(String product_logo) {
        this.product_logo = product_logo;
    }

    public int getVideo_status() {
        return video_status;
    }

    public void setVideo_status(int video_status) {
        this.video_status = video_status;
    }

    public int getBrowser_status() {
        return browser_status;
    }

    public void setBrowser_status(int browser_status) {
        this.browser_status = browser_status;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }


    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }
}
