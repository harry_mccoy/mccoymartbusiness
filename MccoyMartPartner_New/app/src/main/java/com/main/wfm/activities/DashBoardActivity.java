package com.main.wfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.SideMenuAdapter;
import com.main.wfm.apiresponsemodel.SideMenuDataItemsResponse;
import com.main.wfm.apiresponsemodel.SideMenuDataResponse;
import com.main.wfm.apiresponsemodel.SideMenuResponse;
import com.main.wfm.fragments.DashboardFragment;
import com.main.wfm.fragments.LeadsHomeFragment;
import com.main.wfm.fragments.MyBizFragment;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

;


public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {

    boolean doubleBackToExitPressedOnce = false;
    private TextView tvShop, tvLeads, tvNotification, tvMyBiz;
    private DrawerLayout mDrawerLayout;
    private LinearLayout llDrawer,llProfile;
    private ImageView ivDrawer;
    private AppSession appSession;
    private List<SideMenuDataResponse> arlSideMenuData;
    private List<SideMenuDataItemsResponse> arlSideMenuItemData;
    private SideMenuAdapter adapter;
    private ListView lvSideMenu;
    private FrameLayout flCart;
    private String tabType="shop";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        getIds();
        CommonUtils.setFragment(new DashboardFragment(), true, DashBoardActivity.this, R.id.flContainer);

    }

    private void getIds() {

        ivDrawer = findViewById(R.id.ivDrawer);
        tvLeads = findViewById(R.id.tvLeads);
        tvShop = findViewById(R.id.tvShop);
        tvNotification = findViewById(R.id.tvNotification);
        tvMyBiz = findViewById(R.id.tvMyBiz);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        llDrawer = findViewById(R.id.llDrawer);
        lvSideMenu = findViewById(R.id.lvSideMenu);
        flCart=findViewById(R.id.flCart);
        llProfile=findViewById(R.id.llProfile);
        appSession = new AppSession(DashBoardActivity.this);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        tvLeads.setOnClickListener(this);
        tvShop.setOnClickListener(this);
        tvNotification.setOnClickListener(this);
        tvMyBiz.setOnClickListener(this);
        ivDrawer.setOnClickListener(this);
        flCart.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        //EventBus.getDefault().register(DashBoardActivity.this);
        ((TextView) findViewById(R.id.tvDisplayName)).setText(appSession.getUserName());

        if(appSession.getCartCount()>0){
            ((TextView)findViewById(R.id.tvCartCount)).setText(""+appSession.getCartCount());
        }

        getSideMenuDataAPI();

        lvSideMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("PARENT TEXT: "+arlSideMenuItemData.get(position).isSectionHeader());
                Intent intent;
                if(!arlSideMenuItemData.get(position).isSectionHeader()){
                    if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Leads")){
                       /* intent = new Intent(DashBoardActivity.this, LeadsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);*/
                        tabType="leads";
                        tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads_hover, 0, 0);
                        tvLeads.setTextColor(getResources().getColor(R.color.nav_bar));

                        tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                        tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                        tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification, 0, 0);
                        tvNotification.setTextColor(getResources().getColor(R.color.gray_like));
                        tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                        tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                        CommonUtils.setFragment(new LeadsHomeFragment(), true, DashBoardActivity.this, R.id.flContainer);
                        if (mDrawerLayout.isDrawerOpen(llDrawer)) {
                            mDrawerLayout.closeDrawer(llDrawer);
                        }
                    }else
                    if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Logout")){
                        CommonUtils.logOutAlert(DashBoardActivity.this,getString(R.string.logout_alert),appSession);
                    }else
                    if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Notifications")){
                       /* intent = new Intent(DashBoardActivity.this, NotificationAndOfferTabFragment.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);*/
                        tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification_hover, 0, 0);
                        tvNotification.setTextColor(getResources().getColor(R.color.nav_bar));

                        tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                        tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                        tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads, 0, 0);
                        tvLeads.setTextColor(getResources().getColor(R.color.gray_like));
                        tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                        tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                        CommonUtils.setFragment(new NotificationAndOfferTabFragment(), true, DashBoardActivity.this, R.id.flContainer);
                        if (mDrawerLayout.isDrawerOpen(llDrawer)) {
                            mDrawerLayout.closeDrawer(llDrawer);
                        }
                    }else
                    if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Your Orders")){
                        intent = new Intent(DashBoardActivity.this, MyOrdersActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }else if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Dashboard")){
                        tabType="mybiz";
                        setSelectedTab(tabType);
                        CommonUtils.setFragment(new MyBizFragment(), true, DashBoardActivity.this, R.id.flContainer);
                        if (mDrawerLayout.isDrawerOpen(llDrawer)) {
                            mDrawerLayout.closeDrawer(llDrawer);
                        }
                    }else if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Company Profile")){

                    }else if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Products")){

                    }else if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Reviews")){

                    }else if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Support")){
                        intent = new Intent(DashBoardActivity.this, SupportActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }else if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Change Password")){
                        intent = new Intent(DashBoardActivity.this, MyWishListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }else if(arlSideMenuItemData.get(position).isName().equalsIgnoreCase("Manage Address")){
                         intent = new Intent(DashBoardActivity.this, ManageAddressActivity.class);
                         intent.putExtra("addressType","editAddress");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                    else{
                        intent = new Intent(DashBoardActivity.this, CategoryProductsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("category_id",arlSideMenuItemData.get(position).isCategory_id());
                        intent.putExtra("category_name",arlSideMenuItemData.get(position).isName());
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }

            }

        });
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();


        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
/*    @Subscribe
    public void getSearch(LoadLeadsEvent events) {

    }*/
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llProfile:
                Intent intent = new Intent(DashBoardActivity.this, ProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.flCart:
                 intent = new Intent(DashBoardActivity.this, CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.ivDrawer:
                if (mDrawerLayout.isDrawerOpen(llDrawer)) {
                    mDrawerLayout.closeDrawer(llDrawer);
                } else {
                    mDrawerLayout.openDrawer(llDrawer);
                }

                break;
            case R.id.tvShop:
                tabType="shop";
                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop_hover, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.nav_bar));

                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.gray_like));
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.gray_like));
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                CommonUtils.setFragment(new DashboardFragment(), true, DashBoardActivity.this, R.id.flContainer);
                break;
            case R.id.tvLeads:
                tabType="leads";
                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads_hover, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.nav_bar));

                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.gray_like));
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                CommonUtils.setFragment(new LeadsHomeFragment(), true, DashBoardActivity.this, R.id.flContainer);
                /*intent = new Intent(DashBoardActivity.this, LeadsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/

                break;
            case R.id.tvNotification:
                tabType="notification";
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification_hover, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.nav_bar));

                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.gray_like));
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                CommonUtils.setFragment(new NotificationAndOfferTabFragment(), true, DashBoardActivity.this, R.id.flContainer);
                /*intent = new Intent(DashBoardActivity.this, NotificationAndOfferTabFragment.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/


                break;
            case R.id.tvMyBiz:
                tabType="mybiz";
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.mybiz_hover, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.nav_bar));

                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.gray_like));
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.gray_like));

                CommonUtils.setFragment(new MyBizFragment(), true, DashBoardActivity.this, R.id.flContainer);

                break;
        }
    }

    private void getSideMenuDataAPI() {
        final ViewDialog viewDialog = new ViewDialog(DashBoardActivity.this);
        viewDialog.showDialog();


        Call<SideMenuResponse> call = ApiExecutor.getApiService(DashBoardActivity.this).getSideMenuListApi("Bearer "+appSession.getAuthToken());
        System.out.println("API url ---" + call.request().url());
        System.out.println("API url ---" + call.request());

        call.enqueue(new Callback<SideMenuResponse>() {
                         @Override
                         public void onResponse(Call<SideMenuResponse> call, Response<SideMenuResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Dashboard Data " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:

                                     arlSideMenuData = response.body().getArlSideMenuData();
                                     arlSideMenuItemData = new ArrayList<SideMenuDataItemsResponse>();
                                     for (int i = 0; i < arlSideMenuData.size(); i++) {
                                         SideMenuDataItemsResponse obSideMenuDataItemsResponse = new SideMenuDataItemsResponse();
                                         obSideMenuDataItemsResponse.setSectionHeader(true);
                                         obSideMenuDataItemsResponse.setName(arlSideMenuData.get(i).isName());
                                         obSideMenuDataItemsResponse.setCategory_id("0");
                                         arlSideMenuItemData.add(obSideMenuDataItemsResponse);
                                         for (int j = 0; j < arlSideMenuData.get(i).getArlSideMenuData().size(); j++) {
                                             SideMenuDataItemsResponse obSideMenuDataItemsResponse2 = new SideMenuDataItemsResponse();
                                             obSideMenuDataItemsResponse2.setName(arlSideMenuData.get(i).getArlSideMenuData().get(j).isName());
                                             obSideMenuDataItemsResponse2.setCategory_id(arlSideMenuData.get(i).getArlSideMenuData().get(j).isCategory_id());
                                             arlSideMenuItemData.add(obSideMenuDataItemsResponse2);
                                         }

                                     }
                                     adapter = new SideMenuAdapter(DashBoardActivity.this, arlSideMenuItemData);
                                     lvSideMenu.setAdapter(adapter);

                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), DashBoardActivity.this);
                                     break;
                                 case 401:
                                     error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), DashBoardActivity.this);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<SideMenuResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawerLayout.isDrawerOpen(llDrawer)) {
            mDrawerLayout.closeDrawer(llDrawer);
        }
        setSelectedTab(tabType);
    }

    public void setSelectedTab(String tabType){
        switch (tabType){
            case "shop":
                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop_hover, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.nav_bar));

                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.gray_like));
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.gray_like));
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                break;
            case "leads":
                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads_hover, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.nav_bar));
                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.gray_like));
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                break;
            case "notification":
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification_hover, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.nav_bar));

                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.gray_like));
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_biz, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.gray_like));

                break;
            case "mybiz":
                tvMyBiz.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.mybiz_hover, 0, 0);
                tvMyBiz.setTextColor(getResources().getColor(R.color.nav_bar));

                tvShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.shop, 0, 0);
                tvShop.setTextColor(getResources().getColor(R.color.gray_like));
                tvLeads.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.leads, 0, 0);
                tvLeads.setTextColor(getResources().getColor(R.color.gray_like));
                tvNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification, 0, 0);
                tvNotification.setTextColor(getResources().getColor(R.color.gray_like));

                break;
        }
        }


}
