package com.main.wfm.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.ReviewListAdapter;
import com.main.wfm.adapters.WishListAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.RatingReviewResponse;
import com.main.wfm.apiresponsemodel.ReviewItemResponse;
import com.main.wfm.apiresponsemodel.WishListDataResponse;
import com.main.wfm.apiresponsemodel.WishListResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.VerticalLineDecorator;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class MyWishListActivity extends AppCompatActivity {

    private WishListAdapter adapter;
    private List<WishListDataResponse> wishListResponses;
    private RecyclerView rvReviewList;
    private AppSession appSession;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSession=new AppSession(MyWishListActivity.this);
        setContentView(R.layout.activity_review);

        ((ImageView) findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((TextView)findViewById(R.id.tvContinueShopping)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rvReviewList=findViewById(R.id.rvReviewList);
        rvReviewList.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(MyWishListActivity.this);
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        wishListResponses =new ArrayList<WishListDataResponse>();
        adapter=new WishListAdapter(MyWishListActivity.this, wishListResponses);
        rvReviewList.setAdapter(adapter);
        rvReviewList.addItemDecoration(new VerticalLineDecorator(2));
        rvReviewList.setLayoutManager(MyLayoutManager);

        //getWishListApi(getIntent().getStringExtra("productId"));
        getWishListApi();
    }

    public void getWishListApi() {
        final ViewDialog viewDialog = new ViewDialog(MyWishListActivity.this);
        viewDialog.showDialog();

        NetworkAPI.getWishListApi(MyWishListActivity.this, "Bearer " + appSession.getAuthToken(),
                new Dispatch<WishListResponse>() {
                    @Override
                    public void apiSuccess(WishListResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getWishListData() != null) {

                                wishListResponses = body.getWishListData();
                                if(wishListResponses!=null&&wishListResponses.size()>0){
                                    ((LinearLayout)findViewById(R.id.llEmptyWishListFrame)).setVisibility(View.GONE);
                                    rvReviewList.setVisibility(View.VISIBLE);
                                    adapter=new WishListAdapter(MyWishListActivity.this, wishListResponses);
                                    rvReviewList.setAdapter(adapter);
                                    ((TextView) findViewById(R.id.tvTitle)).setText("Wish List ("+wishListResponses.size()+")");
                                }else{
                                    rvReviewList.setVisibility(View.GONE);
                                    ((LinearLayout)findViewById(R.id.llEmptyWishListFrame)).setVisibility(View.VISIBLE);
                                    ((TextView) findViewById(R.id.tvTitle)).setText("Wish List (0)");
                                }

                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(MyWishListActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}
