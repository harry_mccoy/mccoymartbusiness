package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SideMenuResponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String statusMessage;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("data")
    private List<SideMenuDataResponse> arlSideMenuData;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<SideMenuDataResponse> getArlSideMenuData() {
        return arlSideMenuData;
    }

    public void setArlSideMenuData(List<SideMenuDataResponse> arlSideMenuData) {
        this.arlSideMenuData = arlSideMenuData;
    }
}
