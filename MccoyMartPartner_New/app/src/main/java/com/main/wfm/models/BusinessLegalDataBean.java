package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class BusinessLegalDataBean {
    @SerializedName("id")
    private int leagalId;
    @SerializedName("meta_value")
    private String legalTitle;
    private boolean isChecked;

    public int getLeagalId() {
        return leagalId;
    }

    public void setLeagalId(int leagalId) {
        this.leagalId = leagalId;
    }

    public String getLegalTitle() {
        return legalTitle;
    }

    public void setLegalTitle(String legalTitle) {
        this.legalTitle = legalTitle;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
