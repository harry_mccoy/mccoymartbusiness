package com.main.wfm.apimodel;


import com.main.wfm.utils.AppConstants;

public class ErrorUtils {

    static ErrorDTO errorDTO;
    public static ErrorDTO getErrorDetails(String failureResponseJson){
        try{
            errorDTO = AppConstants.GSON.fromJson(failureResponseJson, ErrorDTO.class);
            return errorDTO;
        }
        catch(Exception e){
            e.printStackTrace();
            //errorDTO.setExceptionMessage(e.getMessage());
            return errorDTO;

        }
    }
}
