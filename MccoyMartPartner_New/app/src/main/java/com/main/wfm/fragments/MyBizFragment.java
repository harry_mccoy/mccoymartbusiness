package com.main.wfm.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.main.wfm.R;
import com.main.wfm.activities.DashBoardActivity;
import com.main.wfm.activities.SupportActivity;
import com.main.wfm.utils.CommonUtils;

public class MyBizFragment extends Fragment implements View.OnClickListener {
private Context mContext;

private TextView tvCompanyProfile,tvProducts,tvProjects,tvLeads,tvReviews,tvSupports;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_mybiz, container, false);
        getIds(root);
        return root;
    }

    private void getIds(View root) {

        tvCompanyProfile=root.findViewById(R.id.tvCompanyProfile);
        tvLeads=root.findViewById(R.id.tvLeads);
        tvProducts=root.findViewById(R.id.tvProducts);
        tvProjects=root.findViewById(R.id.tvProjects);
        tvReviews=root.findViewById(R.id.tvReviews);
        tvSupports=root.findViewById(R.id.tvSupports);

        tvReviews.setOnClickListener(this);
        tvSupports.setOnClickListener(this);
        tvProjects.setOnClickListener(this);
        tvProducts.setOnClickListener(this);
        tvLeads.setOnClickListener(this);
        tvCompanyProfile.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext=context;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tvCompanyProfile:
                break;
            case R.id.tvLeads:
                CommonUtils.setFragment(new LeadsHomeFragment(), true, getActivity(), R.id.flContainer);
                ((DashBoardActivity)getActivity()).setSelectedTab("leads");
                break;
            case R.id.tvProducts:
                break;
            case R.id.tvProjects:
                break;
            case R.id.tvReviews:
                break;
            case R.id.tvSupports:
                Intent intent = new Intent(mContext, SupportActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }
}