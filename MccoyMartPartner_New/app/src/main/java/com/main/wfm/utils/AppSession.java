package com.main.wfm.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ravi.shah.
 */
public class AppSession {

    private static final String SHARED = "mewada_Preferences";
    private static final String USER_ID = "userID";
    private static final String DEMO_COMPLETE = "demoComplete";
    //private static final String ACCESS_TOKEN = "accessToken";
    private static final String IS_LOGIN = "isLogin";
    private static final String IS_SYNC = "isSync";
    private static final String BOOKING_ID = "bookingid";
    private static final String CART_COUNT="cartCount";
    private static final String LEADS_PAGER_POSITION="leadsPagerPosition";

    public static final String DEVICE_TOKEN = "deviceTokne";
    public static final String DEVICE_ID = "android_id";


    public static final String USER_NAME = "username";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PHONE_NO = "phone_no";
    public static final String EMAIL_ID = "email_id";
    public static final String LOCATION = "location";
    public static final String ACCOUNT_MANAGER_EMAIL = "account_manager_email";
    public static final String ACCOUNT_MANAGER_NAME = "account_manage_name";
    public static final String ACCOUNT_MANAGER_PROFILE_PIC = "account_manage_profile_pic";
    public static final String CLIENT_PROFILE_PIC= "client_profile_image";
    public static final String CLIENT_DISPLAY_NAME= "display_name";
    public static final String AM_MOBILE= "am_mobilr";

    public static final String AUTH_TOKEN= "auth_token";
    public static final String ENTITY_TYPE= "entity_type";
    public static final String GA_VIEW_ID= "ga_view_id";
    private static final String WEBSITE ="website" ;
    private static final String TWITTER_ID ="twitter_id" ;
    private static final String RESTAURANT_TYPE ="restaurant_type" ;
    private static final String BOOKED_TABLE ="book_table" ;

    private static final String LATITUDE ="latitude" ;
    private static final String LONGITUDE ="Longitude" ;

    private static final String SELECTED_LOCATION ="selected_locaiton" ;
    private static final String SELECTED_BASE_URL ="selected_base_url" ;

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private Context context;

    public AppSession(Context context) {
        this.context = context;
        sharedPref = context.getSharedPreferences(SHARED, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void setSession(String userId, boolean isLogin) {
        editor.putString(USER_ID, userId);
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.commit();
    }

    public void setLatLng(String latitude, String longitude) {
        editor.putString(LONGITUDE, longitude);
        editor.putString(LATITUDE,latitude);
        editor.commit();
    }

    public void setUserData(String username, String firstName, String lastName, String email, String phoneNo) {

        editor.putString(USER_NAME, username);
        editor.putString(FIRST_NAME, firstName);
        editor.putString(LAST_NAME, lastName);
        editor.putString(EMAIL_ID, email);
        editor.putString(PHONE_NO, phoneNo);


        editor.commit();
    }

    public String getLatitude() {
        return sharedPref.getString(LATITUDE, "");
    }
    public String getLongitude() {
        return sharedPref.getString(LONGITUDE, "");
    }


    public String getUserName() {
        return sharedPref.getString(USER_NAME, "");
    }

    public String getFirstName() {
        return sharedPref.getString(FIRST_NAME, "");
    }

    public String getEntityType() {
        return sharedPref.getString(ENTITY_TYPE, "");
    }

    public String getAuthToken() {
        return sharedPref.getString(AUTH_TOKEN, "");
    }

    public String getWebsite() {
        return sharedPref.getString(WEBSITE, "");
    }
    public String getTwitterId() {
        return sharedPref.getString(TWITTER_ID, "");
    }


    public String getUserLocation() {
        return sharedPref.getString(LOCATION, "");
    }

    public String getLastName() {
        return sharedPref.getString(LAST_NAME, "");
    }
    public String getGAViewId() {
        return sharedPref.getString(GA_VIEW_ID, "");
    }

    public String getEmailId() {
        return sharedPref.getString(EMAIL_ID, "");
    }

    public String getPhoneNo() {
        return sharedPref.getString(PHONE_NO, "");
    }
    public String getAccountManagerName() {
        return sharedPref.getString(ACCOUNT_MANAGER_NAME, "");
    }
    public String getAccountManagerEmail() {return sharedPref.getString(ACCOUNT_MANAGER_EMAIL, "");}

    public String getAMmobilel() {return sharedPref.getString(AM_MOBILE, "");}
    public String getAccountManagerProfilePic() {
        return sharedPref.getString(ACCOUNT_MANAGER_PROFILE_PIC, "");
    }
    public String getClientProfilePic() {return sharedPref.getString(CLIENT_PROFILE_PIC, "");}

    public void setClientProfilePic(String client_profile_pic){
        editor.putString(CLIENT_PROFILE_PIC, client_profile_pic);
        editor.commit();
    }

    public String getBookingId() {return sharedPref.getString(BOOKING_ID, "");}

    public void setBookingId(String bookingId){
        editor.putString(BOOKING_ID, bookingId);
        editor.commit();
    }

    public String getDemoComplete() {return sharedPref.getString(DEMO_COMPLETE, "");}

    public void setDemoComplete(String demoComplete){
        editor.putString(DEMO_COMPLETE, demoComplete);
        editor.commit();
    }
    public int getCartCount() {return sharedPref.getInt(CART_COUNT, 0);}

    public void setCartCount(int cartCount){
        editor.putInt(CART_COUNT, cartCount);
        editor.commit();
    }

    public int getLeadsPagerPosition() {return sharedPref.getInt(LEADS_PAGER_POSITION, 0);}

    public void setLeadsPagerPosition(int cartCount){
        editor.putInt(LEADS_PAGER_POSITION, cartCount);
        editor.commit();
    }

    public String getRestaurantType() {
        return sharedPref.getString(RESTAURANT_TYPE, "");}

    public void setRestaurantType(String restaurantType){
        editor.putString(RESTAURANT_TYPE, restaurantType);
        editor.commit();
    }


    public boolean getBookTable() {
        return sharedPref.getBoolean(BOOKED_TABLE, false);}

    public void setBookedTable(boolean bookedTable){
        editor.putBoolean(BOOKED_TABLE, bookedTable);
        editor.commit();
    }


    public void setAuthToken(String auth_token){
        editor.putString(AUTH_TOKEN, auth_token);
        editor.commit();
    }
    public String getUserId() {
        return sharedPref.getString(USER_ID, "");
    }
    public String getClientDisplayName() {
        return sharedPref.getString(CLIENT_DISPLAY_NAME, "");
    }


    /*public String getAccessToken() {
        return sharedPref.getString(ACCESS_TOKEN, "DKFJDJSDFKSDF8789990SDFSFSFAS");
    }
*/
    public boolean getIsLogin() {
        return sharedPref.getBoolean(IS_LOGIN, false);
    }

    public void setUserId(String userId){
        editor.putString(USER_ID, userId);
        editor.commit();
    }


    public void setDeviceId(String deviceId){
        editor.putString(DEVICE_ID, deviceId);
        editor.commit();
    }

    public String getDeviceId(){

        return sharedPref.getString(DEVICE_ID, "");
    }
    public void setDeviceToken(String resgiterId){
        editor.putString(DEVICE_TOKEN, resgiterId);
        editor.commit();
    }

    public String getDeviceToken(){
        return sharedPref.getString(DEVICE_TOKEN, "");
    }


    public void setIsSync(boolean isSync){
        editor.putBoolean(IS_SYNC, isSync);
        editor.commit();
    }

    public boolean getIsSync(){
        return sharedPref.getBoolean(IS_SYNC, false);
    }

    private static final String OFFER_TYPE = "offer_type";
    public String getOfferType() {
        return sharedPref.getString(OFFER_TYPE, "All");
    }
    public void setOfferType(String offerType){
        editor.putString(OFFER_TYPE, offerType);
        editor.commit();
    }


    public String getSelectedBaseUrl() {return sharedPref.getString(SELECTED_BASE_URL, "");}

    public void setSelectedBaseUrl(String type){
        editor.putString(SELECTED_BASE_URL, type);
        editor.commit();
    }


  public String getSelectedLocation() {return sharedPref.getString(SELECTED_LOCATION, "");}

    public void setSelectedLocation(String locationType){
        editor.putString(SELECTED_LOCATION, locationType);
        editor.commit();
    }

}