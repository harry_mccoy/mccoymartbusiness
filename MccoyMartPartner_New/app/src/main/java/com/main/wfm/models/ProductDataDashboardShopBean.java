package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class ProductDataDashboardShopBean {
    @SerializedName("product_id")
    private String product_id;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;
    @SerializedName("manufacturer")
    private String manufacturer;
    @SerializedName("discount")
    private String discount;
    @SerializedName("special")
    private String special;
    @SerializedName("price")
    private String price;
    @SerializedName("rating")
    private String rating;
    @SerializedName("discount_off")
    private String discount_off;
    @SerializedName("href")
    private String href;

    @SerializedName("category_id")
    private String category_id;



    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDiscount_off() {
        return discount_off;
    }

    public void setDiscount_off(String discount_off) {
        this.discount_off = discount_off;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
