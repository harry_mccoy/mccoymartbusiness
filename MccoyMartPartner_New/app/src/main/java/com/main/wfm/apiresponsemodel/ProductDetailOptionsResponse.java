package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetailOptionsResponse {

    @SerializedName("name")
    private String name;

    @SerializedName("option_id")
    private String option_id;

    @SerializedName("product_option_id")
    private String product_option_id;

    @SerializedName("required")
    private String required;

    @SerializedName("type")
    private String manufacturer;

   /* @SerializedName("type")
    private String type;*/

    @SerializedName("value")
    private String value;

    @SerializedName("product_option_value")
    private List<ProductDetailOptionValueResponse> productOptionValues;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }

    public String getProduct_option_id() {
        return product_option_id;
    }

    public void setProduct_option_id(String product_option_id) {
        this.product_option_id = product_option_id;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

   /* public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }*/

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<ProductDetailOptionValueResponse> getProductOptionValues() {
        return productOptionValues;
    }

    public void setProductOptionValues(List<ProductDetailOptionValueResponse> productOptionValues) {
        this.productOptionValues = productOptionValues;
    }
}
