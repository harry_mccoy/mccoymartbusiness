package com.main.wfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.main.wfm.R;
import com.main.wfm.utils.CommonUtils;

public class LoginStepThreeActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvNext;
    private EditText etFullName;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_stepthree);
        getIds();
    }

    private void getIds() {
        tvNext=findViewById(R.id.tvNext);
        etFullName=findViewById(R.id.etFullName);
        tvNext.setOnClickListener(this);

        etFullName.addTextChangedListener(mTextEditorWatcher);
        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tvNext:
                if(etFullName.length()>0){
                    Intent intent = new Intent(LoginStepThreeActivity.this, LoginStepFourActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("otp",getIntent().getStringExtra("otp"));
                    intent.putExtra("username",getIntent().getStringExtra("username"));
                    intent.putExtra("fullname",etFullName.getText().toString().trim());
                    startActivity(intent);

                }else{
                    CommonUtils.showAlertOk("Full name can not be left blank.", LoginStepThreeActivity.this);
                }

                break;
        }
    }
    private final TextWatcher mTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //This sets a textview to the current length
            if(s.length()==0){
                ((TextInputLayout)findViewById(R.id.tvHint)).setHintEnabled(false);
                etFullName.setHint("Enter your full name");
            }else{
                ((TextInputLayout)findViewById(R.id.tvHint)).setHintEnabled(true);
            }
        }

        public void afterTextChanged(Editable s) {
        }
    };
}
