package com.main.wfm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CityListDataBean implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    protected CityListDataBean(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator<CityListDataBean> CREATOR = new Creator<CityListDataBean>() {
        @Override
        public CityListDataBean createFromParcel(Parcel in) {
            return new CityListDataBean(in);
        }

        @Override
        public CityListDataBean[] newArray(int size) {
            return new CityListDataBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }
}
