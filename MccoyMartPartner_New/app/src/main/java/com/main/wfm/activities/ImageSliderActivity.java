package com.main.wfm.activities;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.main.wfm.R;
import com.main.wfm.adapters.SlideImageMainAdapter;
import com.main.wfm.adapters.SlideImageThumbAdapter;
import com.main.wfm.apiresponsemodel.ProductImagesResponse;
import com.main.wfm.utils.CircleIndicator;
import com.main.wfm.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class ImageSliderActivity extends AppCompatActivity {
    private CircleIndicator circleIndicator;

    private ViewPager vPagerTutorial,vPagerTutorialThumb;
    private List<ProductImagesResponse> productImages;
    private SlideImageMainAdapter adapterMain;
    private SlideImageThumbAdapter adapterThumb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageslider);
        getIds();
    }

    private void getIds() {

        vPagerTutorial=findViewById(R.id.vPagerTutorial);
        vPagerTutorialThumb=findViewById(R.id.vPagerTutorialThumb);

        productImages=new ArrayList<ProductImagesResponse>();
        productImages=(ArrayList<ProductImagesResponse>) getIntent().getSerializableExtra("imageArray");

        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        adapterMain = new SlideImageMainAdapter(ImageSliderActivity.this, productImages);
        vPagerTutorial.setAdapter(adapterMain);
        circleIndicator = findViewById(R.id.indicator);
        circleIndicator.setViewPager(vPagerTutorial);

        adapterThumb = new SlideImageThumbAdapter(ImageSliderActivity.this, productImages);
        vPagerTutorialThumb.setAdapter(adapterThumb);
    }
}
