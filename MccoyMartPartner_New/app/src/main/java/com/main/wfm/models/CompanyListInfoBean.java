package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class CompanyListInfoBean {
    @SerializedName("id")
    private int id;
    @SerializedName("company_name")
    private String companyName;

    @SerializedName("status")
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
