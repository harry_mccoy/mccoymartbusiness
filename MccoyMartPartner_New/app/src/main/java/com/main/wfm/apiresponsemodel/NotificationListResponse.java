package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationListResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private String statusCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }


    @SerializedName("data")
    private List<NotificationListDataResponse>  notificationList;

    public List<NotificationListDataResponse> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<NotificationListDataResponse> notificationList) {
        this.notificationList = notificationList;
    }
}
