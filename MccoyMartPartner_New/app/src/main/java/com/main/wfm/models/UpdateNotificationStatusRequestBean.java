package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class UpdateNotificationStatusRequestBean {
    @SerializedName("notification_id")
    private int notificationId;

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }
}
