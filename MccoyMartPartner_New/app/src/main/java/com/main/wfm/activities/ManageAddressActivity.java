package com.main.wfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.AddressListAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.ManageAddressItemResponse;
import com.main.wfm.apiresponsemodel.ManageAddressResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.VerticalLineDecorator;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class ManageAddressActivity extends AppCompatActivity {

    private AddressListAdapter adapter;
    private List<ManageAddressItemResponse> addressList;
    private RecyclerView rvAddressList;
    private AppSession appSession;
    private String addressType;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSession=new AppSession(ManageAddressActivity.this);
        setContentView(R.layout.activity_manage_address);

        ((ImageView) findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addressType=getIntent().getStringExtra("addressType");
        if(addressType.equalsIgnoreCase("changeAddress")){

            ((TextView)findViewById(R.id.tvTitle)).setText("Select Address");
        }else{
            ((TextView)findViewById(R.id.tvTitle)).setText("Manage Address");
        }
        rvAddressList =findViewById(R.id.rvAddressList);
        rvAddressList.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(ManageAddressActivity.this);
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        addressList =new ArrayList<ManageAddressItemResponse>();
        adapter=new AddressListAdapter(ManageAddressActivity.this, addressList,addressType);
        rvAddressList.setAdapter(adapter);
        rvAddressList.addItemDecoration(new VerticalLineDecorator(2));
        rvAddressList.setLayoutManager(MyLayoutManager);

        ((TextView)findViewById(R.id.tvAddNewAddress)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageAddressActivity.this, AddAddressActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        //getWishListApi(getIntent().getStringExtra("productId"));
        getAddressListAPI();
    }

    public void getAddressListAPI() {
        final ViewDialog viewDialog = new ViewDialog(ManageAddressActivity.this);
        viewDialog.showDialog();

        NetworkAPI.getAddressApi(ManageAddressActivity.this, "Bearer " + appSession.getAuthToken(),
                new Dispatch<ManageAddressResponse>() {
                    @Override
                    public void apiSuccess(ManageAddressResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getAddressList() != null) {

                                addressList = body.getAddressList();
                                adapter=new AddressListAdapter(ManageAddressActivity.this, addressList,addressType);
                                rvAddressList.setAdapter(adapter);

                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ManageAddressActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}
