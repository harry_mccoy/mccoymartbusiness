package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class FloatingListInfoBean {
    @SerializedName("title")
    private String title;
    @SerializedName("action")
    private String action;
    @SerializedName("icon")
    private String icon;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
