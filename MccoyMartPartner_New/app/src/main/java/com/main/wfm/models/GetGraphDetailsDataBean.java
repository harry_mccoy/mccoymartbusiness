package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetGraphDetailsDataBean {

    @SerializedName("objLeadGraph")
    private List<Integer> leadGraphList;

    @SerializedName("objProfileViewGraph")
    private List<Integer> profileViewList;

    public List<Integer> getLeadGraphList() {
        return leadGraphList;
    }

    public void setLeadGraphList(List<Integer> leadGraphList) {
        this.leadGraphList = leadGraphList;
    }

    public List<Integer> getProfileViewList() {
        return profileViewList;
    }

    public void setProfileViewList(List<Integer> profileViewList) {
        this.profileViewList = profileViewList;
    }
}
