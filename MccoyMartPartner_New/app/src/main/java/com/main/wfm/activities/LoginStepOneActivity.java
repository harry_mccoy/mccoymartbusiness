package com.main.wfm.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.models.LoginRequestBean;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.Permissions;
import com.main.wfm.utils.ViewDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginStepOneActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvContinue;
    private EditText etMobile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_stepone);
        getIds();
    }

    private void getIds() {
        tvContinue = findViewById(R.id.tvContinue);
        etMobile = findViewById(R.id.etMobile);
        tvContinue.setOnClickListener(this);


        etMobile.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvContinue:

                if (Permissions.checkPermissionReadSMS(LoginStepOneActivity.this)) {
                    if (etMobile.length() > 0) {

                        sendOtpAPI();
                       /* Intent intent = new Intent(LoginStepOneActivity.this, LoginStepTwoActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("username",etMobile.getText().toString().trim());
                        startActivity(intent);*/


                    } else {
                        CommonUtils.showAlertOk("Phone number can not be left blank.", LoginStepOneActivity.this);
                    }

                } else {
                    Permissions.requestPermissionReadSMS(LoginStepOneActivity.this);
                }


                break;
        }
    }

    private void sendOtpAPI() {

        //final ProgressDialog dialog = ProgressDialog.show(SelectOptionActivity.this, null, getString(R.string.loading));
        final ViewDialog viewDialog = new ViewDialog(this);
        viewDialog.showDialog();
        LoginRequestBean request = new LoginRequestBean();


        request.setDeviceType("2");
        request.setUsername(etMobile.getText().toString().trim());
        request.setAction("signup");
        Call<JsonResponse> call = ApiExecutor.getApiService(LoginStepOneActivity.this).sendOtpApi(request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<JsonResponse>() {
                         @Override
                         public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Send Devi e Token " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     Intent intent = new Intent(LoginStepOneActivity.this, LoginStepTwoActivity.class);
                                     intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                     intent.putExtra("username",etMobile.getText().toString().trim());
                                     startActivity(intent);
                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(),LoginStepOneActivity.this);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<JsonResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }
}
