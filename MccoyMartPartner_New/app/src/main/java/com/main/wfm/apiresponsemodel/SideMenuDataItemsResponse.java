package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;
import com.main.wfm.models.DashboardDetailsDataBean;

public class SideMenuDataItemsResponse {
    @SerializedName("name")
    private String name;

    private boolean isSectionHeader;

    @SerializedName("category_id")
    private String category_id;

    public String isName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String isCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public boolean isSectionHeader() {
        return isSectionHeader;
    }

    public void setSectionHeader(boolean sectionHeader) {
        isSectionHeader = sectionHeader;
    }
}
