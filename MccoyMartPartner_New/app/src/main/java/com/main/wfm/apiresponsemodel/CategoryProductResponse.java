package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryProductResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;


    @SerializedName("statusCode")
    private String statusCode;

    @SerializedName("data")
    private List<CategoryProductDataResponse> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<CategoryProductDataResponse> getData() {
        return data;
    }

    public void setData(List<CategoryProductDataResponse> data) {
        this.data = data;
    }
}
