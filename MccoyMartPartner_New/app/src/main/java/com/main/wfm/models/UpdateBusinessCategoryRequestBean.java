package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateBusinessCategoryRequestBean {
    @SerializedName("company_id")
    private String companyId;
    @SerializedName("categories")
    private List<Integer> categoriesIdList;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public List<Integer> getCategoriesIdList() {
        return categoriesIdList;
    }

    public void setCategoriesIdList(List<Integer> categoriesIdList) {
        this.categoriesIdList = categoriesIdList;
    }
}
