package com.main.wfm.models;

/**
 * Created on 06-06-2019.
 */
public class ReviewsListData {
    /**
     * id : 600
     * company_id : 14373
     * company_name : WFM India
     * contact_person : Rahul Sahani
     * status : Unapproved
     * status_code : 0
     * status_color : #FF0000
     * name : Manoj
     * email : manoj@wfm.co.in
     * comment : fgnf,dgnfd,n,fmsn
     * rating : 0
     * reply :
     * date : 10th May, 2019
     */

    private int id;
    private int company_id;
    private String company_name;
    private String contact_person;
    private String status;
    private int status_code;
    private String status_color;
    private String name;
    private String email;
    private String comment;
    private int rating;
    private String reply;
    private String date;
    private boolean replyEnabled;
    private boolean seemore;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public String getStatus_color() {
        return status_color;
    }

    public void setStatus_color(String status_color) {
        this.status_color = status_color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isReplyEnabled() {
        return replyEnabled;
    }

    public void setReplyEnabled(boolean replyEnabled) {
        this.replyEnabled = replyEnabled;
    }

    public boolean isSeemore() {
        return seemore;
    }

    public void setSeemore(boolean seemore) {
        this.seemore = seemore;
    }
}
