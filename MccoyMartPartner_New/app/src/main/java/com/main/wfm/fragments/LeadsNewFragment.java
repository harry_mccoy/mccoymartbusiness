package com.main.wfm.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.LeadsAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.LeadsRequestModel;
import com.main.wfm.apiresponsemodel.LeadsDataListResponse;
import com.main.wfm.apiresponsemodel.LeadsResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.LoadLeadsEvent;
import com.main.wfm.utils.VerticalLineDecorator;
import com.main.wfm.utils.ViewDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class LeadsNewFragment extends Fragment  {
private Context mContext;

private int position;

    private String search="",filter="",leadType="new";
    private RecyclerView rvLeads;
    private AppSession appSession;

    private List<LeadsDataListResponse> ObjData;
    private LeadsAdapter adapter;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_leads, container, false);
        appSession=new AppSession(mContext);

        rvLeads=root.findViewById(R.id.rvLeads);

        rvLeads.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ObjData=new ArrayList<LeadsDataListResponse>();
        adapter=new LeadsAdapter(mContext,ObjData);
        rvLeads.setAdapter(adapter);
        rvLeads.addItemDecoration(new VerticalLineDecorator(2));
        rvLeads.setLayoutManager(MyLayoutManager);

        getLeadsDataAPI("active","","");
        return root;
    }
    public LeadsNewFragment(int position) {
       this.position=position;
    }

    public LeadsNewFragment() {

    }

    public static LeadsNewFragment newInstance(int position) {
        LeadsNewFragment f = new LeadsNewFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext=context;

    }


    public void getLeadsDataAPI(String leadsType,String filter,String searchText) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        LeadsRequestModel request =new LeadsRequestModel();
        request.setFilter(filter);
        request.setSearch(searchText);
        request.setType(leadsType);
        System.out.println("Aouth Token: "+appSession.getAuthToken());
        NetworkAPI.getLeadsApi(mContext, "Bearer " + appSession.getAuthToken(),request,
                new Dispatch<LeadsResponse>() {

                    @Override
                    public void apiSuccess(LeadsResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));
                        viewDialog.hideDialog();
                        switch (body.getStatusCode()) {
                            case 200:
                                ObjData = body.getLeadsData().getObjData();
                                //adapter.notifyDataSetChanged();

                                adapter=new LeadsAdapter(mContext,ObjData);
                                rvLeads.setAdapter(adapter);
                                break;
                            /*case 422:
                                JsonError error = ErrorUtils.parseError(body);
                                //CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                break;
                            case 401:
                                error = ErrorUtils.parseError(response);
                                //CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                break;*/
                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    @Subscribe
    public void getSearch(LoadLeadsEvent events) {
        System.out.println("LeadsNewFragment");

            getLeadsDataAPI(events.getLeadType(),events.getSearch(),events.getFilter());


        }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
}