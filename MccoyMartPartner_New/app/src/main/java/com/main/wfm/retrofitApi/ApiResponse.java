package com.main.wfm.retrofitApi;



/**
 * Created by RSShah on 12/01/2016.
 */
public class ApiResponse {

    public String responseCode;
    public String responseMessage;
    public int stackholder_id;
    public int daily_report_id;
    public int stackholder_locked_id;
}