package com.main.wfm.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.main.wfm.R;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductsAdapter extends BaseAdapter {

    private static final String TAG = ProductsAdapter.class.getSimpleName();
    private Context context;
    private List<ProductDataDashboardShopBean> arlProductDatas;
    private LayoutInflater infalter;


    public ProductsAdapter(Context context, List<ProductDataDashboardShopBean> arlProductDatas) {
        this.context = context;
        this.arlProductDatas = arlProductDatas;
        infalter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {

        return arlProductDatas.size();
    }

    @Override
    public Object getItem(int position) {

        return arlProductDatas.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = infalter.inflate(R.layout.row_product_datas, null);
            holder = new ViewHolder();
            holder.tvItemName = (TextView) convertView.findViewById(R.id.tvItemName);
            holder.ivProductPic = (ImageView) convertView.findViewById(R.id.ivProductPic);
            holder.llParent=convertView.findViewById(R.id.llParent);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(arlProductDatas!=null&& arlProductDatas.size()>0){
            System.out.println("LIST arlProductDatas: "+arlProductDatas.get(position).getName()+" Size: "+arlProductDatas.get(position).getImage());

            holder.tvItemName.setText(arlProductDatas.get(position).getName());
            Picasso.with(context).load(arlProductDatas.get(position).getImage())
                    .placeholder(context.getResources().getDrawable(R.drawable.upvc_hardware)).into(holder.ivProductPic);

        }


        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("product_id",arlProductDatas.get(position).getProduct_id());
                intent.putExtra("product_name",arlProductDatas.get(position).getManufacturer());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    public class ViewHolder {
        TextView tvItemName;
        ImageView ivProductPic;
        private LinearLayout llParent;
    }
}
