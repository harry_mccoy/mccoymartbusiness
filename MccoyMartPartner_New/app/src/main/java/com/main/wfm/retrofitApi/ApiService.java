package com.main.wfm.retrofitApi;

import androidx.annotation.NonNull;

import com.main.wfm.apirequestmodel.AddToCartRequest;
import com.main.wfm.apirequestmodel.LeadsRequestModel;
import com.main.wfm.apiresponsemodel.LeadsDetailsResponse;
import com.main.wfm.apiresponsemodel.LeadsResponse;
import com.main.wfm.apiresponsemodel.SideMenuResponse;
import com.main.wfm.models.DashBoardShopRequestBean;
import com.main.wfm.models.DashboardDetailsShopResponse;
import com.main.wfm.models.GetProductOfferRequestBean;
import com.main.wfm.models.LeadStatusUpdateRequestBean;
import com.main.wfm.models.LeadStatusUpdateResponse;
import com.main.wfm.models.LoginRequestBean;
import com.main.wfm.models.OtpVerificationRequestBean;
import com.main.wfm.models.OtpVerificationResponse;
import com.main.wfm.models.ProductInfoResponseBean;
import com.main.wfm.models.SignUpRequestBean;
import com.main.wfm.models.SignUpResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by RSShah on 06/14/2016.
 */
public interface ApiService {

    @POST("send-otp")
    @Headers( {"content-type: application/json", })
    Call<JsonResponse> sendOtpApi(@Body LoginRequestBean body);

    @POST("signup")
    Call<SignUpResponse> SignUpApi(@Body SignUpRequestBean body);

    @POST("verify-otp")
        Call<OtpVerificationResponse> OTPVerficationApi(@Body OtpVerificationRequestBean body);


    @POST("homeModuleList")
    Call<DashboardDetailsShopResponse> dashBoardShopApi(@Body DashBoardShopRequestBean body);

    @POST("getofferproducts")
    Call<ProductInfoResponseBean> getProductOffersApi(@Body GetProductOfferRequestBean body);

    @POST("lead/1")
    Call<LeadsResponse> getLeadsApi(@Header("Authorization") String authorization,@Body LeadsRequestModel body);

    @POST("lead")
    Call<LeadsDetailsResponse> getLeadsDetailsApi(@Header("Authorization") String authorization, @Body LeadsRequestModel body);

    @NonNull
    @GET("navigation")
    @Headers( {"content-type: application/json",})
    Call<SideMenuResponse> getSideMenuListApi(@Header("Authorization") String authorization);

    @POST("lead-update/")
    Call<LeadStatusUpdateResponse> updateLeadStatusApi(@Body LeadStatusUpdateRequestBean body);

    @NonNull
    @POST("check_pincode")
    Call<JsonResponse> checkDeliveryApi(@Header("Authorization") String authorization, @Body AddToCartRequest body);
}

