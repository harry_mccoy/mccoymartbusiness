package com.main.wfm.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.main.wfm.R;
import com.main.wfm.activities.DashBoardActivity;
import com.main.wfm.activities.LoginStepOneActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class CommonUtils {

    private static final String TAG = CommonUtils.class.getSimpleName();
    public static boolean accept;
    private static ProgressDialog dialogProgress;
    private static Dialog dialog;
    //2016-04-08T12:18:06.523Z
    public final static String SERVICE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public final static String DISPLAY_DATE = "dd MMM, hh:mm aaa";


    public final static String DISPLAY_DATE_FORMAT = "yyyy-MM-dd";

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Uri getAudioContentUri(Context context, File audioFile) {
        String filePath = audioFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            @SuppressWarnings("HardCodedStringLiteral") Uri baseUri = Uri.parse("content://media/external/images/media"); //NON-NLS
            //noinspection HardCodedStringLiteral

            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (audioFile.exists()) {
                //noinspection HardCodedStringLiteral

                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    public static <T> boolean CheckIfNull(T objectToCheck) {
        return objectToCheck == null ? true : false;
    }

    public static String getDateOfTimestamp(long timeStamp) {
        java.text.DateFormat objFormatter = new SimpleDateFormat("MM-dd-yyy");

        Calendar objCalendar = Calendar.getInstance();

        objCalendar.setTimeInMillis(timeStamp * 1000);//edit
        String result = objFormatter.format(objCalendar.getTime());
        objCalendar.clear();
        return result;
    }

    public static String getImeiNumber(Activity activity) {
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") String imei_number = telephonyManager.getDeviceId();
        Log.e(TAG, "IMEI NUMBER<><<><><>:" + imei_number);
        return imei_number;
    }

    public static int getIntPreferences(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(key, 0);
    }

    public static void showAlertOk(String message, Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)

                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void showAlertWithTitleOk(String title, String message, final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                Intent intent = new Intent(context, DashBoardActivity.class);
                                context.startActivity(intent);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.finish();
                            }
                        });

        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ProgressDialog(String tittle, String message, Activity context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set title
        alertDialogBuilder.setTitle(tittle);
        // set dialog message
        alertDialogBuilder.setMessage(message).setCancelable(false);
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
        // After some action
        alertDialog.dismiss();
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void setTextOnView(@Nullable TextView tvView, @Nullable String value) {
        tvView.setText(value);
    }

    public static void savePreferencesString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void savePreferencesBoolean(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getPreferencesBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(key, false);
    }

    public static String getPreferencesString(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, "");
    }

    public static void removePreferences(Activity context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.remove(key);
    }

    public static String getTime(String timestamp_in_string) {
        long dv = Long.valueOf(timestamp_in_string) * 1000L;// its need to be in millisecond
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        cal.setTimeInMillis(dv);
        String date = DateFormat.format("hh:mm a", cal).toString();
        return date;
    }

    public static String bitMapToString(@NonNull Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }



    public static String getDateDiffString(String startDate, String endDate) {
        java.text.DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
        try {
            Date date1 = (Date) formatter.parse(startDate);
            Date date2 = (Date) formatter.parse(endDate);
            long diff = date2.getTime() - date1.getTime();
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            return String.valueOf(days);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void setFragment(Fragment fragment, boolean removeStack, FragmentActivity activity, int mContainer) {

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ftTransaction = fragmentManager.beginTransaction();
        if (removeStack) {
            int size = fragmentManager.getBackStackEntryCount();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            ftTransaction.replace(mContainer, fragment);
        } else {
            ftTransaction.replace(mContainer, fragment);
            ftTransaction.addToBackStack(null);
        }
        ftTransaction.commit();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Called for checking internet connection
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void disMissProgressDialog(Context mContext) {
        if (dialogProgress != null) {
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    /*public static void displayProgressDialog(Context mContext, String message) {
        dialogProgress = new ProgressDialog(mContext);
        dialogProgress.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialogProgress.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.my_progress_indeterminate));
        dialogProgress.show();
        dialogProgress.setCancelable(true);
    }*/




    public static void showProgressDialog(Context context, String msg) {
        if (context != null) {
            dialogProgress = new ProgressDialog(context);
            dialogProgress.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            //   dialogProgress.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.my_progress_indeterminate));
            dialogProgress.setMessage(msg);
            if (!dialogProgress.isShowing()) {
                try {
                    dialogProgress.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            dialogProgress.setCancelable(false);

        }
    }

    public static void printLog(String tag, String msg) {
        Log.e(tag, msg);
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static String getDate(String serverDate) {
        try {
            long dt = Long.parseLong(serverDate);
            dt = dt * 1000;
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(dt);
            String date = DateFormat.format("EEE, dd MMM yyyy", cal).toString();
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    public static String getDateTime(String serverDate) {
        try {
            long dt = Long.parseLong(serverDate);
            dt = dt * 1000;
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(dt);
            String date = DateFormat.format("dd-MM-yyyy HH:mm:ss", cal).toString();
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    public Date dateConverter(String date1) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateInString = date1;
        Date date;
        try {
            date = formatter.parse(dateInString);
            System.out.println(date);
            System.out.println(formatter.format(date));
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public final static boolean isValidPhone(CharSequence target) {
        if (target == null) {
            return false;
        } else {

            return android.util.Patterns.PHONE.matcher(target)
                    .matches() && (target.length() >= 7 && target.length() <= 20);
        }
    }

    /*To hide keyboard*/
    public static void hideKeyPad(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
    public static Typeface setCustomFont(Context mContext, String fontName) {
        Typeface font = null;
        try {
            font = Typeface.createFromAsset(mContext.getAssets(), fontName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return font;

    }

    public static String getDateFormat(String serverDate) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat(SERVICE_DATE_FORMAT, Locale.ENGLISH);
            originalFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = originalFormat.parse(serverDate);

            SimpleDateFormat targetFormat = new SimpleDateFormat(DISPLAY_DATE_FORMAT);
            targetFormat.setTimeZone(TimeZone.getDefault());
            String formattedDate = targetFormat.format(date);

            return formattedDate;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void logOutAlert(final Activity activity, String message, final AppSession appSession){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //appSession.setSession("",false);
                        //appSession.setOfferType("All");
                        //appSession.setUserData("","","","","");
                        appSession.setUserId("");
                        appSession.setCartCount(0);
                        Intent intent = new Intent(activity, LoginStepOneActivity.class);
                        activity.startActivity(intent);
                        activity.finish();

                    }
                }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


            }});

        builder.show();
    }


    public static long getTimestamp(String strDate){

        long timestamp=0;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat(SERVICE_DATE_FORMAT);
            Date date = (Date)formatter.parse(strDate);
            timestamp = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;

    }

    public static String getDateTimeFromTimestamp(long timestamp, String format) {
        try {

            Calendar cal = Calendar.getInstance(Locale.getDefault());
            cal.setTimeInMillis(timestamp);
            String date = DateFormat.format(format, cal).toString();
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }



    public static void clearNotification(int notificationID, Context mContext){
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationID);
    }

    public static String getCurrentDate(){
        Date c = Calendar.getInstance().getTime();
       // System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        return formattedDate;
    }
public static long getCurrentTimeOnhour(){
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
    String currentDateandTime = sdf.format(new Date());

    Date date = null;
    try {
        date = sdf.parse(currentDateandTime);
    } catch (ParseException e) {
        e.printStackTrace();
    }
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.HOUR, 1);
    return calendar.getTimeInMillis();
}
    public static String changeTime(String timeFroServer){
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(timeFroServer);
            System.out.println(dateObj);
            return new SimpleDateFormat("hh:mm aa").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
            return "";
        }
       /* String startTime = CommonUtils.getCurrentDate()+" " +timeFroServer+":00" ;
        System.out.println("Current time => " + startTime);
        StringTokenizer tk = new StringTokenizer(startTime);
        String date = tk.nextToken();
        String time = tk.nextToken();
        System.out.println("Current time Token => " + time);

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

            SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm aa");
            Date dt;
            try {
                dt = sdf.parse(time);
                System.out.println("Time Display: " + sdfs.format(dt)); // <-- I got result here
                return sdfs.format(dt);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        return "";*/
    }
public static void shakeAnimation(Context mContext,View view){
    Animation shake;
    shake = AnimationUtils.loadAnimation(mContext.getApplicationContext(), R.anim.shake);
    view.startAnimation(shake); // starts animation
}
}