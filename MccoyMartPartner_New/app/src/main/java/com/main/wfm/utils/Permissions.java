package com.main.wfm.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


/**
 * Created by RSShah on 25-01-2017.
 */

public class Permissions {

//Permission for Location
    public static boolean checkPermissionLocation(Activity activity){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }
    public static void requestPermissionLocation(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.PERMISSION_REQUEST_CODE_LOCATION);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.PERMISSION_REQUEST_CODE_LOCATION);
        }
    }


    //Permission for Account
    public static boolean checkPermissionForAccount(Activity activity){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }
    public static void requestPermissionForAccount(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.GET_ACCOUNTS)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.GET_ACCOUNTS}, AppConstants.PERMISSION_REQUEST_CODE_ACCOUNT);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.GET_ACCOUNTS}, AppConstants.PERMISSION_REQUEST_CODE_ACCOUNT);
        }
    }


    //Permission for Storage
    public static  boolean checkPermissionStorage(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result  == PackageManager.PERMISSION_GRANTED){

            if(result1 == PackageManager.PERMISSION_GRANTED){
                return true;
            }else {
                return false;
            }
        } else {
            return false;

        }
    }
    public static   void requestPermissionStorage( Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)|| ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.PERMISSION_REQUEST_CODE_STORAGE);
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppConstants.PERMISSION_REQUEST_CODE_STORAGE);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.PERMISSION_REQUEST_CODE_STORAGE);
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},AppConstants.PERMISSION_REQUEST_CODE_STORAGE);
        }
    }


    //Permission for Camera
    public static boolean checkPermissionCamera(Activity activity){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        if (result  == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }
    public static  void requestPermissionCamera(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, AppConstants.PERMISSION_REQUEST_CODE_CAMERA);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CAMERA},AppConstants.PERMISSION_REQUEST_CODE_CAMERA);
        }
    }


    //Permission for Calendar
    public static  boolean checkPermissionCalendar(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR);
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR);
        if (result  == PackageManager.PERMISSION_GRANTED){

            if(result1 == PackageManager.PERMISSION_GRANTED){
                return true;
            }else {
                return false;
            }


        } else {
            return false;

        }
    }
    public static   void requestPermissionCalendar( Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_CALENDAR)|| ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CALENDAR)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_CALENDAR}, AppConstants.PERMISSION_REQUEST_CODE_CALENDAR);
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CALENDAR}, AppConstants.PERMISSION_REQUEST_CODE_CALENDAR);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WRITE_CALENDAR}, AppConstants.PERMISSION_REQUEST_CODE_CALENDAR);
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_CALENDAR},AppConstants.PERMISSION_REQUEST_CODE_CALENDAR);
        }
    }


//Permission for Contact
    public static boolean checkPermissionContact(Activity activity){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }
    public static void requestPermissionContact(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CONTACTS)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, AppConstants.PERMISSION_REQUEST_CODE_CONTACT);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, AppConstants.PERMISSION_REQUEST_CODE_CONTACT);
        }
    }

    //For Call
    public static boolean checkPermissionCall(Activity activity){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }
    public static void requestPermissionCall(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, AppConstants.PERMISSION_REQUEST_CODE_CALL);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, AppConstants.PERMISSION_REQUEST_CODE_CALL);
        }
    }

    //For Telephone
    public static  boolean checkPermissionCallLogs(Activity context){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG);
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALL_LOG);
        if (result  == PackageManager.PERMISSION_GRANTED){

            if(result1 == PackageManager.PERMISSION_GRANTED){
                return true;
            }else {
                return false;
            }


        } else {
            return false;
        }
    }
    public static   void requestPermissionCallLogs( Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_CALL_LOG)|| ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CALL_LOG)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_CALL_LOG}, AppConstants.PERMISSION_REQUEST_CODE_CALL_LOG);
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CALL_LOG}, AppConstants.PERMISSION_REQUEST_CODE_CALL_LOG);
        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WRITE_CALL_LOG}, AppConstants.PERMISSION_REQUEST_CODE_CALL_LOG);
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.READ_CALL_LOG},AppConstants.PERMISSION_REQUEST_CODE_CALL_LOG);
        }
    }

    //For Read SMS
    public static boolean checkPermissionReadSMS(Activity activity){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.RECEIVE_SMS);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {
            return false;

        }
    }
    public static void requestPermissionReadSMS(Activity activity){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.RECEIVE_SMS)){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECEIVE_SMS}, AppConstants.PERMISSION_REQUEST_CODE_READ_SMS);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECEIVE_SMS}, AppConstants.PERMISSION_REQUEST_CODE_READ_SMS);
        }
    }

}
