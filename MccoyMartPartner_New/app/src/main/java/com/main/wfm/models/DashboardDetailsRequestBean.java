package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class DashboardDetailsRequestBean {
    @SerializedName("company_id")
    private String companyId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
