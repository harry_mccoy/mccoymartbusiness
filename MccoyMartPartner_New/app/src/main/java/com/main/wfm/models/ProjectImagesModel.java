package com.main.wfm.models;

import java.io.Serializable;

/**
 * Created on 28-05-2019.
 */
public class ProjectImagesModel implements Serializable {
    /**
     * project_image_id : 128
     * project_id : 43
     * image_title : dsdsd
     * description : sdsdddddddddddddddddddddddd
     * category_id : 328
     * category_name : Architectural Glass
     * image_path : https://uat.mccoymart.com/uploads/projects/2019/5/1558696213755-project-faridabad.jpg
     * status : draft
     */

    private int project_image_id;
    private int project_id;
    private String image_title;
    private String description;
    private int category_id;
    private String category_name;
    private String image_path;
    private String status;

    public int getProject_image_id() {
        return project_image_id;
    }

    public void setProject_image_id(int project_image_id) {
        this.project_image_id = project_image_id;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public String getImage_title() {
        return image_title;
    }

    public void setImage_title(String image_title) {
        this.image_title = image_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
