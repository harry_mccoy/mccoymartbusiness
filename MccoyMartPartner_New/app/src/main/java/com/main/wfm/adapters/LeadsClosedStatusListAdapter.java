package com.main.wfm.adapters;

import android.content.Context;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.main.wfm.R;

import com.main.wfm.models.LeadStatusSubPopUpDataBean;

import java.util.List;



import static androidx.core.content.ContextCompat.getColor;

public class LeadsClosedStatusListAdapter extends RecyclerView.Adapter<LeadsClosedStatusListAdapter.MyViewHolder> {
    private Context context;
    private List<LeadStatusSubPopUpDataBean> leadStatusSubPopUpDataBeanList;
    private LeadsclosedStatusAdapterListener listener;
    private PopupWindow dialog;


    public LeadsClosedStatusListAdapter(Context context, List<LeadStatusSubPopUpDataBean> leadStatusSubPopUpDataBeanList, LeadsclosedStatusAdapterListener listener, PopupWindow dialog ) {
        this.context = context;
        this.leadStatusSubPopUpDataBeanList = leadStatusSubPopUpDataBeanList;
        this.listener = listener;
        this.dialog=dialog;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leads_filter_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (!leadStatusSubPopUpDataBeanList.isEmpty() && leadStatusSubPopUpDataBeanList.get(position) != null) {

            LeadStatusSubPopUpDataBean bean = leadStatusSubPopUpDataBeanList.get(position);

            holder.rl_FilterMainLayout.setTag(position);

            if (!TextUtils.isEmpty(bean.getText())) {
                holder.tv_filterTitle.setText(bean.getText().trim());
            }
            if (bean.getValue().equalsIgnoreCase("1")) {
                holder.iv_filterChecked.setVisibility(View.VISIBLE);
                holder.tv_filterTitle.setTextColor(getColor(context, R.color.white));
                holder.rl_FilterMainLayout.setBackgroundColor(getColor(context, R.color.nav_bar));
            } else {
                holder.iv_filterChecked.setVisibility(View.GONE);
                holder.tv_filterTitle.setTextColor(getColor(context, R.color.nav_bar));
                holder.rl_FilterMainLayout.setBackgroundColor(getColor(context, R.color.white));
            }
        }
    }


    @Override
    public int getItemCount() {
        return leadStatusSubPopUpDataBeanList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_filterTitle;
        public ImageView iv_filterChecked;
        public RelativeLayout rl_FilterMainLayout;



        public MyViewHolder(View view) {
            super(view);
            tv_filterTitle=view.findViewById(R.id.tv_filterTitle);
            iv_filterChecked=view.findViewById(R.id.iv_filterChecked);
            rl_FilterMainLayout=view.findViewById(R.id.rl_FilterMainLayout);

            rl_FilterMainLayout.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            dialog.dismiss();
            int position = (int) rl_FilterMainLayout.getTag();
            if (v.getId() == R.id.rl_FilterMainLayout) {
                LeadStatusSubPopUpDataBean mBean = leadStatusSubPopUpDataBeanList.get(position);

                listener.onClosedStatusItemSelected(leadStatusSubPopUpDataBeanList.get(getAdapterPosition()),position);
                notifyDataSetChanged();

            }
        }
    }

    public interface LeadsclosedStatusAdapterListener {
        void onClosedStatusItemSelected(LeadStatusSubPopUpDataBean bean, int position);
    }
}
