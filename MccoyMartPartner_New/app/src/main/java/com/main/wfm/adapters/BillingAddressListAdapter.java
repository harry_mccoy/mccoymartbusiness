package com.main.wfm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.CartActivity;
import com.main.wfm.activities.ManageAddressActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apiresponsemodel.ManageAddressItemResponse;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.List;

public class BillingAddressListAdapter extends RecyclerView.Adapter<BillingAddressListAdapter.MyViewHolder> {
    private List<ManageAddressItemResponse> addressList;
    private Context mContext;
    private String addressType;
    private AppSession appSession;

    private boolean[]radioPosition;
    private boolean isDeliveryAddress;

    public BillingAddressListAdapter(Context mContext, List<ManageAddressItemResponse> adressList, String addressType) {
        this.addressList = adressList;
        this.mContext = mContext;
        this.addressType=addressType;
        radioPosition=new boolean[this.addressList.size()];
        //radioPosition[0]=true;
        appSession=new AppSession(this.mContext);

    }

    @Override
    public BillingAddressListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_select_address, parent, false);
        BillingAddressListAdapter.MyViewHolder holder = new BillingAddressListAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final BillingAddressListAdapter.MyViewHolder holder, final int position) {


       /* if (addressList.get(position).getFirstname()!=null&&addressList.get(position).getFirstname().length()>0 &&
                addressList.get(position).getLastname()!=null&&addressList.get(position).getLastname().length()>0){
            holder.tvName.setText(addressList.get(position).getFirstname());
        }else if(addressList.get(position).getFirstname()!=null&&addressList.get(position).getFirstname().length()>0){
            holder.tvName.setText(addressList.get(position).getFirstname()+" "+addressList.get(position).getLastname());
        }

        *//*if(addressList.get(position).getAddress_1()!=null&&addressList.get(position).getAddress_1().length()>0&&
                addressList.get(position).getAddress_2()!=null&&addressList.get(position).getAddress_2().length()>0&&
                addressList.get(position).getAddress_1()!=null&&addressList.get(position).get.length()>0){
            holder.tvAddress.setText(addressList.get(position).getAddress_1());
        }*//*

        String strAddress=addressList.get(position).getCompany()+", "+addressList.get(position).getAddress_1()+"\n"+addressList.get(position).getAddress_2()+"\n"+
                addressList.get(position).getCity()+" "+addressList.get(position).getPostcode();

        holder.tvAddress.setText(strAddress);*/
        holder.ivMore.setVisibility(View.GONE);
        holder.cbBillAddress.setVisibility(View.GONE);
        holder.radioSaveAddress.setVisibility(View.VISIBLE);
        holder.tvDeliverHere.setText("Bill Here");
        if(radioPosition[position]){
            holder.radioSaveAddress.setChecked(true);
            holder.tvDeliverHere.setVisibility(View.VISIBLE);

        }else {
            holder.radioSaveAddress.setChecked(false);
            holder.tvDeliverHere.setVisibility(View.GONE);

        }


        holder.radioSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<radioPosition.length;i++){
                    if(i==position){
                        radioPosition[i]=true;
                    }else{
                        radioPosition[i]=false;
                    }
                }
                notifyDataSetChanged();
            }
        });




        holder.tvDeliverHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartActivity.billingAddress.setAddress(addressList.get(position).getAddress_1());
                    Intent intent = new Intent(mContext, CartActivity.class);
                    intent.putExtra("addressId", addressList.get(position).getAddress_id());
                    intent.putExtra("customerId", addressList.get(position).getCustomer_id());
                    intent.putExtra("action", "edit");
                    intent.putExtra("addressType", "Select Billing Address");
                    mContext.startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {
        return addressList.size();
        //return 20;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAddress, tvMobile,tvEdit,tvRemove,tvName,tvDeliverHere;
        public LinearLayout llEditRow;
        public ImageView ivMore;
        public FrameLayout flParent;
        public RadioButton radioSaveAddress;
        public CheckBox cbBillAddress;


        public MyViewHolder(View v) {
            super(v);
            radioSaveAddress=v.findViewById(R.id.radioSaveAddress);
            tvDeliverHere=v.findViewById(R.id.tvDeliverHere);
            tvAddress = (TextView) v.findViewById(R.id.tvAddress);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvMobile = (TextView) v.findViewById(R.id.tvMobile);
            tvEdit = (TextView) v.findViewById(R.id.tvEdit);
            tvRemove = (TextView) v.findViewById(R.id.tvRemove);
            ivMore = (ImageView) v.findViewById(R.id.ivMore);
            llEditRow=v.findViewById(R.id.llEditRow);
            flParent=v.findViewById(R.id.flParent);
            cbBillAddress=v.findViewById(R.id.cbBillAddress);

        }
    }

    public void deleteAddressApi(String userId, String addressId) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();

        AddDeleteWishListRequest request = new AddDeleteWishListRequest();
        request.setAddress_id(addressId);
        request.setUser_id(userId);
        NetworkAPI.deleteAddressApi(mContext, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Add Address Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.statusCode.equalsIgnoreCase("200")) {
                                CommonUtils.showToast(mContext, body.message);
                                ((ManageAddressActivity)mContext).getAddressListAPI();
                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, mContext.getResources().getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}