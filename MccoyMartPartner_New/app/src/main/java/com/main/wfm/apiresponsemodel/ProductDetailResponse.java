package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class ProductDetailResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private String statusCode;

    @SerializedName("data")
    private ProductDetailDataResponse productDetailData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ProductDetailDataResponse getProductDetailData() {
        return productDetailData;
    }

    public void setProductDetailData(ProductDetailDataResponse productDetailData) {
        this.productDetailData = productDetailData;
    }
}
