package com.main.wfm.apimodel;


import android.util.Log;


import com.main.wfm.utils.AppConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestCallback<T> implements Callback<T> {
    private Dispatch<T> dispatch;

    public RequestCallback(Dispatch<T> dispatch) {
        this.dispatch = dispatch;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        //CommonMethods.dismissProgressDialog();
        Log.i("RESPONSE", AppConstants.GSON.toJson(response.body()));
        if (response.isSuccessful()) {
            dispatch.apiSuccess(response.body());
        } else {
            try {
               Log.e("RESPONSE", AppConstants.GSON.toJson(response.errorBody()));
                ErrorDTO errorDTO = ErrorUtils.getErrorDetails(response.errorBody().string());
                dispatch.apiError(errorDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        //CommonMethods.dismissProgressDialog();
        if (t != null) {
            dispatch.error(t.getMessage());
        }
    }
}
