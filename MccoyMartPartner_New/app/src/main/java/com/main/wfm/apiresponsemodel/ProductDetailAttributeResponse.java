package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetailAttributeResponse {

    @SerializedName("attribute_group_id")
    private String attribute_group_id;

    @SerializedName("name")
    private String name;


    @SerializedName("attribute")
    private List<ProductDetailAttributeItemResponse> productAttributesItems;

    public String getAttribute_group_id() {
        return attribute_group_id;
    }

    public void setAttribute_group_id(String attribute_group_id) {
        this.attribute_group_id = attribute_group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductDetailAttributeItemResponse> getProductAttributesItems() {
        return productAttributesItems;
    }

    public void setProductAttributesItems(List<ProductDetailAttributeItemResponse> productAttributesItems) {
        this.productAttributesItems = productAttributesItems;
    }
}
