package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class ProductDataInfoDataBean {
    @SerializedName("product_id")
    private String product_id;
    @SerializedName("company_id")
    private String company_id;
    @SerializedName("product_name")
    private String product_name;
    @SerializedName("product_slug")
    private String product_slug;


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_slug() {
        return product_slug;
    }

    public void setProduct_slug(String product_slug) {
        this.product_slug = product_slug;
    }
}
