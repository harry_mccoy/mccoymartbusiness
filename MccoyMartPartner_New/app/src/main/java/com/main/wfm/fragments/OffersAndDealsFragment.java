package com.main.wfm.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.NotificationListAdapter;
import com.main.wfm.adapters.OffersListAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.NotificationListDataResponse;
import com.main.wfm.apiresponsemodel.NotificationListResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.VerticalLineDecorator;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class OffersAndDealsFragment extends Fragment {
    private Context mContext;
    private OffersListAdapter adapter;
    private List<NotificationListDataResponse> notificationList;
    private RecyclerView rvOffers;
    private AppSession appSession;
    private LinearLayout llEmptyNotificationFrame;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_offer, container, false);
        getIds(root);
        return root;
    }

    private void getIds(View root) {
        rvOffers =root.findViewById(R.id.rvOffers);
        llEmptyNotificationFrame=root.findViewById(R.id.llEmptyNotificationFrame);

        appSession=new AppSession(mContext);
        rvOffers.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        notificationList=new ArrayList<NotificationListDataResponse>();
        adapter=new OffersListAdapter(mContext,notificationList);
        rvOffers.setAdapter(adapter);
        rvOffers.addItemDecoration(new VerticalLineDecorator(2));
        rvOffers.setLayoutManager(MyLayoutManager);

        getOfferListResponse();
    }


    public void getOfferListResponse() {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();


        NetworkAPI.getOfferListResponse(mContext, "Bearer " + appSession.getAuthToken(),
                new Dispatch<NotificationListResponse>() {
                    @Override
                    public void apiSuccess(NotificationListResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getNotificationList() != null) {
                               notificationList=body.getNotificationList();
                               if (notificationList!=null&&notificationList.size()>0){
                                   rvOffers.setVisibility(View.VISIBLE);
                                   llEmptyNotificationFrame.setVisibility(View.GONE);
                                   adapter=new OffersListAdapter(mContext,notificationList);
                                   rvOffers.setAdapter(adapter);
                               }else{
                                   rvOffers.setVisibility(View.GONE);
                                   llEmptyNotificationFrame.setVisibility(View.VISIBLE);
                               }


                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    public OffersAndDealsFragment() {


    }

    public static OffersAndDealsFragment newInstance(int position) {
        OffersAndDealsFragment f = new OffersAndDealsFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

}