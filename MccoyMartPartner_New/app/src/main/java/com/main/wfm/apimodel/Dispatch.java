package com.main.wfm.apimodel;


/**
 * Created by ravi.shah
 */
public interface Dispatch<T> {
    void apiSuccess(T body);
    void apiError(ErrorDTO errorDTO);
    void error(String error);
}
