package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashboardDetailsDataBean {

    @SerializedName("objCount")
    private List<DashboardCountDataBean> dashboardCountDataBeanList;

    @SerializedName("objRecentLead")
    private List<DashboardRecentLeadsDataBean> dashboardRecentLeadsDataBeanList;

    @SerializedName("objCompanyProfile")
    private DashboardCompanyProfileDataBean dashboardCompanyProfileDataBean;

    @SerializedName("objYear")
    private List<String> dashboardGraphYearList;

    public List<DashboardCountDataBean> getDashboardCountDataBeanList() {
        return dashboardCountDataBeanList;
    }

    public void setDashboardCountDataBeanList(List<DashboardCountDataBean> dashboardCountDataBeanList) {
        this.dashboardCountDataBeanList = dashboardCountDataBeanList;
    }

    public List<DashboardRecentLeadsDataBean> getDashboardRecentLeadsDataBeanList() {
        return dashboardRecentLeadsDataBeanList;
    }

    public void setDashboardRecentLeadsDataBeanList(List<DashboardRecentLeadsDataBean> dashboardRecentLeadsDataBeanList) {
        this.dashboardRecentLeadsDataBeanList = dashboardRecentLeadsDataBeanList;
    }

    public DashboardCompanyProfileDataBean getDashboardCompanyProfileDataBean() {
        return dashboardCompanyProfileDataBean;
    }

    public void setDashboardCompanyProfileDataBean(DashboardCompanyProfileDataBean dashboardCompanyProfileDataBean) {
        this.dashboardCompanyProfileDataBean = dashboardCompanyProfileDataBean;
    }

    public List<String> getDashboardGraphYearList() {
        return dashboardGraphYearList;
    }

    public void setDashboardGraphYearList(List<String> dashboardGraphYearList) {
        this.dashboardGraphYearList = dashboardGraphYearList;
    }
}
