package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class ForceupdateDataBean {
    @SerializedName("play-store-version-android")
    private int playStoreVersion;
    @SerializedName("is_forceupdate")
    private int is_forceUpdate;

    public int getPlayStoreVersion() {
        return playStoreVersion;
    }

    public void setPlayStoreVersion(int playStoreVersion) {
        this.playStoreVersion = playStoreVersion;
    }

    public int getIs_forceUpdate() {
        return is_forceUpdate;
    }

    public void setIs_forceUpdate(int is_forceUpdate) {
        this.is_forceUpdate = is_forceUpdate;
    }
}
