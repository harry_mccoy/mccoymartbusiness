package com.main.wfm.models;

/**
 * Created on 06-05-2019.
 */
public class ProjectListDataBean {

    /**
     * id : 42
     * company_id : 14373
     * company_name : WFM India
     * status : 0
     * status_code : draft
     * status_color : #FF0000
     * project_name :
     * project_slug : test-project
     * views : 0
     */

    private int id;
    private int company_id;
    private String company_name;
    private int status;
    private String status_code;
    private String status_color;
    private String project_name;
    private String project_slug;
    private String project_logo;
    private String project_image;
    private String project_location;
    private double project_price;
    private int views;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus_color() {
        return status_color;
    }

    public void setStatus_color(String status_color) {
        this.status_color = status_color;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_slug() {
        return project_slug;
    }

    public void setProject_slug(String project_slug) {
        this.project_slug = project_slug;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getProject_logo() {
        return project_logo;
    }

    public void setProject_logo(String project_logo) {
        this.project_logo = project_logo;
    }

    public String getProject_location() {
        return project_location;
    }

    public void setProject_location(String project_location) {
        this.project_location = project_location;
    }

    public double getProject_price() {
        return project_price;
    }

    public void setProject_price(double project_price) {
        this.project_price = project_price;
    }

    public String getImage() {
        return project_image;
    }

    public void setImage(String image) {
        this.project_image = image;
    }
}
