package com.main.wfm.adapters;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.main.wfm.R;
import com.main.wfm.apiresponsemodel.SideMenuDataItemsResponse;
import com.main.wfm.utils.AppConstants;
import com.main.wfm.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class SideMenuAdapter extends BaseAdapter {
    private Context context;
    private List<SideMenuDataItemsResponse> item;
    private ArrayList<ClipData.Item> originalItem;

    public SideMenuAdapter() {
        super();
    }

    public SideMenuAdapter(Context context, List<SideMenuDataItemsResponse> item) {
        this.context = context;
        this.item = item;
        System.out.println("SIZE OF LIST: "+ item.size());
        //this.originalItem = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int position) {
        return item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (item.get(position).isSectionHeader()) {

            // if section header
            convertView = inflater.inflate(R.layout.lv_header, parent, false);
            TextView tvSectionTitle = (TextView) convertView.findViewById(R.id.tvParent);
            tvSectionTitle.setText(item.get(position).isName());
            if(item.get(position).isName().equalsIgnoreCase("Shop")) {
                tvSectionTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shop_leftbar,0,0,0);
            }else if (item.get(position).isName().equalsIgnoreCase("My Biz")) {
                tvSectionTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.lead_leftbar,0,0,0);
            }else{
                tvSectionTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.myaccount_leftbar,0,0,0);
            }
            tvSectionTitle.setTypeface(CommonUtils.setCustomFont(context, AppConstants.BOLD_FONT));
        } else {
            // if item
            convertView = inflater.inflate(R.layout.lv_child, parent, false);
            TextView tvItemTitle = (TextView) convertView.findViewById(R.id.tvChild);
            if(item.get(position).isName().equalsIgnoreCase("Change Password")){
                tvItemTitle.setText("My Wish List");
            }else{
                tvItemTitle.setText(item.get(position).isName());
            }

            tvItemTitle.setTypeface(CommonUtils.setCustomFont(context, AppConstants.REGULAR_FONT));
        }


        return convertView;
    }
}