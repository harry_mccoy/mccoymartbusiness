package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetContactInfoDataBean {
    @SerializedName("objCompanyData")
    private ContactInfoDataBean contactInfoDataBean;
    @SerializedName("objAddressType")
    private List<String> addressTypeList;
    private List<AddressListItembean> objAddressList;

    public ContactInfoDataBean getContactInfoDataBean() {
        return contactInfoDataBean;
    }

    public void setContactInfoDataBean(ContactInfoDataBean contactInfoDataBean) {
        this.contactInfoDataBean = contactInfoDataBean;
    }

    public List<String> getAddressTypeList() {
        return addressTypeList;
    }

    public void setAddressTypeList(List<String> addressTypeList) {
        this.addressTypeList = addressTypeList;
    }


    public List<AddressListItembean> getObjAddressList() {
        return objAddressList;
    }

    public void setObjAddressList(List<AddressListItembean> objAddressList) {
        this.objAddressList = objAddressList;
    }
}
