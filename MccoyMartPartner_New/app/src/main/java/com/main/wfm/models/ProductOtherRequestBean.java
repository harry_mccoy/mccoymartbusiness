package com.main.wfm.models;

public class ProductOtherRequestBean {

    /**
     * product_id : 10505
     * attribute_id : 2
     * type : text
     * text : Frame Color
     * value : 111
     * status : add
     */

    private String product_id;
    private String attribute_id;
    private String type;
    private String text;
    private String value;
    private String status;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAttribute_id() {
        return attribute_id;
    }

    public void setAttribute_id(String attribute_id) {
        this.attribute_id = attribute_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
