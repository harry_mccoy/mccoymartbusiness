package com.main.wfm.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.main.wfm.R;
import com.main.wfm.fragments.NotificationFragment;
import com.main.wfm.fragments.OffersAndDealsFragment;
import com.main.wfm.utils.AppSession;

public class NotificationAndOfferTabFragment extends Fragment {
    private ViewPager vpPager;
    private TabLayout tabLayout;
    private AppSession appSession;
    private Context mContext;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_notification, container, false);
        getIds(root);
        return root;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext=context;
    }
    private void getIds(View root) {
        appSession=new AppSession(mContext);
        tabLayout = (TabLayout) root.findViewById(R.id.tabLayout);
        vpPager = (ViewPager) root.findViewById(R.id.vpPager);
        createViewPager(vpPager);
        tabLayout.setupWithViewPager(vpPager);
        createTabIcons();
    }

    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabOne.setText("Notifications");
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Offers & Deals");
        tabLayout.getTabAt(1).setCustomView(tabTwo);

    }

    private void createViewPager(final ViewPager viewPager) {
        NotificationAndOfferTabFragment.ViewPagerAdapter adapter = new NotificationAndOfferTabFragment.ViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                //obLoadLeadsInterface.loadLeadData(arTypes[position],filter,search);

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 2;

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {

            //return LeadsNewFragment.newInstance(position);
            //return new LeadsNewFragment(position);
            switch (position) {
                case 0:
                    return NotificationFragment.newInstance(position);

                case 1:
                    return OffersAndDealsFragment.newInstance(position);

                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "pp";
        }


    }
}
