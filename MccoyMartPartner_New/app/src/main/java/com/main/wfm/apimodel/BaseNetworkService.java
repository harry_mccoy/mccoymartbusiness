package com.main.wfm.apimodel;


import android.content.Context;


import com.main.wfm.retrofitApi.RequestUrl;
import com.main.wfm.utils.AppSession;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ravi.shah
 */
public abstract class BaseNetworkService {
    private static RequestEndpoints requestEndpoints;
    private static String baseUrl;

    public static RequestEndpoints getRequestEndPoints(Context mContext) {
        if(requestEndpoints == null) {

            AppSession appSession = new AppSession(mContext);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

            baseUrl = RequestUrl.BASE_URL;
           /* if(appSession.getSelectedBaseUrl().equalsIgnoreCase("production")){
                baseUrl = RequestUrl.BASE_URL_PRODUCTION;
            }else if(appSession.getSelectedBaseUrl().equalsIgnoreCase("stage")){
                baseUrl = RequestUrl.BASE_URL_STAGE;
            }else {
                baseUrl = RequestUrl.BASE_URL_PRODUCTION;
            }*/

            Retrofit retrofit = new Retrofit.Builder().
                    baseUrl(baseUrl).
                    addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            requestEndpoints = retrofit.create(RequestEndpoints.class);
        }
        return requestEndpoints;
    }
}
