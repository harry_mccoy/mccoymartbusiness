package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;
import com.main.wfm.models.DashboardDetailsDataBean;

import java.util.List;

public class SideMenuDataResponse {
    @SerializedName("name")
    private String name;
    private boolean isSectionHeader;

    @SerializedName("data")
    private List<SideMenuDataItemsResponse> arlSideMenuData;

    public SideMenuDataResponse() {
    }


    public String isName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<SideMenuDataItemsResponse> getArlSideMenuData() {
        return arlSideMenuData;
    }

    public void setArlSideMenuData(List<SideMenuDataItemsResponse> arlSideMenuData) {
        this.arlSideMenuData = arlSideMenuData;
    }

    public boolean isSectionHeader() {
        return isSectionHeader;
    }

    public void setSectionHeader(boolean sectionHeader) {
        isSectionHeader = sectionHeader;
    }
}
