package com.main.wfm.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.ProductCommonGridAdapter;
import com.main.wfm.adapters.SortOrderAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.LeadsFilterDataResponse;
import com.main.wfm.apiresponsemodel.LeadsFilterResponse;
import com.main.wfm.apiresponsemodel.ProductCommonResponse;
import com.main.wfm.models.GetProductOfferRequestBean;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.models.ProductInfoResponseBean;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class ProductsCommonActivity extends AppCompatActivity implements View.OnClickListener {


    private List<ProductDataDashboardShopBean>arlProductInfoResponseBeans;
    private ImageView ivShort,ivFilter;
    private TextView tvSort,tvFilter;
    private GridView gvProducts;
    private String moduleType="";
    private AppSession appSession;
    private ProductCommonGridAdapter adapter;
    private SortOrderAdapter sortOrderAdapter;
    private TextView tvProductCount;
    private BottomSheetDialog sortDialog;
    private ListView lvFilter;
    private int totalCount=0;
    private List<LeadsFilterDataResponse> sortDataResponse;
    private String category_id,category_name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_grid);
        appSession = new AppSession(ProductsCommonActivity.this);
        getIds();



        if(moduleType.equalsIgnoreCase("b2btodaydeal")){
            getProductsAPI(moduleType);
            ((TextView)findViewById(R.id.tvTitle)).setText("Today's Deal");
        }else if(moduleType.equalsIgnoreCase("latestproduct")){
            getLatestProductsAPI();
            ((TextView)findViewById(R.id.tvTitle)).setText("New Launches");
        }else if(moduleType.equalsIgnoreCase("topBrands")){
            getTopBrandsProductsAPI();
            ((TextView)findViewById(R.id.tvTitle)).setText("Top Brands");
        }else{
            getProductsByCategoryAPI(category_id,"","",totalCount,"0");
            ((TextView)findViewById(R.id.tvTitle)).setText(category_name);
            tvProductCount.setVisibility(View.GONE);
        }


    }

    private void getIds() {
        ivFilter=findViewById(R.id.ivFilter);
        ivShort=findViewById(R.id.ivShort);
        tvSort =findViewById(R.id.tvSort);
        tvFilter=findViewById(R.id.tvFilter);

        gvProducts=findViewById(R.id.grid_view);
        tvProductCount=findViewById(R.id.tvProductCount);
        arlProductInfoResponseBeans=new ArrayList<ProductDataDashboardShopBean>();
        /*adapter=new ProductCommonGridAdapter(ProductsCommonActivity.this,arlProductInfoResponseBeans,moduleType);
        gvProducts.setAdapter(adapter);*/

        ivShort.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        tvFilter.setOnClickListener(this);
        tvSort.setOnClickListener(this);
        if(getIntent().getStringExtra("category_id")!=null){
            category_id=getIntent().getStringExtra("category_id");
        }else {
            category_id="";
        }
        if(getIntent().getStringExtra("category_name")!=null){
            category_name=getIntent().getStringExtra("category_name");
        }else {
            category_name="";
        }
        if (getIntent().getStringExtra("module_type")!=null){
            moduleType=getIntent().getStringExtra("module_type");
        }else {
            moduleType="";
        }

        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }



    public void getProductsAPI(final String moduleType) {
        final ViewDialog viewDialog = new ViewDialog(ProductsCommonActivity.this);
        viewDialog.showDialog();
        GetProductOfferRequestBean requestt =new GetProductOfferRequestBean();
        requestt.setSource("2");
        requestt.setModule_type(moduleType);
        NetworkAPI.getTodaysProducts(ProductsCommonActivity.this, "Bearer " + appSession.getAuthToken(),requestt,
                new Dispatch<ProductCommonResponse>() {

                    @Override
                    public void apiSuccess(ProductCommonResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));


                        if (body != null) {
                            if (body.getProductDataCommonResponse().getProductDataDashboardShopBeans() != null && body.getProductDataCommonResponse().getProductDataDashboardShopBeans().size() > 0) {
                            viewDialog.hideDialog();
                            tvProductCount.setText(body.getProductDataCommonResponse().getTotal_count()+
                                    " Listing in Today's Deal");
                            arlProductInfoResponseBeans=body.getProductDataCommonResponse().getProductDataDashboardShopBeans();
                                if(!(arlProductInfoResponseBeans.size()%2==0)){
                                    ProductDataDashboardShopBean obProductDataDashboardShopBean=new ProductDataDashboardShopBean();
                                    arlProductInfoResponseBeans.add(obProductDataDashboardShopBean);
                                }
                                adapter=new ProductCommonGridAdapter(ProductsCommonActivity.this,arlProductInfoResponseBeans,moduleType);
                                gvProducts.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ProductsCommonActivity.this, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();

                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }


    public void getLatestProductsAPI() {
        final ViewDialog viewDialog = new ViewDialog(ProductsCommonActivity.this);
        viewDialog.showDialog();
        GetProductOfferRequestBean requestt =new GetProductOfferRequestBean();
        requestt.setSource("2");

        NetworkAPI.getLatestProducts(ProductsCommonActivity.this, "Bearer " + appSession.getAuthToken(),requestt,
                new Dispatch<ProductCommonResponse>() {

                    @Override
                    public void apiSuccess(ProductCommonResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));


                        if (body != null) {
                            if (body.getProductDataCommonResponse().getProductDataDashboardShopBeans() != null && body.getProductDataCommonResponse().getProductDataDashboardShopBeans().size() > 0) {
                                viewDialog.hideDialog();
                                tvProductCount.setText(body.getProductDataCommonResponse().getTotal_count()+
                                        " Listing in New Launches");
                                arlProductInfoResponseBeans=body.getProductDataCommonResponse().getProductDataDashboardShopBeans();
                                if(!(arlProductInfoResponseBeans.size()%2==0)){
                                    ProductDataDashboardShopBean obProductDataDashboardShopBean=new ProductDataDashboardShopBean();
                                    arlProductInfoResponseBeans.add(obProductDataDashboardShopBean);
                                }
                                adapter=new ProductCommonGridAdapter(ProductsCommonActivity.this,arlProductInfoResponseBeans,moduleType);
                                gvProducts.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ProductsCommonActivity.this, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();

                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    public void getTopBrandsProductsAPI() {
        final ViewDialog viewDialog = new ViewDialog(ProductsCommonActivity.this);
        viewDialog.showDialog();
        GetProductOfferRequestBean requestt =new GetProductOfferRequestBean();
        requestt.setSource("2");

        NetworkAPI.getTopBrandsProducts(ProductsCommonActivity.this, "Bearer " + appSession.getAuthToken(),requestt,
                new Dispatch<ProductInfoResponseBean>() {

                    @Override
                    public void apiSuccess(ProductInfoResponseBean body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));


                        if (body != null) {
                            if (body.getProductData() != null && body.getProductData().size() > 0) {
                                viewDialog.hideDialog();
                                tvProductCount.setVisibility(View.GONE);
                                ((LinearLayout)findViewById(R.id.llSortFilterHeader)).setVisibility(View.GONE);
                                arlProductInfoResponseBeans=body.getProductData();
                                if(!(arlProductInfoResponseBeans.size()%2==0)){
                                    ProductDataDashboardShopBean obProductDataDashboardShopBean=new ProductDataDashboardShopBean();
                                    arlProductInfoResponseBeans.add(obProductDataDashboardShopBean);
                                }
                                adapter=new ProductCommonGridAdapter(ProductsCommonActivity.this,arlProductInfoResponseBeans,moduleType);
                                gvProducts.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ProductsCommonActivity.this, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();

                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    public void getProductsByCategoryAPI(String category_id, String sort, String order, int required_total_count, String required_sort_order_data) {
        final ViewDialog viewDialog = new ViewDialog(ProductsCommonActivity.this);
        viewDialog.showDialog();
        GetProductOfferRequestBean requestt =new GetProductOfferRequestBean();
        requestt.setSource("2");
        requestt.setCategory_id(category_id);
        requestt.setSort(sort);
        requestt.setOrder(order);
        requestt.setRequired_total_count(required_total_count);
        requestt.setRequired_sort_order_data(required_sort_order_data);
        NetworkAPI.getProductByCategory(ProductsCommonActivity.this, "Bearer " + appSession.getAuthToken(),requestt,
                new Dispatch<ProductCommonResponse>() {

                    @Override
                    public void apiSuccess(ProductCommonResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));


                        if (body != null) {
                            if (body.getProductDataCommonResponse().getProductDataDashboardShopBeans() != null && body.getProductDataCommonResponse().getProductDataDashboardShopBeans().size() > 0) {
                                viewDialog.hideDialog();
                                //tvProductCount.setText(body.getProductDataCommonResponse().getTotal_count()+" Listing in Today's Deal");

                                arlProductInfoResponseBeans=body.getProductDataCommonResponse().getProductDataDashboardShopBeans();
                                if(!(arlProductInfoResponseBeans.size()%2==0)){
                                    ProductDataDashboardShopBean obProductDataDashboardShopBean=new ProductDataDashboardShopBean();
                                    arlProductInfoResponseBeans.add(obProductDataDashboardShopBean);
                                }
                                adapter=new ProductCommonGridAdapter(ProductsCommonActivity.this,arlProductInfoResponseBeans,moduleType);
                                gvProducts.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                                tvProductCount.setVisibility(View.VISIBLE);
                                tvProductCount.setText(arlProductInfoResponseBeans.size()+" Listing in "+category_name);
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ProductsCommonActivity.this, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();

                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ivShort:
            case R.id.tvSort:
                openSortDialog();
                break;

            case R.id.ivFilter:
            case R.id.tvFilter:
                break;

        }
    }

    private void openSortDialog() {
        sortDialog = new BottomSheetDialog(ProductsCommonActivity.this);
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_sort_leads, null);
        //sortDialog.setContentView(R.layout.dialog_sort_leads);
        sortDialog.setContentView(sheetView);
        // set the custom dialog components - text, image and button
        sortDialog.setCancelable(true);

        lvFilter = sortDialog.findViewById(R.id.lvLeadsFilter);

        sortDataResponse = new ArrayList<LeadsFilterDataResponse>();
        sortOrderAdapter = new SortOrderAdapter(ProductsCommonActivity.this,
                sortDataResponse,category_id,totalCount,moduleType);
        lvFilter.setAdapter(sortOrderAdapter);


        getSortData();
        sortDialog.show();

    }
    private void getSortData() {
        NetworkAPI.getSortOrder(ProductsCommonActivity.this, "Bearer " + appSession.getAuthToken(),
                new Dispatch<LeadsFilterResponse>() {

                    @Override
                    public void apiSuccess(LeadsFilterResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));


                        if (body != null) {
                            if (body.getLeadsData() != null && body.getLeadsData().size() > 0) {

                                sortDataResponse = body.getLeadsData();
                                sortOrderAdapter = new SortOrderAdapter(ProductsCommonActivity.this,
                                        sortDataResponse,category_id,totalCount,moduleType);
                                lvFilter.setAdapter(sortOrderAdapter);
                            } else {

                            }
                        } else {
                            CommonUtils.showToast(ProductsCommonActivity.this, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {


                    }

                    @Override
                    public void error(String error) {

                        System.out.println("API Data Error : " + error);
                    }
                });


    }

    public void disableFilterDialog() {
        if (sortDialog != null) {
            sortDialog.dismiss();
        }
    }
}
