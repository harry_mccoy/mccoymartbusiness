package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadsInfoDataBean {
    @SerializedName("objNew")
    private LeadsTypeInfoDataBean newLeads;

    @SerializedName("objActive")
    private LeadsTypeInfoDataBean activeLeads;

    @SerializedName("objClosed")
    private LeadsTypeInfoDataBean closedLeads;

    @SerializedName("ObjData")
    private List<LeadsTypeItemDataBean> leadsTypeItemDataBeanList;

    public LeadsTypeInfoDataBean getNewLeads() {
        return newLeads;
    }

    public void setNewLeads(LeadsTypeInfoDataBean newLeads) {
        this.newLeads = newLeads;
    }

    public LeadsTypeInfoDataBean getActiveLeads() {
        return activeLeads;
    }

    public void setActiveLeads(LeadsTypeInfoDataBean activeLeads) {
        this.activeLeads = activeLeads;
    }

    public LeadsTypeInfoDataBean getClosedLeads() {
        return closedLeads;
    }

    public void setClosedLeads(LeadsTypeInfoDataBean closedLeads) {
        this.closedLeads = closedLeads;
    }

    public List<LeadsTypeItemDataBean> getLeadsTypeItemDataBeanList() {
        return leadsTypeItemDataBeanList;
    }

    public void setLeadsTypeItemDataBeanList(List<LeadsTypeItemDataBean> leadsTypeItemDataBeanList) {
        this.leadsTypeItemDataBeanList = leadsTypeItemDataBeanList;
    }
}
