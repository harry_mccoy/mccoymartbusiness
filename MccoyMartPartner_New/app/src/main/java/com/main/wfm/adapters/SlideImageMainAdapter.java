package com.main.wfm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.main.wfm.R;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.apiresponsemodel.ProductImagesResponse;

import java.util.List;

public class SlideImageMainAdapter extends PagerAdapter {
    Context context;
    private com.main.wfm.utils.TouchImageView imageView;
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
    LayoutInflater layoutInflater;
    private List<ProductImagesResponse> productImages;

    public SlideImageMainAdapter(Context context, List<ProductImagesResponse> productImages) {
        this.context = context;
        this.productImages = productImages;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.slider_image_item_main, container, false);

         imageView = (com.main.wfm.utils.TouchImageView) itemView.findViewById(R.id.ivSlidMain);
       // imageView.setImageResource(images[position]);
        Glide.with(context)
                .load(productImages.get(position).getImage())
                .into(imageView);

        container.addView(itemView);

        //listening to image click
        /*imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });
*/
     /*   imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mScaleGestureDetector.onTouchEvent(event);

                return true;
            }
        });
*/
        //mScaleGestureDetector = new ScaleGestureDetector(context, new ScaleListener());
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(0.1f,
                    Math.min(mScaleFactor, 10.0f));
            imageView.setScaleX(mScaleFactor);
            imageView.setScaleY(mScaleFactor);
            return true;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            mScaleGestureDetector.onTouchEvent(motionEvent);
            return true;
        }
    }
}
