package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class LeadsDetailsResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private String statusCode;

    @SerializedName("data")
    private LeadsDetailsDataResponse leadsDetailsData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public LeadsDetailsDataResponse getLeadsDetailsData() {
        return leadsDetailsData;
    }

    public void setLeadsDetailsData(LeadsDetailsDataResponse leadsDetailsData) {
        this.leadsDetailsData = leadsDetailsData;
    }
}
