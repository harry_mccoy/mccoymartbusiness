package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashboardDataBean {

    @SerializedName("objNotifiCount")
    private int notificationCount;

    @SerializedName("objUSerInfo")
    private DashboardUserInfoBean dashboardUserInfoBean;
    @SerializedName("objSideBar")
    private List<NavigationItemInfoBean> navigationItemInfoBeanList;

    @SerializedName("objCompany")
    private List<CompanyListInfoBean> companyListInfoBeanList;

    @SerializedName("objFloating")
    private List<FloatingListInfoBean> floatingListInfoBeanList;

    @SerializedName("objBlankData")
    private DashboardBlanckDataBean dashboardBlanckDataBean;

    @SerializedName("objSupport")
    private DashboardSupportDataBean objSupport;

    public DashboardUserInfoBean getDashboardUserInfoBean() {
        return dashboardUserInfoBean;
    }

    public void setDashboardUserInfoBean(DashboardUserInfoBean dashboardUserInfoBean) {
        this.dashboardUserInfoBean = dashboardUserInfoBean;
    }

    public List<NavigationItemInfoBean> getNavigationItemInfoBeanList() {
        return navigationItemInfoBeanList;
    }

    public void setNavigationItemInfoBeanList(List<NavigationItemInfoBean> navigationItemInfoBeanList) {
        this.navigationItemInfoBeanList = navigationItemInfoBeanList;
    }

    public List<CompanyListInfoBean> getCompanyListInfoBeanList() {
        return companyListInfoBeanList;
    }

    public void setCompanyListInfoBeanList(List<CompanyListInfoBean> companyListInfoBeanList) {
        this.companyListInfoBeanList = companyListInfoBeanList;
    }

    public List<FloatingListInfoBean> getFloatingListInfoBeanList() {
        return floatingListInfoBeanList;
    }

    public void setFloatingListInfoBeanList(List<FloatingListInfoBean> floatingListInfoBeanList) {
        this.floatingListInfoBeanList = floatingListInfoBeanList;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public DashboardBlanckDataBean getDashboardBlanckDataBean() {
        return dashboardBlanckDataBean;
    }

    public void setDashboardBlanckDataBean(DashboardBlanckDataBean dashboardBlanckDataBean) {
        this.dashboardBlanckDataBean = dashboardBlanckDataBean;
    }

    public DashboardSupportDataBean getObjSupport() {
        return objSupport;
    }

    public void setObjSupport(DashboardSupportDataBean objSupport) {
        this.objSupport = objSupport;
    }
}
