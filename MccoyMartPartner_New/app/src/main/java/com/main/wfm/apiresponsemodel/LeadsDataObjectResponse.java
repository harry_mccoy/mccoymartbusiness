package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class LeadsDataObjectResponse {
    @SerializedName("title")
    private String title;

    @SerializedName("action")
    private String action;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @SerializedName("count")
    private String count;

    
}
