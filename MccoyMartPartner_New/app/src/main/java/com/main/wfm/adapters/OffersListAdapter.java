package com.main.wfm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.main.wfm.R;
import com.main.wfm.apiresponsemodel.NotificationListDataResponse;

import java.util.List;

public class OffersListAdapter extends RecyclerView.Adapter<OffersListAdapter.MyViewHolder> {
    private List<NotificationListDataResponse> notificationList;
    private Context mContext;

    public OffersListAdapter(Context mContext, List<NotificationListDataResponse> notificationList) {
        this.notificationList = notificationList;
        this.mContext = mContext;
    }

    @Override
    public OffersListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_offer, parent, false);
        OffersListAdapter.MyViewHolder holder = new OffersListAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final OffersListAdapter.MyViewHolder holder, final int position) {
        holder.tvNotificationTitle.setText(notificationList.get(position).getTitle());
        holder.tvNameAddress.setText(notificationList.get(position).getMessage());
        holder.tvDateTime.setText(notificationList.get(position).getDate());



       /* holder.llNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, LeadsDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("notification_id", notificationList.get(position).getNotification_id());
                intent.putExtra("company_id", notificationList.get(position).getCompany_id());
                mContext.startActivity(intent);
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
        //return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNotificationTitle, tvNameAddress, tvDateTime, tvReadStatus;
        public ImageView ivNotificationImage;
        public LinearLayout llNotification;

        public MyViewHolder(View v) {
            super(v);
            tvNotificationTitle = (TextView) v.findViewById(R.id.tvOfferTitle);
            tvDateTime = (TextView) v.findViewById(R.id.tvDateTime );
            tvNameAddress  = (TextView) v.findViewById(R.id.tvOfferDays);


            llNotification = v.findViewById(R.id.llNotification);

        }
    }
}