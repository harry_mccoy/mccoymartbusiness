package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BusinessDetailsDataBean {
    @SerializedName("BusinessType")
    private List<BusinessRoleDataBean> businessRoleDataBeanList;

    @SerializedName("LegalStatus")
    private List<BusinessLegalDataBean> businessLegalDataBeanList;

    public List<BusinessRoleDataBean> getBusinessRoleDataBeanList() {
        return businessRoleDataBeanList;
    }

    public void setBusinessRoleDataBeanList(List<BusinessRoleDataBean> businessRoleDataBeanList) {
        this.businessRoleDataBeanList = businessRoleDataBeanList;
    }

    public List<BusinessLegalDataBean> getBusinessLegalDataBeanList() {
        return businessLegalDataBeanList;
    }

    public void setBusinessLegalDataBeanList(List<BusinessLegalDataBean> businessLegalDataBeanList) {
        this.businessLegalDataBeanList = businessLegalDataBeanList;
    }
}
