package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class NotificationHubDataBean {
    @SerializedName("user_id")
    private int userId;
    @SerializedName("company_id")
    private int companyId;
    @SerializedName("lead_id")
    private int leadId;
    @SerializedName("notification_type")
    private int notificationType;
    @SerializedName("notification_id")
    private int notificationId;
    @SerializedName("title")
    private String title;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private int readStatus;
    @SerializedName("date")
    private String time;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public int getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(int notificationType) {
        this.notificationType = notificationType;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
