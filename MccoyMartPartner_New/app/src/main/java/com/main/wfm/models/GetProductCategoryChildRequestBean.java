package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class GetProductCategoryChildRequestBean {
    @SerializedName("business_type")
    private String businessType;
    @SerializedName("parent_id")
    private String parentId;
    @SerializedName("product_id")
    private String product_id;
    private String device_name;

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }



    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
