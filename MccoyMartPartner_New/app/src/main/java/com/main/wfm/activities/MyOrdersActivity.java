package com.main.wfm.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.MyOrderAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.CategoryProductResponse;
import com.main.wfm.apiresponsemodel.MyOrderDataItemResponse;
import com.main.wfm.apiresponsemodel.MyOrderDataResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.VerticalLineDecorator;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class MyOrdersActivity extends AppCompatActivity {

    private List<MyOrderDataItemResponse> myOrderDataList;
    private AppSession appSession;
    private RecyclerView rvMyOrderList;
    private MyOrderAdapter adapter;
    private LinearLayout llEmptyMyOrderFrame;
    private int page=1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorders);
        appSession=new AppSession(MyOrdersActivity.this);
        getIds();
        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((TextView) findViewById(R.id.tvContinueShopping)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        myOrderDataList=new ArrayList<MyOrderDataItemResponse>();
        rvMyOrderList.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(MyOrdersActivity.this);
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter=new MyOrderAdapter(MyOrdersActivity.this,myOrderDataList);
        rvMyOrderList.setAdapter(adapter);
        rvMyOrderList.addItemDecoration(new VerticalLineDecorator(2));
        rvMyOrderList.setLayoutManager(MyLayoutManager);

        getMyOrdersAPI(page);
    }

    private void getIds() {
        rvMyOrderList=findViewById(R.id.rvMyOrderList);
        llEmptyMyOrderFrame=findViewById(R.id.llEmptyMyOrderFrame);
    }

    private void getMyOrdersAPI(final int page) {
        final ViewDialog viewDialog = new ViewDialog(MyOrdersActivity.this);
        viewDialog.showDialog();

        NetworkAPI.getMyOrderApi(MyOrdersActivity.this, "Bearer " + appSession.getAuthToken(), page,
                new Dispatch<MyOrderDataResponse>() {

                    @Override
                    public void apiSuccess(MyOrderDataResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));
                        viewDialog.hideDialog();
                        if (body != null) {
                            if (body.getMyOrderDataList() != null && body.getMyOrderDataList().size() > 0) {
                                viewDialog.hideDialog();

                                myOrderDataList=body.getMyOrderDataList();

                                if(myOrderDataList!=null&&myOrderDataList.size()>0){
                                    adapter=new MyOrderAdapter(MyOrdersActivity.this,myOrderDataList);
                                    rvMyOrderList.setAdapter(adapter);
                                    llEmptyMyOrderFrame.setVisibility(View.GONE);
                                    rvMyOrderList.setVisibility(View.VISIBLE);
                                }else{
                                    llEmptyMyOrderFrame.setVisibility(View.VISIBLE);
                                    rvMyOrderList.setVisibility(View.GONE);
                                }


                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(MyOrdersActivity.this, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();

                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}
