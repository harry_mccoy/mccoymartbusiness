package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class ProductDetailRelatedProductResponse {

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("name")
    private String name;


    @SerializedName("discount")
    private String discount;

    @SerializedName("discount_off")
    private String discount_off;

    @SerializedName("image")
    private String image;

    @SerializedName("manufacturer")
    private String manufacturer;

    @SerializedName("price")
    private String price;

    @SerializedName("special")
    private String special;

    @SerializedName("stock_status")
    private String stock_status;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_off() {
        return discount_off;
    }

    public void setDiscount_off(String discount_off) {
        this.discount_off = discount_off;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getStock_status() {
        return stock_status;
    }

    public void setStock_status(String stock_status) {
        this.stock_status = stock_status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
