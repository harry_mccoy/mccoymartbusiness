package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProductOfferRequestBean {
    @SerializedName("source")
    private String source;


    @SerializedName("module_type")
    private String module_type;

    @SerializedName("category_id")
    private String category_id;

    @SerializedName("sort")
    private String sort;

    @SerializedName("order")
    private String order;

    @SerializedName("required_total_count")
    private int required_total_count;

    @SerializedName("required_sort_order_data")
    private String required_sort_order_data;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getRequired_total_count() {
        return required_total_count;
    }

    public void setRequired_total_count(int required_total_count) {
        this.required_total_count = required_total_count;
    }

    public String getRequired_sort_order_data() {
        return required_sort_order_data;
    }

    public void setRequired_sort_order_data(String required_sort_order_data) {
        this.required_sort_order_data = required_sort_order_data;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }






    public String getModule_type() {
        return module_type;
    }

    public void setModule_type(String module_type) {
        this.module_type = module_type;
    }
}
