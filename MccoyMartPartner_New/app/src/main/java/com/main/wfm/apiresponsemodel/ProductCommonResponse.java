package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class ProductCommonResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private String statusCode;

    @SerializedName("data")
    private ProductDataCommonResponse productDataCommonResponse;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }


    public ProductDataCommonResponse getProductDataCommonResponse() {
        return productDataCommonResponse;
    }

    public void setProductDataCommonResponse(ProductDataCommonResponse productDataCommonResponse) {
        this.productDataCommonResponse = productDataCommonResponse;
    }
}
