package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class ObjAreaDataBean {
    @SerializedName("id")
    private int objAreaId;
    @SerializedName("name")
    private String objAreaName;
    private boolean isChecked;


    public int getObjAreaId() {
        return objAreaId;
    }

    public void setObjAreaId(int objAreaId) {
        this.objAreaId = objAreaId;
    }

    public String getObjAreaName() {
        return objAreaName;
    }

    public void setObjAreaName(String objAreaName) {
        this.objAreaName = objAreaName;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
