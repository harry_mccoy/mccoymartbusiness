package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class OtpVerificationDataBean {
    @SerializedName("user_id")
    private String userId;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private String phone;
    @SerializedName("access_token")
    private String token;
    @SerializedName("profile_picture")
    private String profilePicture;
   /* @SerializedName("is_phone_verified")
    private boolean isPhoneVerified;
    @SerializedName("is_email_verified")
    private boolean isEmailVerified;*/
    /*@SerializedName("company")
    private List<CompanyDataBean> companyDataBeanList;
*/
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

   /* public boolean isPhoneVerified() {
        return isPhoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        isPhoneVerified = phoneVerified;
    }

    public boolean isEmailVerified() {
        return isEmailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        isEmailVerified = emailVerified;
    }
*/
/*    public List<CompanyDataBean> getCompanyDataBeanList() {
        return companyDataBeanList;
    }

    public void setCompanyDataBeanList(List<CompanyDataBean> companyDataBeanList) {
        this.companyDataBeanList = companyDataBeanList;
    }*/
}
