package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class StarRatingItemResponse {



    @SerializedName("count")
    private String count;

    @SerializedName("color")
    private String color;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
