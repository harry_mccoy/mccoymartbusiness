package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateProductCategoryRequestBean {
    @SerializedName("company_id")
    private String companyId;
    private String device_name;
    @SerializedName("category")
    private List<Integer> categoriesIdList;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public List<Integer> getCategoriesIdList() {
        return categoriesIdList;
    }

    public void setCategoriesIdList(List<Integer> categoriesIdList) {
        this.categoriesIdList = categoriesIdList;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }
}
