package com.main.wfm.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.NotificationListAdapter;
import com.main.wfm.adapters.ReviewListAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.NotificationListDataResponse;
import com.main.wfm.apiresponsemodel.RatingReviewResponse;
import com.main.wfm.apiresponsemodel.ReviewItemResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.VerticalLineDecorator;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class ReviewListActivity extends AppCompatActivity {

    private ReviewListAdapter adapter;
    private List<ReviewItemResponse> reviewList;
    private RecyclerView rvReviewList;
    private AppSession appSession;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSession=new AppSession(ReviewListActivity.this);
        setContentView(R.layout.activity_review);

        ((ImageView) findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rvReviewList=findViewById(R.id.rvReviewList);
        rvReviewList.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(ReviewListActivity.this);
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reviewList=new ArrayList<ReviewItemResponse>();
        adapter=new ReviewListAdapter(ReviewListActivity.this,reviewList);
        rvReviewList.setAdapter(adapter);
        rvReviewList.addItemDecoration(new VerticalLineDecorator(2));
        rvReviewList.setLayoutManager(MyLayoutManager);

        //getWishListApi(getIntent().getStringExtra("productId"));
        getReviewRatingAPI("694");
    }

    public void getReviewRatingAPI(final String productId) {
        final ViewDialog viewDialog = new ViewDialog(ReviewListActivity.this);
        viewDialog.showDialog();

        NetworkAPI.getProductReviewAndRatingApi(ReviewListActivity.this, "Bearer " + appSession.getAuthToken(), productId,
                new Dispatch<RatingReviewResponse>() {
                    @Override
                    public void apiSuccess(RatingReviewResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getReviewRatingData() != null) {

                                reviewList = body.getReviewRatingData().getReview_list();
                                adapter=new ReviewListAdapter(ReviewListActivity.this,reviewList);
                                rvReviewList.setAdapter(adapter);
                                ((TextView) findViewById(R.id.tvTitle)).setText("("+reviewList.size()+") Reviews");
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ReviewListActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}
