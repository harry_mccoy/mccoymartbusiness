package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class LeadDetailsResponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String statusMessage;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("data")
    private LeadDetailsDataBean leadDetailsDataBean;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public LeadDetailsDataBean getLeadDetailsDataBean() {
        return leadDetailsDataBean;
    }

    public void setLeadDetailsDataBean(LeadDetailsDataBean leadDetailsDataBean) {
        this.leadDetailsDataBean = leadDetailsDataBean;
    }
}
