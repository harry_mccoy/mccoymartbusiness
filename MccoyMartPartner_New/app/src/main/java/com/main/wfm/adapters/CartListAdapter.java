package com.main.wfm.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.CartActivity;
import com.main.wfm.activities.MyWishListActivity;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apirequestmodel.AddToCartRequest;
import com.main.wfm.apiresponsemodel.CartListDataResponse;
import com.main.wfm.apiresponsemodel.ReviewItemResponse;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.List;

public class CartListAdapter extends BaseAdapter {
    private List<CartListDataResponse> cartListData;
    private Context mContext;
    private LayoutInflater infalter;
    private AppSession appSession;
    //private int producQquantity = 1;
    //private int selectedPosition;
    private ViewHolder holder;
    private int[] arTempQty;

    public CartListAdapter(Context mContext, List<CartListDataResponse> cartListData) {
        this.mContext = mContext;
        this.cartListData = cartListData;
        appSession=new AppSession(this.mContext);
        arTempQty=new int[this.cartListData.size()];
        for (int i=0;i<this.cartListData.size();i++){
            arTempQty[i]=Integer.parseInt(this.cartListData.get(i).getCart_quantity());
        }
        infalter = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {

        return cartListData.size();
    }

    @Override
    public Object getItem(int position) {

        return cartListData.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = infalter.inflate(R.layout.row_cart, null);
            holder = new ViewHolder();
            holder.tvProductName=convertView.findViewById(R.id.tvProductName);
            holder.tvRatingQty=convertView.findViewById(R.id.tvRatingQty);
            holder.tvRatingAndReview=convertView.findViewById(R.id.tvRatingAndReview);
            holder.tvOldPrice=convertView.findViewById(R.id.tvOldPrice);
            holder.tvNewPrice=convertView.findViewById(R.id.tvNewPrice);
            holder.tvOff=convertView.findViewById(R.id.tvOff);
            holder.tvDecrease=convertView.findViewById(R.id.tvDecrease);
            holder.tvQty=convertView.findViewById(R.id.tvQty);
            holder.tvIncrease=convertView.findViewById(R.id.tvIncrease);
            holder.tvRemove=convertView.findViewById(R.id.tvRemove);
            holder.tvAddToWishList=convertView.findViewById(R.id.tvAddToWishList);
            holder.ivItemImage=convertView.findViewById(R.id.ivItemImage);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvProductName.setText(cartListData.get(position).getName());
        holder.tvQty.setText(cartListData.get(position).getCart_quantity());
        //holder.tvRatingAndReview.setText(cartListData.get(position).getReview()+" Ratings and Reviews");

        if (cartListData.get(position).getSpecial() != null) {
            holder.tvNewPrice.setVisibility(View.VISIBLE);
            holder.tvNewPrice.setText("Rs. "+cartListData.get(position).getSpecial());
        } else {
            holder.tvNewPrice.setVisibility(View.GONE);
        }
        if (cartListData.get(position).getPrice() != null) {
            holder.tvOldPrice.setText("Rs. " + cartListData.get(position).getPrice());
            if (cartListData.get(position).getSpecial() != null &&
                    cartListData.get(position).getSpecial().length() > 0) {
                holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }
        //holder.tvOff.setText(cartListData.get(position).getDiscount()+"% off");
        //holder.tvRatingQty.setText(cartListData.get(position).getRating());


        //selectedPosition=position;

        holder.tvIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int qty=Integer.parseInt(cartListData.get(position).getCart_quantity());
                if (qty<arTempQty[position]) {
                     cartListData.get(position).setCart_quantity(""+(qty+1));
                    //producQquantity += 1;
                    //holder.tvQty.setText("" + producQquantity);
                    addToCartApi(cartListData.get(position).getProduct_id(),cartListData.get(position).getCart_quantity());
                    ((CartActivity)mContext).getCartListApi();
                    notifyDataSetChanged();
                }
            }
        });
        holder.tvDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty=Integer.parseInt(cartListData.get(position).getCart_quantity());

                if ( qty>1) {
                    cartListData.get(position).setCart_quantity(""+(qty-1));
                    //holder.tvQty.setText("" + producQquantity);
                    addToCartApi(cartListData.get(position).getProduct_id(),cartListData.get(position).getCart_quantity());
                    ((CartActivity)mContext).getCartListApi();
                    notifyDataSetChanged();
                }
            }
        });

        holder.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFromCartApi(cartListData.get(position).getProduct_id(),cartListData.get(position).getCart_quantity());
                ((CartActivity)mContext).getCartListApi();
            }
        });

        holder.tvAddToWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemToWishListApi(cartListData.get(position).getProduct_id());
                ((CartActivity)mContext).getCartListApi();
            }
        });

        return convertView;
    }

    public class ViewHolder {
        public TextView tvProductName, tvRatingQty, tvRatingAndReview, tvOldPrice,tvNewPrice,tvOff,
                tvDecrease,tvQty,tvIncrease,tvRemove,tvAddToWishList;
        public ImageView ivItemImage;
    }

    public void addToCartApi(final String productId,String producQquantity) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        AddToCartRequest request = new AddToCartRequest();
        request.setSource("2");
        request.setProduct_id(productId);
        request.setQty(producQquantity);
        request.setDevice_id(appSession.getDeviceId());

        NetworkAPI.addToCartApi(mContext, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.showToast(mContext,body.message);
                                notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, mContext.getResources().getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    public void removeFromCartApi(final String productId,String producQquantity) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        AddToCartRequest request = new AddToCartRequest();
        request.setSource("2");
        request.setProduct_id(productId);
        request.setQty(producQquantity);
        request.setDevice_id(appSession.getDeviceId());

        NetworkAPI.removeFromCartApi(mContext, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.showToast(mContext,body.message);
                                notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, mContext.getResources().getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    public void addItemToWishListApi(String productId) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setAction("add");
        request.setProduct_id(productId);
        NetworkAPI.saveForLaterApi(mContext, "Bearer " + appSession.getAuthToken(),request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.showToast(mContext,body.message);
                                notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, mContext.getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}