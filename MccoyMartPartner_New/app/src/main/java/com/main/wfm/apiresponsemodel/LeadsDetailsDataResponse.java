package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadsDetailsDataResponse {
    @SerializedName("objStatus")
    private List<LeadsDetailsDataObjectResponse> objStatus;

    @SerializedName("objEnquiry")
    private List<LeadsDetailsDataObjectResponse> objEnquiry;


    @SerializedName("objBuyer")
    private LeadsDataListResponse objBuyer;

    @SerializedName("objClosed")
    private List<LeadsDetailsDataObjectResponse> objClosed;

    public List<LeadsDetailsDataObjectResponse> getObjStatus() {
        return objStatus;
    }

    public void setObjStatus(List<LeadsDetailsDataObjectResponse> objStatus) {
        this.objStatus = objStatus;
    }

    public List<LeadsDetailsDataObjectResponse> getObjEnquiry() {
        return objEnquiry;
    }

    public void setObjEnquiry(List<LeadsDetailsDataObjectResponse> objEnquiry) {
        this.objEnquiry = objEnquiry;
    }

    public LeadsDataListResponse getObjBuyer() {
        return objBuyer;
    }

    public void setObjBuyer(LeadsDataListResponse objBuyer) {
        this.objBuyer = objBuyer;
    }

    public List<LeadsDetailsDataObjectResponse> getObjClosed() {
        return objClosed;
    }

    public void setObjClosed(List<LeadsDetailsDataObjectResponse> objClosed) {
        this.objClosed = objClosed;
    }
}
