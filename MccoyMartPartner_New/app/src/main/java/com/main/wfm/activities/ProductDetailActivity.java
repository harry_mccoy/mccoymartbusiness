package com.main.wfm.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.RelatedProductAdapter;
import com.main.wfm.adapters.SizeAdapter;
import com.main.wfm.adapters.ThumbImageAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddToCartRequest;
import com.main.wfm.apirequestmodel.ProductDetailRequest;
import com.main.wfm.apiresponsemodel.ProductDetailAttributeItemResponse;
import com.main.wfm.apiresponsemodel.ProductDetailAttributeResponse;
import com.main.wfm.apiresponsemodel.ProductDetailDataItemsResponse;
import com.main.wfm.apiresponsemodel.ProductDetailOptionValueResponse;
import com.main.wfm.apiresponsemodel.ProductDetailOptionsResponse;
import com.main.wfm.apiresponsemodel.ProductDetailRelatedProductResponse;
import com.main.wfm.apiresponsemodel.ProductDetailResponse;
import com.main.wfm.apiresponsemodel.ProductImagesResponse;
import com.main.wfm.apiresponsemodel.RatingReviewResponse;
import com.main.wfm.apiresponsemodel.ReviewItemResponse;
import com.main.wfm.apiresponsemodel.StarRatingItemResponse;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private AppSession appSession;
    private String product_id, product_name;
    private ProductDetailDataItemsResponse productDetail;
    private List<ProductDetailOptionsResponse> productOptions;
    private List<ProductDetailAttributeResponse> productAttributes;
    private List<ProductDetailRelatedProductResponse> relatedProduct;
    private List<ProductImagesResponse> productImages;
    private ArrayList<String> imageArrayList;
    private LinearLayout llReview1, llReview2;
    private RecyclerView rvSimilarProduct, rvSize,rvThumbImage;
    public ImageView ivProductImage;
    private String ratingCount;
    private List<StarRatingItemResponse> star_rating_list;
    private List<ReviewItemResponse> review_list;
    private RelatedProductAdapter adapter;
    private SizeAdapter sizeAdapter;
    private ThumbImageAdapter thumbImageAdapter;
    private LinearLayout llProductDetailParent;
    private NestedScrollView scrollParent;
    private List<ProductDetailAttributeItemResponse> productAttributesItems;
    private List<ProductDetailOptionValueResponse> productOptionValues;
    private TextView tvQty, tvDecrease, tvIncrease,tvAddToCart,tvCheck,tvDeliveryAlert;
    private int producQquantity = 1;
    private FrameLayout flCart;
    private EditText etPinCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        getIds();
        ((ImageView) findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((TextView) findViewById(R.id.tvTitle)).setText(product_name);
    }

    private void getIds() {
        appSession = new AppSession(ProductDetailActivity.this);
        tvIncrease = findViewById(R.id.tvIncrease);
        tvQty = findViewById(R.id.tvQty);
        tvDecrease = findViewById(R.id.tvDecrease);
        tvAddToCart=findViewById(R.id.tvAddToCart);
        tvCheck=findViewById(R.id.tvCheck);
        tvDeliveryAlert=findViewById(R.id.tvDeliveryAlert);
        ivProductImage = findViewById(R.id.ivProductImage);
        llReview1 = findViewById(R.id.llReview1);
        llReview2 = findViewById(R.id.llReview2);
        llProductDetailParent = findViewById(R.id.llProductDetailParent);
        //llThumbnailParent= findViewById(R.id.llThumbnailParent);
        scrollParent = findViewById(R.id.scrollParent);
        rvSimilarProduct = findViewById(R.id.rvSimilarProduct);
        flCart=findViewById(R.id.flCart);
        rvSize = findViewById(R.id.rvSize);
        rvThumbImage=findViewById(R.id.rvThumbImage);
        etPinCode=findViewById(R.id.etPinCode);
        product_id = getIntent().getStringExtra("product_id");
        product_name = getIntent().getStringExtra("product_name");
        getProductDetailAPI("694");
        System.out.println("PRODUCT ID: " + product_id);

        flCart.setOnClickListener(this);
        tvCheck.setOnClickListener(this);
        tvDecrease.setOnClickListener(this);
        tvIncrease.setOnClickListener(this);
        tvAddToCart.setOnClickListener(this);
        ivProductImage.setOnClickListener(this);
        relatedProduct = new ArrayList<ProductDetailRelatedProductResponse>();
        rvSimilarProduct.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(ProductDetailActivity.this);
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        adapter = new RelatedProductAdapter(ProductDetailActivity.this, relatedProduct);
        if (relatedProduct.size() > 0 & relatedProduct != null) {
            rvSimilarProduct.setAdapter(adapter);
        }
        rvSimilarProduct.setLayoutManager(MyLayoutManager);


        productImages=new ArrayList<ProductImagesResponse>();
        rvThumbImage.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManagerSize2 = new LinearLayoutManager(ProductDetailActivity.this);
        MyLayoutManagerSize2.setOrientation(LinearLayoutManager.HORIZONTAL);
        thumbImageAdapter = new ThumbImageAdapter(ProductDetailActivity.this, productImages,ivProductImage);
        if (productImages.size() > 0 & productImages != null) {
            rvThumbImage.setAdapter(thumbImageAdapter);
        }
        rvThumbImage.setLayoutManager(MyLayoutManagerSize2);

        productOptionValues = new ArrayList<ProductDetailOptionValueResponse>();
        rvSize.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManagerSize = new LinearLayoutManager(ProductDetailActivity.this);
        MyLayoutManagerSize.setOrientation(LinearLayoutManager.HORIZONTAL);
        sizeAdapter = new SizeAdapter(ProductDetailActivity.this, productOptionValues);
        if (productOptionValues.size() > 0 & productOptionValues != null) {
            rvSize.setAdapter(sizeAdapter);
        }
        rvSize.setLayoutManager(MyLayoutManagerSize);

        if (appSession.getCartCount()>0){
            ((TextView)findViewById(R.id.tvCartCount)).setText(""+appSession.getCartCount());
        }

    }

    public void addToCartApi(final String productId) {
        final ViewDialog viewDialog = new ViewDialog(ProductDetailActivity.this);
        viewDialog.showDialog();
        AddToCartRequest request = new AddToCartRequest();
        request.setSource("2");
        request.setProduct_id(productId);
        request.setQty(""+producQquantity);
        request.setDevice_id(appSession.getDeviceId());

        NetworkAPI.addToCartApi(ProductDetailActivity.this, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.shakeAnimation(ProductDetailActivity.this,
                                        ((ImageView)findViewById(R.id.ivCart)));
                               CommonUtils.showToast(ProductDetailActivity.this,body.message);
                                ((TextView)findViewById(R.id.tvCartCount)).setText(""+producQquantity);
                                appSession.setCartCount(producQquantity);
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ProductDetailActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    private void setProductAttribute() {

        if (productAttributes != null && productAttributes.size() > 0) {
            ((TextView) findViewById(R.id.tvAttributeTitle)).setText(productAttributes.get(0).getName());
            productAttributesItems = new ArrayList<ProductDetailAttributeItemResponse>();
            productAttributesItems = productAttributes.get(0).getProductAttributesItems();
            if (productAttributesItems != null && productAttributesItems.size() > 0) {
                if (productAttributesItems.get(0).getName() != null && productAttributesItems.get(0).getText() != null) {
                    ((TextView) findViewById(R.id.tvFeatureType1)).setText(productAttributesItems.get(0).getName());
                    ((TextView) findViewById(R.id.tvFeatureText1)).setText(productAttributesItems.get(0).getText());
                }

                if (productAttributesItems.get(1).getName() != null && productAttributesItems.get(1).getText() != null) {
                    ((TextView) findViewById(R.id.tvFeatureType2)).setText(productAttributesItems.get(1).getName());
                    ((TextView) findViewById(R.id.tvFeatureText2)).setText(productAttributesItems.get(1).getText());
                }

                if (productAttributesItems.get(2).getName() != null && productAttributesItems.get(2).getText() != null) {
                    ((TextView) findViewById(R.id.tvFeatureType3)).setText(productAttributesItems.get(2).getName());
                    ((TextView) findViewById(R.id.tvFeatureText3)).setText(productAttributesItems.get(2).getText());
                }

                if (productAttributesItems.get(3).getName() != null && productAttributesItems.get(3).getText() != null) {
                    ((TextView) findViewById(R.id.tvFeatureType4)).setText(productAttributesItems.get(3).getName());
                    ((TextView) findViewById(R.id.tvFeatureText4)).setText(productAttributesItems.get(3).getText());
                }
            }
        }
        if (productAttributesItems.size() > 4) {

            ((LinearLayout) findViewById(R.id.llProductDetail)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tvProductDetailTitle)).setText(productAttributes.get(0).getName());

            ((TextView) findViewById(R.id.tvAllDetail)).setText("Get All " + productAttributes.get(0).getName() + "...");
            ((TextView) findViewById(R.id.tvAllDetail)).setPaintFlags(((TextView) findViewById(R.id.tvAllDetail)).getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) findViewById(R.id.tvAllDetail)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    scrollToView(scrollParent, llProductDetailParent);
                }
            });
        }

        for (int i = 0; i < productAttributesItems.size(); i++) {
            LinearLayout llPersonal = new LinearLayout(this);
            llPersonal.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams paramsPersonalValue = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            paramsPersonalValue.setMargins(0, 10, 0, 0);

            llPersonal.setLayoutParams(paramsPersonalValue);


            TextView tvName = new TextView(this);
            tvName.setText(productAttributesItems.get(i).getName());
            tvName.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
            tvName.setTextColor(getResources().getColor(R.color.text_color));
            tvName.setTextSize(12);
            tvName.setId(i);
            tvName.setPadding(10, 10, 10, 10);

            TextView tvDosage = new TextView(this);
            tvDosage.setText(productAttributesItems.get(i).getText());
            tvDosage.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
            tvDosage.setTextColor(getResources().getColor(R.color.text_color));
            tvDosage.setTextSize(12);
            tvDosage.setId(i);
            tvDosage.setPadding(10, 10, 10, 10);

            llPersonal.addView(tvName);
            llPersonal.addView(tvDosage);
            if (i % 2 == 0) {
                llPersonal.setBackgroundColor(getResources().getColor(R.color.gray));
                //llPersonal.setPadding(15,15,15,15);
            } else {
                llPersonal.setBackgroundColor(getResources().getColor(R.color.white));
            }
            //llPersonal.setPadding(10,10,10,10);
            llProductDetailParent.addView(llPersonal);
        }
        ((TextView)findViewById(R.id.tvDeliveryLabel)).setText(productDetail.getDelivery_text());
    }

    private void SetProductDetail() {
        getReviewRatingAPI("694");
        ((TextView) findViewById(R.id.tvName)).setText(productDetail.getName() + ",\n Pack of " + productDetail.getPack_quantity());

        if (productDetail.getRating() != null && productDetail.getReviews() != null) {
            ((LinearLayout) findViewById(R.id.llReviewRating)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tvRatingQty)).setText(productDetail.getRating());
            ((TextView) findViewById(R.id.tvRatingAndReview)).setText(productDetail.getRating() + " Rating and Reviews");
        } else {
            ((LinearLayout) findViewById(R.id.llReviewRating)).setVisibility(View.GONE);
        }
        if (productDetail.getPrice() != null && productDetail.getPrice().length() > 0 && productDetail.getSpecial() != null && productDetail.getSpecial().length() > 0) {
            ((TextView) findViewById(R.id.tvOldPrice)).setText("Rs " + productDetail.getPrice());
            ((TextView) findViewById(R.id.tvOldPrice)).setPaintFlags(((TextView) findViewById(R.id.tvOldPrice)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            ((TextView) findViewById(R.id.tvOldPrice)).setVisibility(View.VISIBLE);
        } else if (productDetail.getPrice() != null && productDetail.getPrice().length() > 0) {
            ((TextView) findViewById(R.id.tvOldPrice)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tvOldPrice)).setText("Rs " + productDetail.getPrice());
        } else {
            ((TextView) findViewById(R.id.tvOldPrice)).setVisibility(View.GONE);
        }

        if (productDetail.getSpecial() != null && productDetail.getSpecial().length() > 0) {
            ((TextView) findViewById(R.id.tvNewPrice)).setText("Rs " + productDetail.getSpecial());
            ((TextView) findViewById(R.id.tvNewPrice)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) findViewById(R.id.tvNewPrice)).setVisibility(View.GONE);
        }
        if (productDetail.getDiscount_off() != null && productDetail.getDiscount_off().length() > 0) {
            ((TextView) findViewById(R.id.tvOff)).setText(productDetail.getDiscount_off() + "% off");
            ((TextView) findViewById(R.id.tvOff)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) findViewById(R.id.tvOff)).setVisibility(View.GONE);
        }

        ((TextView) findViewById(R.id.tvSizeName)).setText(productOptions.get(0).getName());

        ((TextView) findViewById(R.id.tvDescription)).setText(productDetail.getDescription());

        productOptionValues = productOptions.get(0).getProductOptionValues();

        sizeAdapter = new SizeAdapter(ProductDetailActivity.this, productOptionValues);
        if (productOptionValues != null && productOptionValues.size() > 0) {
            rvSize.setAdapter(sizeAdapter);
        }

        tvQty.setText(productDetail.getMinimum());
        ((TextView) findViewById(R.id.tvPackPeice)).setText("Pack Quantity : " + productDetail.getPack_quantity() + " Peice");
        ((TextView) findViewById(R.id.tvPackQty)).setText("Min. Order Qty : " + productDetail.getMinimum() + " Pack");
    }

    public void getReviewRatingAPI(final String productId) {
        final ViewDialog viewDialog = new ViewDialog(ProductDetailActivity.this);
        viewDialog.showDialog();

        NetworkAPI.getProductReviewAndRatingApi(ProductDetailActivity.this, "Bearer " + appSession.getAuthToken(), productId,
                new Dispatch<RatingReviewResponse>() {
                    @Override
                    public void apiSuccess(RatingReviewResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getReviewRatingData() != null) {
                                ratingCount = body.getReviewRatingData().getTotal_rating();
                                review_list = body.getReviewRatingData().getReview_list();
                                star_rating_list = body.getReviewRatingData().getStar_rating_list();

                                setRatingReviewData();

                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ProductDetailActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    private void setRatingReviewData() {
        ((TextView) findViewById(R.id.tvRatingReviewCount)).setText(ratingCount);
        ((RatingBar) findViewById(R.id.rbProductRating)).setRating((float) Double.parseDouble(ratingCount));

        if (review_list != null && review_list.size() > 0) {
            llReview1.setVisibility(View.VISIBLE);
            llReview2.setVisibility(View.VISIBLE);
            if (review_list.size() == 1) {
                llReview1.setVisibility(View.VISIBLE);
                llReview2.setVisibility(View.GONE);
                ((TextView) findViewById(R.id.tvUserName1)).setText(review_list.get(0).getAuthor());
                //((TextView)findViewById(R.id.tvDateTime)).setText(review_list.get(0).getAuthor());
                ((TextView) findViewById(R.id.tvRatingReviewCount1)).setText(review_list.get(0).getUser_rating());
                ((RatingBar) findViewById(R.id.rbProductRating1)).setRating((float) Double.parseDouble(review_list.get(0).getUser_rating()));
                ((TextView) findViewById(R.id.tvDescriptionReview)).setText(review_list.get(0).getUser_review());
            } else if (review_list.size() == 2) {
                llReview1.setVisibility(View.VISIBLE);
                llReview2.setVisibility(View.VISIBLE);

                ((TextView) findViewById(R.id.tvUserName1)).setText(review_list.get(0).getAuthor());
                //((TextView)findViewById(R.id.tvDateTime)).setText(review_list.get(0).getAuthor());
                ((TextView) findViewById(R.id.tvRatingReviewCount1)).setText(review_list.get(0).getUser_rating());
                ((RatingBar) findViewById(R.id.rbProductRating1)).setRating((float) Double.parseDouble(review_list.get(0).getUser_rating()));
                ((TextView) findViewById(R.id.tvDescriptionReview)).setText(review_list.get(0).getUser_review());

                ((TextView) findViewById(R.id.tvUserName2)).setText(review_list.get(1).getAuthor());
                //((TextView)findViewById(R.id.tvDateTime)).setText(review_list.get(0).getAuthor());
                ((TextView) findViewById(R.id.tvRatingReviewCount2)).setText(review_list.get(1).getUser_rating());
                ((RatingBar) findViewById(R.id.rbProductRating2)).setRating((float) Double.parseDouble(review_list.get(1).getUser_rating()));
                ((TextView) findViewById(R.id.tvDescriptionReview2)).setText(review_list.get(1).getUser_review());


            } else if (review_list.size() > 2) {

                ((TextView) findViewById(R.id.tvShowAllReview)).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.tvShowAllReview)).setText("View All " + review_list.size() + " Reviews");
                ((TextView) findViewById(R.id.tvShowAllReview)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(ProductDetailActivity.this, ReviewListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("productId",product_id);
                        startActivity(intent);
                    }
                });
            }




        } else {
            llReview1.setVisibility(View.GONE);
            llReview2.setVisibility(View.GONE);
        }

        ((TextView)findViewById(R.id.tvRatingCount)).setText(star_rating_list.get(0).getCount()+" Reviews");
        ((TextView)findViewById(R.id.tvRatingCount1)).setText(star_rating_list.get(1).getCount()+" Reviews");
        ((TextView)findViewById(R.id.tvRatingCount2)).setText(star_rating_list.get(2).getCount()+" Reviews");
        ((TextView)findViewById(R.id.tvRatingCount3)).setText(star_rating_list.get(3).getCount()+" Reviews");
        ((TextView)findViewById(R.id.tvRatingCount4)).setText(star_rating_list.get(4).getCount()+" Reviews");

        ((ProgressBar)findViewById(R.id.progressBar)).getIndeterminateDrawable().setColorFilter(Color.parseColor(star_rating_list.get(0).getColor()),PorterDuff.Mode.SRC_IN);
        ((ProgressBar)findViewById(R.id.progressBar)).getProgressDrawable().setColorFilter(Color.parseColor(star_rating_list.get(0).getColor()),PorterDuff.Mode.SRC_IN);


        //((ProgressBar)findViewById(R.id.progressBar))
        ((ProgressBar)findViewById(R.id.progressBar1Rate)).getIndeterminateDrawable().setColorFilter(Color.parseColor(star_rating_list.get(1).getColor()),PorterDuff.Mode.SRC_IN);
        ((ProgressBar)findViewById(R.id.progressBar1Rate)).getProgressDrawable().setColorFilter(Color.parseColor(star_rating_list.get(1).getColor()),PorterDuff.Mode.SRC_IN);


        ((ProgressBar)findViewById(R.id.progressBar2Rate)).getIndeterminateDrawable().setColorFilter(Color.parseColor(star_rating_list.get(2).getColor()),PorterDuff.Mode.SRC_IN);
        ((ProgressBar)findViewById(R.id.progressBar2Rate)).getProgressDrawable().setColorFilter(Color.parseColor(star_rating_list.get(2).getColor()),PorterDuff.Mode.SRC_IN);

        ((ProgressBar)findViewById(R.id.progressBar3Rate)).getIndeterminateDrawable().setColorFilter(Color.parseColor(star_rating_list.get(3).getColor()),PorterDuff.Mode.SRC_IN);
        ((ProgressBar)findViewById(R.id.progressBar3Rate)).getProgressDrawable().setColorFilter(Color.parseColor(star_rating_list.get(3).getColor()),PorterDuff.Mode.SRC_IN);

        ((ProgressBar)findViewById(R.id.progressBar4Rate)).getIndeterminateDrawable().setColorFilter(Color.parseColor(star_rating_list.get(4).getColor()),PorterDuff.Mode.SRC_IN);
        ((ProgressBar)findViewById(R.id.progressBar4Rate)).getProgressDrawable().setColorFilter(Color.parseColor(star_rating_list.get(4).getColor()),PorterDuff.Mode.SRC_IN);


        int totalRating=0;
        for(int i=0;i<star_rating_list.size();i++){
            totalRating+= Integer.parseInt(star_rating_list.get(i).getCount());
        }
        if(totalRating<100){
            totalRating*=10;
        }
        ((ProgressBar)findViewById(R.id.progressBar)).setMax(totalRating);
        ((ProgressBar)findViewById(R.id.progressBar1Rate)).setMax(totalRating);
        ((ProgressBar)findViewById(R.id.progressBar2Rate)).setMax(totalRating);
        ((ProgressBar)findViewById(R.id.progressBar3Rate)).setMax(totalRating);
        ((ProgressBar)findViewById(R.id.progressBar4Rate)).setMax(totalRating);

        int progress1=Integer.parseInt(star_rating_list.get(0).getCount());
        int progress2=Integer.parseInt(star_rating_list.get(1).getCount());
        int progress3=Integer.parseInt(star_rating_list.get(2).getCount());
        int progress4=Integer.parseInt(star_rating_list.get(3).getCount());
        int progress5=Integer.parseInt(star_rating_list.get(4).getCount());


    if(totalRating<100){
        ((ProgressBar)findViewById(R.id.progressBar)).setProgress(10*(100*progress1)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar1Rate)).setProgress(10*(100*progress2)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar2Rate)).setProgress(10*(100*progress3)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar3Rate)).setProgress(10*(100*progress4)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar4Rate)).setProgress(10*(100*progress5)/totalRating);

    }else{
        ((ProgressBar)findViewById(R.id.progressBar)).setProgress((100*progress1)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar1Rate)).setProgress((100*progress2)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar2Rate)).setProgress((100*progress3)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar3Rate)).setProgress((100*progress4)/totalRating);
        ((ProgressBar)findViewById(R.id.progressBar4Rate)).setProgress((100*progress5)/totalRating);

    }


    }

    private void setProductImage() {
        Glide.with(ProductDetailActivity.this)
                .load(productDetail.getImage())
                .into(ivProductImage);

        thumbImageAdapter = new ThumbImageAdapter(ProductDetailActivity.this, productImages, ivProductImage);
        if (productImages.size() > 0 & productImages != null) {
            rvThumbImage.setAdapter(thumbImageAdapter);

        }
       /* for (int i=0;i<productImages.size();i++){

            ImageView ivThumb = new ImageView(this);
            //ivThumb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            ivThumb.setLayoutParams(new LinearLayout.LayoutParams(120, 120));
            Glide.with(ProductDetailActivity.this)
                    .load(productImages.get(i).getImage())
                    .into(ivThumb);

            ivThumb.setId(i);
            ivThumb.setPadding(10, 10, 10, 10);
            llThumbnailParent.addView(ivThumb);

            final int finalI = i;
            ivThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Glide.with(ProductDetailActivity.this)
                            .load(productImages.get(finalI).getImage())
                            .into(ivProductImage);
                }
            });
        }*/
    }

    public void getProductDetailAPI(final String productId) {
        final ViewDialog viewDialog = new ViewDialog(ProductDetailActivity.this);
        viewDialog.showDialog();
        ProductDetailRequest request = new ProductDetailRequest();
        request.setSource("2");
        request.setProduct_id(productId);

        NetworkAPI.getProductDetailApi(ProductDetailActivity.this, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<ProductDetailResponse>() {
                    @Override
                    public void apiSuccess(ProductDetailResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getProductDetailData() != null) {
                                productDetail = body.getProductDetailData().getProductDetail();
                                productAttributes = body.getProductDetailData().getProductAttributes();
                                productOptions = body.getProductDetailData().getProductOptions();
                                relatedProduct = body.getProductDetailData().getRelatedProduct();
                                productImages = body.getProductDetailData().getProductImages();
                                setProductAttribute();
                                adapter = new RelatedProductAdapter(ProductDetailActivity.this, relatedProduct);
                                if (relatedProduct.size() > 0 & relatedProduct != null) {
                                    rvSimilarProduct.setAdapter(adapter);
                                }

                                setProductImage();
                                SetProductDetail();
                                //producQquantity = Integer.parseInt(productDetail.getQuantity());
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(ProductDetailActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
    private void scrollToView(final NestedScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivProductImage:
                Intent intent = new Intent(ProductDetailActivity.this, ImageSliderActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("imageArray", (Serializable) productImages);
                startActivity(intent);
                break;
            case R.id.tvCheck:
                if(etPinCode.getText().length()==0){
                    CommonUtils.showToast(ProductDetailActivity.this,"Pincode is required");
                }else{
                    checkDeliveryApi(etPinCode.getText().toString().trim(),productDetail.getWeight());
                }

                break;
            case R.id.flCart:
                 intent = new Intent(ProductDetailActivity.this, CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.tvAddToCart:
                addToCartApi(product_id);
                break;
            case R.id.tvIncrease:
                if (producQquantity <= Integer.parseInt(productDetail.getQuantity())) {
                    producQquantity = Integer.parseInt(tvQty.getText().toString());
                    producQquantity += 1;
                    tvQty.setText("" + producQquantity);
                }

                break;
            case R.id.tvDecrease:
                if (Integer.parseInt(productDetail.getMinimum()) < producQquantity) {
                    producQquantity = Integer.parseInt(tvQty.getText().toString());
                    producQquantity -= 1;
                    tvQty.setText("" + producQquantity);
                } else {

                }
                break;
        }
    }

    private void checkDeliveryApi(String pin_code,String weight) {
        final ViewDialog viewDialog = new ViewDialog(ProductDetailActivity.this);
        viewDialog.showDialog();

        AddToCartRequest request=new AddToCartRequest();
        request.setPin_code(pin_code);
        //request.setWeight(weight);
        request.setWeight("5");
        Call<JsonResponse> call = ApiExecutor.getApiService(ProductDetailActivity.this).checkDeliveryApi("Bearer "+appSession.getAuthToken(),request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API url ---" + call.request());

        call.enqueue(new Callback<JsonResponse>() {
                         @Override
                         public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Dashboard Data " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:

                                     tvDeliveryAlert.setVisibility(View.VISIBLE);
                                     tvDeliveryAlert.setText(response.body().message);
                                     tvDeliveryAlert.setTextColor(getResources().getColor(R.color.green_btn));

                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), ProductDetailActivity.this);
                                     break;
                                 case 401:
                                     error = ErrorUtils.parseError(response);
                                     //CommonUtils.showAlertOk(error.getMessage(), ProductDetailActivity.this);
                                     tvDeliveryAlert.setVisibility(View.VISIBLE);
                                     tvDeliveryAlert.setText(error.getMessage());
                                     tvDeliveryAlert.setTextColor(getResources().getColor(R.color.red_google_btn));
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<JsonResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }
}
