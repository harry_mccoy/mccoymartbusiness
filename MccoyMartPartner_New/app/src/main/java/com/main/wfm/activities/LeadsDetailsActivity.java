package com.main.wfm.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.LeadsClosedStatusListAdapter;
import com.main.wfm.adapters.LeadsDetailAdapter;
import com.main.wfm.apirequestmodel.LeadsRequestModel;
import com.main.wfm.apiresponsemodel.LeadsDataListResponse;
import com.main.wfm.apiresponsemodel.LeadsDetailsDataObjectResponse;
import com.main.wfm.apiresponsemodel.LeadsDetailsDataResponse;
import com.main.wfm.apiresponsemodel.LeadsDetailsResponse;
import com.main.wfm.apiresponsemodel.LeadsResponse;
import com.main.wfm.models.LeadStatusSubPopUpDataBean;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.Permissions;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadsDetailsActivity extends AppCompatActivity implements View.OnClickListener,LeadsClosedStatusListAdapter.LeadsclosedStatusAdapterListener {


    private List<LeadStatusSubPopUpDataBean>mLeadClosedPopUpDataBeanList;

    private String leadId, companyId;
    private AppSession appSession;
    private ListView lvLeadsDetails;
    private TextView tv_statusNew, tv_statusContacted, tv_statusQuotation, tv_statusClose;
    private ImageView iv_statusClose;
    public TextView tvname, tvAddress, tvType, tvRequirement, tvPrice, tvDate;
    public ImageView ivCall;
    private LeadsDetailsDataResponse leadsDetailsData;
    private List<LeadsDetailsDataObjectResponse> arlEnquiry;
    private ViewGroup header;
    private LeadsDetailAdapter adapter;
    private LinearLayout llStatusClose;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_detail);
        appSession = new AppSession(LeadsDetailsActivity.this);
        lvLeadsDetails = findViewById(R.id.lvLeadsDetails);
        leadId = getIntent().getStringExtra("lead_id");
        companyId = getIntent().getStringExtra("company_id");

        //Adding Header To List View
        LayoutInflater inflaterHeader = getLayoutInflater();
        header = (ViewGroup) inflaterHeader.inflate(R.layout.leads_status_details, lvLeadsDetails, false);
        lvLeadsDetails.addHeaderView(header);
        getIds();

        arlEnquiry=new ArrayList<LeadsDetailsDataObjectResponse>();
        adapter=new LeadsDetailAdapter(LeadsDetailsActivity.this,arlEnquiry);
        lvLeadsDetails.setAdapter(adapter);
        getLeadsDetailsAPI(leadId, companyId);
        selectedLeadStatus("new");
    }

    private void getIds() {

        tv_statusNew = header.findViewById(R.id.tv_statusNew);
        tv_statusContacted = header.findViewById(R.id.tv_statusContacted);
        tv_statusQuotation = header.findViewById(R.id.tv_statusQuotation);
        tv_statusClose = header.findViewById(R.id.tv_statusClose);
        tvname = header.findViewById(R.id.tvName);
        tvAddress = header.findViewById(R.id.tvAddress);
        tvType = header.findViewById(R.id.tvType);
        tvRequirement = header.findViewById(R.id.tvRequirement);
        tvPrice = header.findViewById(R.id.tvPrice);
        tvDate = header.findViewById(R.id.tvDate);
        iv_statusClose = header.findViewById(R.id.iv_statusClose);
        llStatusClose=header.findViewById(R.id.ll_statusClose);

        tv_statusClose.setOnClickListener(this);
        tv_statusContacted.setOnClickListener(this);
        tv_statusNew.setOnClickListener(this);
        tv_statusQuotation.setOnClickListener(this);
        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        llStatusClose.setOnClickListener(this);




    }

    private void selectedLeadStatus(String selected) {
        switch (selected) {
            case "new":
                tv_statusNew.setTextColor(ContextCompat.getColor(this, R.color.white));
                ((LinearLayout) header.findViewById(R.id.ll_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_seleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusNew)).setBackgroundResource(R.drawable.ic_leads_arrow_selected_right_side);

                tv_statusContacted.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusContacted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusContracted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusContracted)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_right_side);

                tv_statusQuotation.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusQuotation)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_right_side);

                ((LinearLayout) header.findViewById(R.id.ll_statusClose)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                tv_statusClose.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                iv_statusClose.setColorFilter(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));

                break;

            case "connected":
                tv_statusContacted.setTextColor(ContextCompat.getColor(this, R.color.white));
                ((LinearLayout) header.findViewById(R.id.ll_statusContacted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_seleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusContracted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusContracted)).setBackgroundResource(R.drawable.ic_leads_arrow_selected_right_side);

                tv_statusNew.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusNew)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_left_side);

                tv_statusQuotation.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusQuotation)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_right_side);

                ((LinearLayout) header.findViewById(R.id.ll_statusClose)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                tv_statusClose.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                iv_statusClose.setColorFilter(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                break;

            case "quotation":

                tv_statusQuotation.setTextColor(ContextCompat.getColor(this, R.color.white));
                ((LinearLayout) header.findViewById(R.id.ll_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_seleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusQuotation)).setBackgroundResource(R.drawable.ic_leads_arrow_selected_right_side);

                tv_statusContacted.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusContacted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusContracted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusContracted)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_left_side);

                tv_statusNew.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusNew)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_right_side);

                ((LinearLayout) header.findViewById(R.id.ll_statusClose)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                tv_statusClose.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                iv_statusClose.setColorFilter(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));


                break;

            case "closed":

                tv_statusNew.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusNew)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusNew)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_right_side);

                tv_statusContacted.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusContacted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusContracted)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusContracted)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_right_side);

                tv_statusQuotation.setTextColor(ContextCompat.getColor(this, R.color.leads_status_text_unSelected));
                ((LinearLayout) header.findViewById(R.id.ll_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((LinearLayout) header.findViewById(R.id.ll_view_statusQuotation)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_unseleced));
                ((View) header.findViewById(R.id.view_statusQuotation)).setBackgroundResource(R.drawable.ic_leads_arrow_unselected_left_side);


                tv_statusClose.setTextColor(ContextCompat.getColor(this, R.color.white));
                iv_statusClose.setColorFilter(ContextCompat.getColor(this, R.color.white));
                ((LinearLayout) header.findViewById(R.id.ll_statusClose)).setBackgroundColor(ContextCompat.getColor(this, R.color.leads_status_background_seleced));

                break;
        }


    }

    private void getLeadsDetailsAPI(String leadId, String companyId) {
        final ViewDialog viewDialog = new ViewDialog(LeadsDetailsActivity.this);
        viewDialog.showDialog();

        LeadsRequestModel request = new LeadsRequestModel();
        request.setLead_id(leadId);
        request.setCompany_id(companyId);

        Call<LeadsDetailsResponse> call = ApiExecutor.getApiService(LeadsDetailsActivity.this).getLeadsDetailsApi
                ("Bearer " + appSession.getAuthToken(), request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<LeadsDetailsResponse>() {
                         @Override
                         public void onResponse(Call<LeadsDetailsResponse> call, Response<LeadsDetailsResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Dashboard Data " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     leadsDetailsData = response.body().getLeadsDetailsData();
                                     arlEnquiry=leadsDetailsData.getObjEnquiry();
                                     adapter=new LeadsDetailAdapter(LeadsDetailsActivity.this,arlEnquiry);
                                     lvLeadsDetails.setAdapter(adapter);

                                     setBuyerDetails(response.body().getLeadsDetailsData().getObjBuyer());
                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), LeadsDetailsActivity.this);
                                     break;
                                 case 401:
                                     error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), LeadsDetailsActivity.this);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<LeadsDetailsResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }

    private void setBuyerDetails(final LeadsDataListResponse objBuyer) {

        ((TextView)findViewById(R.id.tvName)).setText(objBuyer.getName());
        ((TextView)findViewById(R.id.tvAddress)).setText(objBuyer.getLocation());
        ((TextView)findViewById(R.id.tvMobile)).setText(objBuyer.getMobile());
        ((TextView)findViewById(R.id.tvEmail)).setText(objBuyer.getEmail());
        ((TextView)findViewById(R.id.tvDate)).setText(objBuyer.getDate());

        ((ImageView)findViewById(R.id.ivCall)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Permissions.checkPermissionCall(LeadsDetailsActivity.this)) {
                    String uri = "tel:" + objBuyer.getMobile() ;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                }else {
                    Permissions.requestPermissionCall(LeadsDetailsActivity.this);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ll_statusClose:
                showClosedStatusDialog(this, llStatusClose, mLeadClosedPopUpDataBeanList, this);
                break;

            case R.id.tv_statusNew:
                selectedLeadStatus("new");
                break;

            case R.id.tv_statusContacted:
                selectedLeadStatus("connected");
                break;

            case R.id.tv_statusQuotation:
                selectedLeadStatus("quotation");
                break;
            case R.id.tv_statusClose:
                selectedLeadStatus("close");
                break;

        }
    }

    public static void showClosedStatusDialog(Activity context, View belowVIew, List<LeadStatusSubPopUpDataBean> list,LeadsClosedStatusListAdapter.LeadsclosedStatusAdapterListener listener) {
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.5);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.8);
        View layout = context.getLayoutInflater().inflate(R.layout.dialog_leads_filter_list, null);
        PopupWindow closedPopUp = new PopupWindow(layout, width, ViewGroup.LayoutParams.WRAP_CONTENT);
        closedPopUp.setBackgroundDrawable(new BitmapDrawable());
        closedPopUp.setOutsideTouchable(true);
        closedPopUp.setContentView(layout);
        closedPopUp.setFocusable(true);
        closedPopUp.showAsDropDown(belowVIew);
        RecyclerView rvTest = layout.findViewById(R.id.rv_recyclerView);
        rvTest.setHasFixedSize(true);
        rvTest.setLayoutManager(new LinearLayoutManager(context));

        LeadsClosedStatusListAdapter rvAdapter = new LeadsClosedStatusListAdapter(context, list, listener, closedPopUp);
        rvTest.setAdapter(rvAdapter);
        rvAdapter.notifyDataSetChanged();


    }

    @Override
    public void onClosedStatusItemSelected(LeadStatusSubPopUpDataBean bean, int position) {

    }
}
