package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BusinessEditDetailsDataBean {
    @SerializedName("company_type")
    private List<BusinessRoleDataBean> businessRoleDataBeanList;

    @SerializedName("legal_status")
    private List<BusinessRoleDataBean> businessLegalDataBeanList;

    public List<BusinessRoleDataBean> getBusinessRoleDataBeanList() {
        return businessRoleDataBeanList;
    }

    public void setBusinessRoleDataBeanList(List<BusinessRoleDataBean> businessRoleDataBeanList) {
        this.businessRoleDataBeanList = businessRoleDataBeanList;
    }

    public List<BusinessRoleDataBean> getBusinessLegalDataBeanList() {
        return businessLegalDataBeanList;
    }

    public void setBusinessLegalDataBeanList(List<BusinessRoleDataBean> businessLegalDataBeanList) {
        this.businessLegalDataBeanList = businessLegalDataBeanList;
    }
}
