package com.main.wfm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.main.wfm.R;


/**
 * Created by RSShah
 */

public class PageTwoFragment extends Fragment {

    private Context mContext;
    private View view;
    private ImageView ivPic;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (AppCompatActivity)context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tutorial_page_two, container, false);



        return view;
    }

    public static androidx.fragment.app.Fragment newInstance() {
        
        Bundle args = new Bundle();
        
        PageTwoFragment fragment = new PageTwoFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
