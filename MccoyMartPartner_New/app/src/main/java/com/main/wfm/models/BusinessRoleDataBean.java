package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class BusinessRoleDataBean {
    @SerializedName("id")
    private int roleId;
    @SerializedName("name")
    private String roleTitle;
    private boolean isChecked;
    private int status;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
