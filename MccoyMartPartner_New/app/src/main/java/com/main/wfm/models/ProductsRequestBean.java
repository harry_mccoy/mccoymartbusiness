package com.main.wfm.models;

/**
 * Created on 06-05-2019.
 */
public class ProductsRequestBean {
    /**
     * search :
     * company_id :
     * status :
     */

    private String search="a";
    private String company_id="";
    private String status="";

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
