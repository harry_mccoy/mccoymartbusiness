package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class LeadFilterDataBean {

    @SerializedName("title")
    private String title;

    @SerializedName("value")
    private String value;

    private boolean isChecked;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
