package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class LeadDetailsRequestBean {
    @SerializedName("lead_id")
    private String leadId;

    @SerializedName("company_id")
    private String companyId;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
