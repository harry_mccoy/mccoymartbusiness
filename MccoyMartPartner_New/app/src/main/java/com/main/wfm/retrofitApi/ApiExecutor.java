package com.main.wfm.retrofitApi;

import android.content.Context;


import com.main.wfm.utils.AppSession;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RSShah on 12/01/2016.
 */
public class ApiExecutor {
    private static String baseUrl;
    private static Retrofit retrofit;


    public static ApiService getApiService(Context mContext) {

        AppSession appSession = new AppSession(mContext);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

            baseUrl = RequestUrl.BASE_URL;

        retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        //retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit.create(ApiService.class);
    }

}