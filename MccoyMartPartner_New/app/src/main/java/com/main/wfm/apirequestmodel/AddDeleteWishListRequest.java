package com.main.wfm.apirequestmodel;

import com.google.gson.annotations.SerializedName;

public class AddDeleteWishListRequest {

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("address_id")
    private String address_id;

    @SerializedName("payment_method")
    private String payment_method;



    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    @SerializedName("action")
    private String action;

    @SerializedName("source")
    private String source;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    @SerializedName("device_id")
    private String device_id;


    @SerializedName("product_id")
    private String product_id;
    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String source) {
        this.action = source;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }
}
