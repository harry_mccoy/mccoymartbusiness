package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProjectInfoDataBean {
    @SerializedName("objRegistered")
    private UpdateProjectInfoResponseBean objRegistered;
    @SerializedName("style")
    private List<String> style;
    @SerializedName("project_type")
    private List<String> project_type;
    @SerializedName("project_cost")
    private List<String> project_cost;
    @SerializedName("objImages")
    private List<ProjectImagesModel> objImages;


    public UpdateProjectInfoResponseBean getObjRegistered() {
        return objRegistered;
    }

    public void setObjRegistered(UpdateProjectInfoResponseBean objRegistered) {
        this.objRegistered = objRegistered;
    }

    public List<String> getStyle() {
        return style;
    }

    public void setStyle(List<String> style) {
        this.style = style;
    }

    public List<String> getProject_type() {
        return project_type;
    }

    public void setProject_type(List<String> project_type) {
        this.project_type = project_type;
    }

    public List<String> getProject_cost() {
        return project_cost;
    }

    public void setProject_cost(List<String> project_cost) {
        this.project_cost = project_cost;
    }


    public List<ProjectImagesModel> getObjImages() {
        return objImages;
    }

    public void setObjImages(List<ProjectImagesModel> objImages) {
        this.objImages = objImages;
    }
}
