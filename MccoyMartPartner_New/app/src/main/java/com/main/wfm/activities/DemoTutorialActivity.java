package com.main.wfm.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.main.wfm.R;
import com.main.wfm.fragments.PageOneFragment;
import com.main.wfm.fragments.PageThreeFragment;
import com.main.wfm.fragments.PageTwoFragment;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CircleIndicator;
import com.main.wfm.utils.OnIndicatorClickListener;

import java.util.Timer;


/**
 * Created by RSShah
 */

public class DemoTutorialActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager vPagerTutorial;
    private CircleIndicator indicator;
    int currentPage = 0;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.

    Timer timer;
    private TextView tvLogin, tvScheduleAppointment;
    private AppSession appSession;
    private ProgressDialog mDialog;
    private String inputLine = "";
    private TextView tvNext;
    private int prescriptionTabCount=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_demo_tutorial);
        appSession=new AppSession(DemoTutorialActivity.this);
        vPagerTutorial=findViewById(R.id.vPagerTutorial);
        indicator = (CircleIndicator)findViewById(R.id.indicator);
        ((TextView)findViewById(R.id.tvNext)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DemoTutorialActivity.this, LoginStepOneActivity.class);
                startActivity(intent);
                finish();

            }
        });
        appSession.setDemoComplete("Complete");
        setViewPager();
    }
    private void setViewPager() {
        vPagerTutorial.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        //vPagerTutorial.setOffscreenPageLimit(1);

        /*After setting the adapter use the timer */

        vPagerTutorial.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        indicator.setOnIndicatorClickListener(new OnIndicatorClickListener() {
            @Override
            public void onIndicatorClick(int position) {
                vPagerTutorial.setCurrentItem(position, true);
            }
        });
        indicator.setViewPager(vPagerTutorial);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
        private class MyPagerAdapter extends FragmentPagerAdapter {

            public MyPagerAdapter

                    (FragmentManager fm) {
                super(fm);
            }

            @Override
            public Fragment getItem(int pos) {
                switch(pos) {
                    case 0: return PageOneFragment.newInstance();
                    case 1: return PageTwoFragment.newInstance();
                    case 2: return PageThreeFragment.newInstance();

                    default: return PageOneFragment.newInstance();
                }
            }

            @Override
            public int getCount() {
                return 3;
            }
        }
}
