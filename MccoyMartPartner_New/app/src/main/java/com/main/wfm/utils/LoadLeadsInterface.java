package com.main.wfm.utils;

public interface LoadLeadsInterface {

    public void loadLeadData(String leadType,String filter,String search);
}
