package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class ReviewItemResponse {



    @SerializedName("user_review")
    private String user_review;

    @SerializedName("author")
    private String author;

    @SerializedName("review_id")
    private String review_id;

    @SerializedName("user_rating")
    private String user_rating;

    @SerializedName("date")
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser_review() {
        return user_review;
    }

    public void setUser_review(String user_review) {
        this.user_review = user_review;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }
}
