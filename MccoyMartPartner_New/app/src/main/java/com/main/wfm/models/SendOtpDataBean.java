package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class SendOtpDataBean {
    @SerializedName("user_id")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
