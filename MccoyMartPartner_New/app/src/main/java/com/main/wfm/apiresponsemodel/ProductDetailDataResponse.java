package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetailDataResponse {
    @SerializedName("product_detail")
    private ProductDetailDataItemsResponse productDetail;

    @SerializedName("product_options")
    private List<ProductDetailOptionsResponse> productOptions;

    @SerializedName("product_attributes")
    private List<ProductDetailAttributeResponse> productAttributes;

    @SerializedName("related_product")
    private List<ProductDetailRelatedProductResponse> relatedProduct;

    @SerializedName("product_images")
    private List<ProductImagesResponse> productImages;

    public ProductDetailDataItemsResponse getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetailDataItemsResponse productDetail) {
        this.productDetail = productDetail;
    }

    public List<ProductDetailOptionsResponse> getProductOptions() {
        return productOptions;
    }

    public void setProductOptions(List<ProductDetailOptionsResponse> productOptions) {
        this.productOptions = productOptions;
    }

    public List<ProductDetailAttributeResponse> getProductAttributes() {
        return productAttributes;
    }

    public void setProductAttributes(List<ProductDetailAttributeResponse> productAttributes) {
        this.productAttributes = productAttributes;
    }

    public List<ProductDetailRelatedProductResponse> getRelatedProduct() {
        return relatedProduct;
    }

    public void setRelatedProduct(List<ProductDetailRelatedProductResponse> relatedProduct) {
        this.relatedProduct = relatedProduct;
    }

    public List<ProductImagesResponse> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<ProductImagesResponse> productImages) {
        this.productImages = productImages;
    }
}
