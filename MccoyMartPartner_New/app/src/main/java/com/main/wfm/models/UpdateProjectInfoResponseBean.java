package com.main.wfm.models;

public class UpdateProjectInfoResponseBean {

    /**
     * project_id : 43
     * company_id : 14373
     * project_name : test project
     * project_slug : kunar-test
     * style : modern
     * project_type : commercial
     * project_cost : 2000
     * project_year : 2013
     * project_location : sxsxsx
     * description : xzxzxzx
     */

    private int project_id;
    private int company_id;
    private String project_name;
    private String project_slug;
    private String style;
    private String project_type;
    private String project_cost;
    private String project_year;
    private String project_location;
    private String description;

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_slug() {
        return project_slug;
    }

    public void setProject_slug(String project_slug) {
        this.project_slug = project_slug;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getProject_type() {
        return project_type;
    }

    public void setProject_type(String project_type) {
        this.project_type = project_type;
    }

    public String getProject_cost() {
        return project_cost;
    }

    public void setProject_cost(String project_cost) {
        this.project_cost = project_cost;
    }

    public String getProject_year() {
        return project_year;
    }

    public void setProject_year(String project_year) {
        this.project_year = project_year;
    }

    public String getProject_location() {
        return project_location;
    }

    public void setProject_location(String project_location) {
        this.project_location = project_location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
