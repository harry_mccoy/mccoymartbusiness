package com.main.wfm.models;

import java.util.List;

public class DashboardListDataBean {

    private int uiType;

    private List<DashboardCountDataBean> dashboardCountDataBeanList;
    private List<DashboardRecentLeadsDataBean> dashboardRecentLeadsDataBeanList;
    private DashboardCompanyProfileDataBean dashboardCompanyProfileDataBean;
    private List<String> dashboardGraphYearList;

    public int getUiType() {
        return uiType;
    }

    public void setUiType(int uiType) {
        this.uiType = uiType;
    }

    public List<DashboardCountDataBean> getDashboardCountDataBeanList() {
        return dashboardCountDataBeanList;
    }

    public void setDashboardCountDataBeanList(List<DashboardCountDataBean> dashboardCountDataBeanList) {
        this.dashboardCountDataBeanList = dashboardCountDataBeanList;
    }

    public List<DashboardRecentLeadsDataBean> getDashboardRecentLeadsDataBeanList() {
        return dashboardRecentLeadsDataBeanList;
    }

    public void setDashboardRecentLeadsDataBeanList(List<DashboardRecentLeadsDataBean> dashboardRecentLeadsDataBeanList) {
        this.dashboardRecentLeadsDataBeanList = dashboardRecentLeadsDataBeanList;
    }

    public DashboardCompanyProfileDataBean getDashboardCompanyProfileDataBean() {
        return dashboardCompanyProfileDataBean;
    }

    public void setDashboardCompanyProfileDataBean(DashboardCompanyProfileDataBean dashboardCompanyProfileDataBean) {
        this.dashboardCompanyProfileDataBean = dashboardCompanyProfileDataBean;
    }

    public List<String> getDashboardGraphYearList() {
        return dashboardGraphYearList;
    }

    public void setDashboardGraphYearList(List<String> dashboardGraphYearList) {
        this.dashboardGraphYearList = dashboardGraphYearList;
    }
}
