package com.main.wfm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.main.wfm.R;


/**
 * Created by RSShah
 */

public class PageOneFragment extends Fragment {

    private Context mContext;
    private View view;
    private ImageView ivPic;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (AppCompatActivity)context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tutorial_page_one, container, false);
        //ivPic = (ImageView)view.findViewById(R.id.ivPic);
        // Picasso.with(mContext).load(R.drawable.demo_one).resize(720, 720).into(ivPic);

        return view;
    }

    public static PageOneFragment newInstance() {

        Bundle args = new Bundle();

        PageOneFragment fragment = new PageOneFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
