package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCityListByAreaResponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String statusMessage;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("data")
    private List<CityListDataBean> cityListDataBeanList;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<CityListDataBean> getCityListDataBeanList() {
        return cityListDataBeanList;
    }

    public void setCityListDataBeanList(List<CityListDataBean> cityListDataBeanList) {
        this.cityListDataBeanList = cityListDataBeanList;
    }
}
