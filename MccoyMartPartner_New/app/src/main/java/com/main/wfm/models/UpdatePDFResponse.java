package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdatePDFResponse {

    /**
     * status : true
     * message : Successfully
     * statusCode : 200
     * data : [{"pdf_file1":"http://45.118.135.192/wfm_new/uploads/company_pdfs/1556003874162-wfm-india.pdf","pdf_file2":"http://45.118.135.192/wfm_new/uploads/company_pdfs/1556006393224-wfm-india.pdf","pdf_file3":"http://45.118.135.192/wfm_new/uploads/company_pdfs/1556006412609-wfm-india.pdf","pdf_file4":"http://45.118.135.192/wfm_new/uploads/company_pdfs/1556006402861-wfm-india.pdf"},{"nextkey":"pdf_file5"}]
     */

    @SerializedName("status")
    public boolean status;
    @SerializedName("message")
    public String statusMessage;
    @SerializedName("statusCode")
    public int statusCode;
    private List<DataBean> data;



    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public static class DataBean {
        /**
         * pdf_file1 : http://45.118.135.192/wfm_new/uploads/company_pdfs/1556003874162-wfm-india.pdf
         * pdf_file2 : http://45.118.135.192/wfm_new/uploads/company_pdfs/1556006393224-wfm-india.pdf
         * pdf_file3 : http://45.118.135.192/wfm_new/uploads/company_pdfs/1556006412609-wfm-india.pdf
         * pdf_file4 : http://45.118.135.192/wfm_new/uploads/company_pdfs/1556006402861-wfm-india.pdf
         * nextkey : pdf_file5
         */

        private String pdf_file1;
        private String pdf_file2;
        private String pdf_file3;
        private String pdf_file4;
        private String nextkey;

        public String getPdf_file1() {
            return pdf_file1;
        }

        public void setPdf_file1(String pdf_file1) {
            this.pdf_file1 = pdf_file1;
        }

        public String getPdf_file2() {
            return pdf_file2;
        }

        public void setPdf_file2(String pdf_file2) {
            this.pdf_file2 = pdf_file2;
        }

        public String getPdf_file3() {
            return pdf_file3;
        }

        public void setPdf_file3(String pdf_file3) {
            this.pdf_file3 = pdf_file3;
        }

        public String getPdf_file4() {
            return pdf_file4;
        }

        public void setPdf_file4(String pdf_file4) {
            this.pdf_file4 = pdf_file4;
        }

        public String getNextkey() {
            return nextkey;
        }

        public void setNextkey(String nextkey) {
            this.nextkey = nextkey;
        }
    }
}
