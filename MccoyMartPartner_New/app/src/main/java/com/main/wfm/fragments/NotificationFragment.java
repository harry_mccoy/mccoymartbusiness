package com.main.wfm.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.adapters.LeadsAdapter;
import com.main.wfm.adapters.NotificationListAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.LeadsDataListResponse;
import com.main.wfm.apiresponsemodel.NotificationListDataResponse;
import com.main.wfm.apiresponsemodel.NotificationListResponse;
import com.main.wfm.apiresponsemodel.RatingReviewResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.VerticalLineDecorator;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends Fragment {
    private Context mContext;
    private NotificationListAdapter adapter;
    private List<NotificationListDataResponse> notificationList;
    private RecyclerView rvNotification;
    private AppSession appSession;
    private LinearLayout llEmptyNotificationFrame;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notification, container, false);
        getIds(root);
        return root;
    }

    private void getIds(View root) {
        rvNotification=root.findViewById(R.id.rvNotification);
        llEmptyNotificationFrame=root.findViewById(R.id.llEmptyNotificationFrame);

        appSession=new AppSession(mContext);
        rvNotification.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        notificationList=new ArrayList<NotificationListDataResponse>();
        adapter=new NotificationListAdapter(mContext,notificationList);
        rvNotification.setAdapter(adapter);
        rvNotification.addItemDecoration(new VerticalLineDecorator(2));
        rvNotification.setLayoutManager(MyLayoutManager);

        getNotificationList();
    }


    public void getNotificationList() {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();


        NetworkAPI.getNotificationListResponse(mContext, "Bearer " + appSession.getAuthToken(),
                new Dispatch<NotificationListResponse>() {
                    @Override
                    public void apiSuccess(NotificationListResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getNotificationList() != null) {
                               notificationList=body.getNotificationList();
                               if (notificationList!=null&&notificationList.size()>0){
                                   rvNotification.setVisibility(View.VISIBLE);
                                   llEmptyNotificationFrame.setVisibility(View.GONE);
                                   adapter=new NotificationListAdapter(mContext,notificationList);
                                   rvNotification.setAdapter(adapter);
                               }else{
                                   rvNotification.setVisibility(View.GONE);
                                   llEmptyNotificationFrame.setVisibility(View.VISIBLE);
                               }


                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    public NotificationFragment() {


    }

    public static NotificationFragment newInstance(int position) {
        NotificationFragment f = new NotificationFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

}