package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryProductSubCategoryDataResponse {
    @SerializedName("image")
    private String icon;

    @SerializedName("category_id")
    private String category_id;

    @SerializedName("category_name")
    private String category_name;

    @SerializedName("parent_id")
    private String parent_id;


    private String icon2;
    private String category_id2;
    private String category_name2;
    private String parent_id2;

    public String getIcon2() {
        return icon2;
    }

    public void setIcon2(String icon2) {
        this.icon2 = icon2;
    }

    public String getCategory_id2() {
        return category_id2;
    }

    public void setCategory_id2(String category_id2) {
        this.category_id2 = category_id2;
    }

    public String getCategory_name2() {
        return category_name2;
    }

    public void setCategory_name2(String category_name2) {
        this.category_name2 = category_name2;
    }

    public String getParent_id2() {
        return parent_id2;
    }

    public void setParent_id2(String parent_id2) {
        this.parent_id2 = parent_id2;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }


}
