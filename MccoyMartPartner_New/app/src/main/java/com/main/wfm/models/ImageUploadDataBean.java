package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class ImageUploadDataBean {
    @SerializedName("image_url")
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
