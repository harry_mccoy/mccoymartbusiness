package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class UpdateNotificationTokenRequestBean {
    @SerializedName("fcm_token")
    private String fcmToken;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
