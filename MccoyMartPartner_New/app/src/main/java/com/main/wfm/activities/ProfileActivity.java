package com.main.wfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.main.wfm.R;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;

public class ProfileActivity extends AppCompatActivity {


    private AppSession appSession;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        appSession=new AppSession(ProfileActivity.this);

        ((TextView)findViewById(R.id.tvUserName)).setText(appSession.getUserName());
        ((TextView)findViewById(R.id.tvMobile)).setText(appSession.getPhoneNo());
        ((TextView)findViewById(R.id.tvEmail)).setText(appSession.getEmailId());

        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((FrameLayout)findViewById(R.id.flCart)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        ((TextView)findViewById(R.id.tvMyOrder)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, MyOrdersActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        ((TextView)findViewById(R.id.tvMyWishList)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, MyWishListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        ((TextView)findViewById(R.id.tvLogout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.logOutAlert(ProfileActivity.this,getString(R.string.logout_alert),appSession);
            }
        });

        ((TextView)findViewById(R.id.tvManageAddress)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, ManageAddressActivity.class);
                intent.putExtra("addressType","editAddress");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

    }
}
