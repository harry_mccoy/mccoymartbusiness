package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class SendOtpRequestBean {
    @SerializedName("username")
    private String username;
    @SerializedName("key")
    private String requestType;
    @SerializedName("user_id")
    private int userId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
