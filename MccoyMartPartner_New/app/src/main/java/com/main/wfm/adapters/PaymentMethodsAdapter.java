package com.main.wfm.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.CartActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apirequestmodel.AddToCartRequest;
import com.main.wfm.apiresponsemodel.CartListDataResponse;
import com.main.wfm.apiresponsemodel.PaymentMethodItemsResponse;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.List;

public class PaymentMethodsAdapter extends BaseAdapter {
    private List<PaymentMethodItemsResponse> paymentMethodList;
    private Context mContext;
    private LayoutInflater infalter;
    private AppSession appSession;
    //private int producQquantity = 1;
    //private int selectedPosition;
    private boolean[]radioPosition;;
    private ViewHolder holder;
    private int[] arTempQty;

    public PaymentMethodsAdapter(Context mContext, List<PaymentMethodItemsResponse> paymentMethodList) {
        this.mContext = mContext;
        this.paymentMethodList = paymentMethodList;
        appSession=new AppSession(this.mContext);
        arTempQty=new int[this.paymentMethodList.size()];
        radioPosition=new boolean[this.paymentMethodList.size()];
        /*for (int i = 0; i<this.paymentMethodList.size(); i++){
            arTempQty[i]=Integer.parseInt(this.paymentMethodList.get(i).getCart_quantity());
        }*/
        infalter = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {

        return paymentMethodList.size();
    }

    @Override
    public Object getItem(int position) {

        return paymentMethodList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = infalter.inflate(R.layout.row_payment_method, null);
            holder = new ViewHolder();
            holder.radioPaymentMethodType=convertView.findViewById(R.id.radioPaymentMethodType);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.radioPaymentMethodType.setText(paymentMethodList.get(position).getTitle());

        if(radioPosition[position]){
            holder.radioPaymentMethodType.setChecked(true);

            addPaymentMethodApi(paymentMethodList.get(position).getCode());
        }else {
            holder.radioPaymentMethodType.setChecked(false);

        }
        holder.radioPaymentMethodType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<radioPosition.length;i++){
                    if(i==position){
                        radioPosition[i]=true;
                    }else{
                        radioPosition[i]=false;
                    }
                }
                notifyDataSetChanged();
            }
        });


        return convertView;
    }

    public class ViewHolder {
       public RadioButton radioPaymentMethodType;
    }



    public void addPaymentMethodApi(String paymentMethod) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setSource("2");
        request.setDevice_id(appSession.getDeviceId());
        request.setPayment_method(paymentMethod);
        NetworkAPI.addPaymentMethodApi(mContext, "Bearer " + appSession.getAuthToken(),request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.showToast(mContext,body.message);
                                notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, mContext.getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}