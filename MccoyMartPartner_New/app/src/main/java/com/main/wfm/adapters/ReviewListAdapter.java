package com.main.wfm.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.main.wfm.R;
import com.main.wfm.activities.LeadsDetailsActivity;
import com.main.wfm.apiresponsemodel.NotificationListDataResponse;
import com.main.wfm.apiresponsemodel.ReviewItemResponse;

import java.util.List;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.MyViewHolder> {
    private List<ReviewItemResponse> reviewList;
    private Context mContext;

    public ReviewListAdapter(Context mContext, List<ReviewItemResponse> reviewList) {
        this.reviewList = reviewList;
        this.mContext = mContext;
    }

    @Override
    public ReviewListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_review_list, parent, false);
        ReviewListAdapter.MyViewHolder holder = new ReviewListAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ReviewListAdapter.MyViewHolder holder, final int position) {
        holder.tvDateTime.setText(reviewList.get(position).getDate());
        holder.tvDescriptionReview.setText(reviewList.get(position).getUser_review());
        holder.tvDateTime.setText(reviewList.get(position).getDate());
        holder.tvRatingReviewCount.setText(reviewList.get(position).getUser_rating());
        holder.rbProductRating.setRating((float) Double.parseDouble(reviewList.get(position).getUser_rating()));

    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDescriptionReview, tvRatingReviewCount, tvDateTime, tvUserName;

        private RatingBar rbProductRating;


        public MyViewHolder(View v) {
            super(v);
            tvDescriptionReview = (TextView) v.findViewById(R.id.tvDescriptionReview);
            tvRatingReviewCount = (TextView) v.findViewById(R.id.tvRatingReviewCount);
            tvDateTime = (TextView) v.findViewById(R.id.tvDateTime);
            tvUserName = (TextView) v.findViewById(R.id.tvUserName);
            rbProductRating=v.findViewById(R.id.rbProductRating);

        }
    }
}