package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class LeadsDataListResponse {


    @SerializedName("lead_id")
    private String lead_id;

    @SerializedName("id")
    private String id;

    @SerializedName("company_id")
    private String company_id;

    @SerializedName("name")
    private String name;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("email")
    private String email;

    @SerializedName("Category")
    private String Category;

    @SerializedName("date")
    private String date;

    @SerializedName("location")
    private String location;

    @SerializedName("glv")
    private String glv;

    @SerializedName("description")
    private String description;

    @SerializedName("marked")
    private String marked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGlv() {
        return glv;
    }

    public void setGlv(String glv) {
        this.glv = glv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMarked() {
        return marked;
    }

    public void setMarked(String marked) {
        this.marked = marked;
    }
}
