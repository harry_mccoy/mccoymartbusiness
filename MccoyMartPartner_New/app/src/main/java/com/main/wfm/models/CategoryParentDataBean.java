package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class CategoryParentDataBean {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("business_type")
    private int businessType;

    boolean isChecked;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBusinessType() {
        return businessType;
    }

    public void setBusinessType(int businessType) {
        this.businessType = businessType;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
