package com.main.wfm.adapters;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.CategoryProductsActivity;
import com.main.wfm.activities.LoginStepOneActivity;
import com.main.wfm.activities.LoginStepTwoActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.CategoryProductResponse;
import com.main.wfm.models.ListDataModel;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.utils.AppConstants;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CircleTransformation;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {
    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopFeaturedCatBeans;
    private Context mContext;
    private AppSession appSession;
    public HorizontalAdapter(Context mContext, List<ProductDataDashboardShopBean> arlProductDataDashboardShopFeaturedCatBeans) {
        this.arlProductDataDashboardShopFeaturedCatBeans = arlProductDataDashboardShopFeaturedCatBeans;
        this.mContext=mContext;
        appSession=new AppSession(this.mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lv_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(arlProductDataDashboardShopFeaturedCatBeans.get(position).getName().contains("&")){
            holder.tvname.setText(arlProductDataDashboardShopFeaturedCatBeans.get(position).getName().replace("&","\n"));
        }else if(arlProductDataDashboardShopFeaturedCatBeans.get(position).getName().contains(" ")){
            holder.tvname.setText(arlProductDataDashboardShopFeaturedCatBeans.get(position).getName().replace(" ","\n"));
        }else {
            holder.tvname.setText(arlProductDataDashboardShopFeaturedCatBeans.get(position).getName());
        }
        holder.tvname.setTypeface(CommonUtils.setCustomFont(mContext, AppConstants.REGULAR_FONT));

        //holder.iv.setImageResource(imageModelArrayList.get(position).getImage_drawable());

        Picasso.with(mContext).load(arlProductDataDashboardShopFeaturedCatBeans.get(position).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).transform(new CircleTransformation()).resize(120,120).into(holder.iv);

        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCategoryProductsAPI(arlProductDataDashboardShopFeaturedCatBeans.get(position).getCategory_id(),position);


            }
        });

    }

    @Override
    public int getItemCount() {
        return arlProductDataDashboardShopFeaturedCatBeans.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvname;
        public ImageView iv;
        public LinearLayout llItem;


        public MyViewHolder(View v) {
            super(v);
            tvname = (TextView) v.findViewById(R.id.name);
            iv = (ImageView) v.findViewById(R.id.imgView);
            llItem=v.findViewById(R.id.llItem);


        }
    }

    private void getCategoryProductsAPI(final String categoryId, final int position) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();

        NetworkAPI.getCategoryProductsApi(mContext, "Bearer " + appSession.getAuthToken(), categoryId,
                new Dispatch<CategoryProductResponse>() {

                    @Override
                    public void apiSuccess(CategoryProductResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));
                        viewDialog.hideDialog();
                        if (body != null) {
                            if (body.getData() != null && body.getData().size() > 0) {
                                viewDialog.hideDialog();
                                Intent intent = new Intent(mContext, CategoryProductsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("category_id",arlProductDataDashboardShopFeaturedCatBeans.get(position).getCategory_id());
                                intent.putExtra("category_name",arlProductDataDashboardShopFeaturedCatBeans.get(position).getName());
                                mContext.startActivity(intent);
                                //setCategoryListData(body.getData());

                            } else {
                                CommonUtils.showToast(mContext,"No data found.");
                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext,mContext.getResources().getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();

                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}