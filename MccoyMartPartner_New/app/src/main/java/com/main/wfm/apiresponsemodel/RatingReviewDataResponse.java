package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatingReviewDataResponse {
    @SerializedName("total_rating")
    private String total_rating;


    @SerializedName("star_rating")
    private List<StarRatingItemResponse> star_rating_list;

    @SerializedName("review")
    private List<ReviewItemResponse> review_list;

    public List<ReviewItemResponse> getReview_list() {
        return review_list;
    }

    public void setReview_list(List<ReviewItemResponse> review_list) {
        this.review_list = review_list;
    }

    public String getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(String total_rating) {
        this.total_rating = total_rating;
    }

    public List<StarRatingItemResponse> getStar_rating_list() {
        return star_rating_list;
    }

    public void setStar_rating_list(List<StarRatingItemResponse> star_rating_list) {
        this.star_rating_list = star_rating_list;
    }
}
