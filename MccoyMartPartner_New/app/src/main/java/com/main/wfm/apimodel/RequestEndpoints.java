package com.main.wfm.apimodel;





import androidx.annotation.NonNull;

import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apirequestmodel.AddEditAddressRequest;
import com.main.wfm.apirequestmodel.AddToCartRequest;
import com.main.wfm.apirequestmodel.LeadsRequestModel;
import com.main.wfm.apirequestmodel.ProductDetailRequest;
import com.main.wfm.apiresponsemodel.CartListResponse;
import com.main.wfm.apiresponsemodel.CartTotalResponse;
import com.main.wfm.apiresponsemodel.CategoryProductResponse;
import com.main.wfm.apiresponsemodel.LeadsDetailsResponse;
import com.main.wfm.apiresponsemodel.LeadsFilterResponse;
import com.main.wfm.apiresponsemodel.LeadsResponse;
import com.main.wfm.apiresponsemodel.ManageAddressResponse;
import com.main.wfm.apiresponsemodel.MyOrderDataResponse;
import com.main.wfm.apiresponsemodel.NotificationListResponse;
import com.main.wfm.apiresponsemodel.PaymentMethodResponse;
import com.main.wfm.apiresponsemodel.ProductCommonResponse;
import com.main.wfm.apiresponsemodel.ProductDetailResponse;
import com.main.wfm.apiresponsemodel.RatingReviewResponse;
import com.main.wfm.apiresponsemodel.SideMenuResponse;
import com.main.wfm.apiresponsemodel.WishListResponse;
import com.main.wfm.models.DashBoardShopRequestBean;
import com.main.wfm.models.DashboardDetailsShopResponse;
import com.main.wfm.models.GetProductOfferRequestBean;
import com.main.wfm.models.LeadStatusUpdateRequestBean;
import com.main.wfm.models.LeadStatusUpdateResponse;
import com.main.wfm.models.LoginRequestBean;
import com.main.wfm.models.OtpVerificationRequestBean;
import com.main.wfm.models.OtpVerificationResponse;
import com.main.wfm.models.ProductInfoResponseBean;
import com.main.wfm.models.SignUpRequestBean;
import com.main.wfm.models.SignUpResponse;
import com.main.wfm.retrofitApi.JsonResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RequestEndpoints {
    @POST("send-otp")
    @Headers( {"content-type: application/json", })
    Call<JsonResponse> sendOtpApi(@Body LoginRequestBean body);

    @POST("signup")
    Call<SignUpResponse> SignUpApi(@Body SignUpRequestBean body);

    @POST("verify-otp")
    Call<OtpVerificationResponse> OTPVerficationApi(@Body OtpVerificationRequestBean body);


    @POST("homeModuleList")
    Call<DashboardDetailsShopResponse> dashBoardShopApi(@Body DashBoardShopRequestBean body);

    @POST("getofferproducts")
    Call<ProductInfoResponseBean> getProductOffersApi(@Body GetProductOfferRequestBean body);

    @POST("lead/1")
    Call<LeadsResponse> getLeadsApi(@Header("Authorization") String authorization, @Body LeadsRequestModel body);

    @POST("lead")
    Call<LeadsDetailsResponse> getLeadsDetailsApi(@Header("Authorization") String authorization, @Body LeadsRequestModel body);

    @NonNull
    @GET("navigation")
    @Headers( {"content-type: application/json",})
    Call<SideMenuResponse> getSideMenuListApi(@Header("Authorization") String authorization);

    @POST("lead-update/")
    Call<LeadStatusUpdateResponse> updateLeadStatusApi(@Body LeadStatusUpdateRequestBean body);

    @NonNull
    @GET("lead/filters")
    @Headers( {"content-type: application/json",})
    Call<LeadsFilterResponse> getLeadsFilters(@Header("Authorization") String authorization);

    @NonNull
    @GET("sort-order")
    @Headers( {"content-type: application/json",})
    Call<LeadsFilterResponse> getSortOrder(@Header("Authorization") String authorization);

    @POST("todaydealproducts/1")
    Call<ProductCommonResponse> getTodaysProductApi(@Header("Authorization") String authorization, @Body GetProductOfferRequestBean body);

    @POST("latestproducts/1")
    Call<ProductCommonResponse> getLatestProductApi(@Header("Authorization") String authorization, @Body GetProductOfferRequestBean body);

    @POST("getproducts/1")
    Call<ProductCommonResponse> getProductApi(@Header("Authorization") String authorization, @Body GetProductOfferRequestBean body);

    @GET("notification/list/1")
    Call<NotificationListResponse> getNotificationListResponse(@Header("Authorization") String authorization);

    @GET("notification/offer/1")
    Call<NotificationListResponse> getOfferListResponse(@Header("Authorization") String authorization);


    @POST("wishlist")
    Call<WishListResponse> getWishListApi(@Header("Authorization") String authorization);


    @POST("action_wishlist")
    Call<JsonResponse> deleteAddItemToWishListApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);


    @POST("getallmanufacturer")
    Call<ProductInfoResponseBean> getTopBrandsProductApi(@Header("Authorization") String authorization, @Body GetProductOfferRequestBean body);

    @NonNull
    @POST("category/{category_id}")
    Call<CategoryProductResponse> getCategoryProductsApi(@Header("Authorization") String authorization, @Path("category_id") String category_id);


    @NonNull
    @GET("review_rating/{product_id}")
    Call<RatingReviewResponse> getProductReviewAndRatingApi(@Header("Authorization") String authorization, @Path("product_id") String product_id);

    @NonNull
    @GET("address-list")
    Call<ManageAddressResponse> getAddressApi(@Header("Authorization") String authorization);

    @NonNull
    @POST("productdetail")
    Call<ProductDetailResponse> getProductDetailApi(@Header("Authorization") String authorization, @Body ProductDetailRequest body);

    @NonNull
    @POST("addtocart")
    Call<JsonResponse> addToCartApi(@Header("Authorization") String authorization, @Body AddToCartRequest body);

    @NonNull
    @POST("removecart")
    Call<JsonResponse> removeFromCartApi(@Header("Authorization") String authorization, @Body AddToCartRequest body);



    @NonNull
    @POST("cartlist")
    Call<CartListResponse> getCartListApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);

    @NonNull
    @POST("carttotal")
    Call<CartTotalResponse> getCartTotalApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);

    @NonNull
    @POST("add_edit_address")
    Call<JsonResponse> addEditAddressApi(@Header("Authorization") String authorization, @Body AddEditAddressRequest body);

    @NonNull
    @POST("delete-address")
    Call<JsonResponse> deleteAddressApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);

    @NonNull
    @POST("paymentmethods")
    Call<PaymentMethodResponse> paymentMethodsApi(@Header("Authorization") String authorization, @Body AddToCartRequest body);


    @NonNull
    @POST("save-for-later")
    Call<JsonResponse> saveForLaterApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);


    @NonNull
    @POST("addpaymentmethod")
    Call<JsonResponse> addPaymentMethodApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);

    @NonNull
    @POST("addorder")
    Call<JsonResponse> addOrderApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);

    @NonNull
    @POST("category/{page}")
    Call<MyOrderDataResponse> getMyOrderApi(@Header("Authorization") String authorization, @Path("page") int page);

    @NonNull
    @POST("write-review")
    Call<JsonResponse> writeReviewApi(@Header("Authorization") String authorization, @Body AddDeleteWishListRequest body);


}





