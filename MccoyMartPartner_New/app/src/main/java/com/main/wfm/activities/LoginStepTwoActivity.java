package com.main.wfm.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.models.LoginRequestBean;
import com.main.wfm.models.OtpVerificationRequestBean;
import com.main.wfm.models.OtpVerificationResponse;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.smsreader.SmsListner;
import com.main.wfm.smsreader.SmsReceiver;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginStepTwoActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvContinue,tvResendOtp;
    private String otpFromMsg="";
    private AppSession appSession;
    private EditText etCode1,etCode2,etCode3,etCode4;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_steptwo);
        appSession=new AppSession(LoginStepTwoActivity.this);
        getIds();
    }

    private void getIds() {
        tvContinue=findViewById(R.id.tvContinue);
        tvResendOtp=findViewById(R.id.tvResendOtp);
        etCode1=findViewById(R.id.etCode1);
        etCode2=findViewById(R.id.etCode2);
        etCode3=findViewById(R.id.etCode3);
        etCode4=findViewById(R.id.etCode4);
        tvContinue.setOnClickListener(this);
        tvResendOtp.setOnClickListener(this);

        etCode1.addTextChangedListener(new GenericTextWatcher(etCode1));
        etCode2.addTextChangedListener(new GenericTextWatcher(etCode2));
        etCode3.addTextChangedListener(new GenericTextWatcher(etCode3));
        etCode4.addTextChangedListener(new GenericTextWatcher(etCode4));

        tvResendOtp.setPaintFlags(tvResendOtp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        etCode1.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etCode1, InputMethodManager.SHOW_IMPLICIT);

        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

       setOtpFromSms();
    }

    private void setOtpFromSms() {
        SmsReceiver.bindListener(new SmsListner() {
            @Override
            public void messageReceived(String messageText) {

                otpFromMsg=messageText;
                etCode1.setText(""+messageText.charAt(0));
                etCode2.setText(""+messageText.charAt(1));
                etCode3.setText(""+messageText.charAt(2));
                etCode4.setText(""+messageText.charAt(3));
            }
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tvResendOtp:
                sendOtpAPI(getIntent().getStringExtra("username"));
                break;
            case R.id.tvContinue:
                String otpEntered;
                otpEntered=etCode1.getText().toString().trim()+etCode2.getText().toString().trim()+
                        etCode3.getText().toString().trim()+etCode4.getText().toString().trim();
                System.out.println("OTP From MSG: "+otpFromMsg);
                if(otpEntered.equalsIgnoreCase(otpFromMsg)){

                    verifyOTPAPI(getIntent().getStringExtra("username"),otpFromMsg);


                }else{
                    if(otpFromMsg.length()==0){
                        verifyOTPAPI(getIntent().getStringExtra("username"),otpEntered);
                    }else{
                        CommonUtils.showAlertOk("Please enter correct OTP.", LoginStepTwoActivity.this);
                    }

                }
              /*  Intent intent = new Intent(LoginStepTwoActivity.this, LoginStepThreeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("otp",otpFromMsg);
                intent.putExtra("username",getIntent().getStringExtra("username"));
                startActivity(intent);*/
                break;

        }
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.etCode1:
                    if (text.length() == 1)
                        etCode2.requestFocus();
                    break;
                case R.id.etCode2:
                    if (text.length() == 1)
                        etCode3.requestFocus();
                    else if (text.length() == 0)
                        etCode1.requestFocus();
                    break;
                case R.id.etCode3:
                    if (text.length() == 1)
                        etCode4.requestFocus();
                    else if (text.length() == 0)
                        etCode2.requestFocus();
                    break;
                case R.id.etCode4:
                    if (text.length() == 0)
                        etCode3.requestFocus();
                    break;
            }
        }

    }

    private void verifyOTPAPI(String mobile,String otp) {

        //final ProgressDialog dialog = ProgressDialog.show(SelectOptionActivity.this, null, getString(R.string.loading));
        final ViewDialog viewDialog = new ViewDialog(this);
        viewDialog.showDialog();
        OtpVerificationRequestBean request = new OtpVerificationRequestBean();


        request.setDeviceType("2");
        request.setMobile(mobile);
        request.setAction("other");
        request.setDeviceId(appSession.getDeviceId());
        request.setPassword("");
        request.setFcmId("");
        request.setOtp(otp);
        ////device_type for web=1,Android=2,IOS=3
        Call<OtpVerificationResponse> call = ApiExecutor.getApiService(LoginStepTwoActivity.this).OTPVerficationApi(request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<OtpVerificationResponse>() {
                         @Override
                         public void onResponse(Call<OtpVerificationResponse> call, Response<OtpVerificationResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Send Device Token " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     //System.out.println("DATA: "+response.body().getOtpVerificationDataBean().getUserId());
                                     if(response.body().getOtpVerificationDataBean().getUserId()!=null){
                                         if(response.body().getOtpVerificationDataBean().getUserId()!=null){
                                             appSession.setAuthToken(response.body().getOtpVerificationDataBean()
                                             .getToken());
                                             appSession.setUserId(response.body().getOtpVerificationDataBean().getUserId());
                                               appSession.setUserData(response.body().getOtpVerificationDataBean().getName(),
                                                       "","",response.body().getOtpVerificationDataBean().getEmail(),response.body().getOtpVerificationDataBean().getPhone());
                                             Intent intent = new Intent(LoginStepTwoActivity.this, DashBoardActivity.class);
                                             intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                             startActivity(intent);
                                         }
                                     }else{
                                         appSession.setUserId("");
                                         Intent intent = new Intent(LoginStepTwoActivity.this, LoginStepThreeActivity.class);
                                         intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                         intent.putExtra("otp",otpFromMsg);
                                         intent.putExtra("username",getIntent().getStringExtra("username"));
                                         startActivity(intent);
                                     }


                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(),LoginStepTwoActivity.this);
                                     break;
                                 case 401:
                                      error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(),LoginStepTwoActivity.this);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<OtpVerificationResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }

    private void sendOtpAPI(String mobile) {

        //final ProgressDialog dialog = ProgressDialog.show(SelectOptionActivity.this, null, getString(R.string.loading));
        final ViewDialog viewDialog = new ViewDialog(this);
        viewDialog.showDialog();
        LoginRequestBean request = new LoginRequestBean();


        request.setDeviceType("2");
        request.setUsername(mobile);
        request.setAction("signup");
        Call<JsonResponse> call = ApiExecutor.getApiService(LoginStepTwoActivity.this).sendOtpApi(request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<JsonResponse>() {
                         @Override
                         public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                             viewDialog.hideDialog();
                             //System.out.println("Send Devi e Token " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     etCode1.setText("");
                                     etCode2.setText("");
                                     etCode3.setText("");
                                     etCode4.setText("");
                                     setOtpFromSms();

                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(),LoginStepTwoActivity.this);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<JsonResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }
}
