package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;
import com.main.wfm.models.AddressListItembean;

import java.util.List;

public class CartDataResponse {
    @SerializedName("sub_total")
    private String sub_total;

    @SerializedName("cart_list")
    private List<CartListDataResponse> cart_list;

    @SerializedName("default_address")
    private AddressListItembean default_address;

    @SerializedName("cart_total")
    private List<CartListDataResponse> cart_total;


    public String getSub_total() {
        return sub_total;
    }

    public void setSub_total(String sub_total) {
        this.sub_total = sub_total;
    }

    public List<CartListDataResponse> getCart_list() {
        return cart_list;
    }

    public void setCart_list(List<CartListDataResponse> cart_list) {
        this.cart_list = cart_list;
    }

    public List<CartListDataResponse> getCart_total() {
        return cart_total;
    }

    public void setCart_total(List<CartListDataResponse> cart_total) {
        this.cart_total = cart_total;
    }

    public AddressListItembean getDefault_address() {
        return default_address;
    }

    public void setDefault_address(AddressListItembean default_address) {
        this.default_address = default_address;
    }
}
