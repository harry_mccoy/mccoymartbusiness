package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartListDataResponse {

   // public String totalItem,totalItemQty,totalDiscount,deliveryCharge,totalPayble;

    @SerializedName("code")
    private String code;

    @SerializedName("title")
    private String title;

    @SerializedName("value")
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @SerializedName("cart_quantity")
    private String cart_quantity;

    @SerializedName("cart_date_added")
    private String cart_date_added;

    @SerializedName("model")
    private String model;

    @SerializedName("price")
    private String price;

    @SerializedName("special")
    private String special;

    @SerializedName("product_sku")
    private String product_sku;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @SerializedName("manufacturer")
    private String manufacturer;

    @SerializedName("product_id")
    private String product_id;

    /*@SerializedName("rating")
    private String rating;

    @SerializedName("review")
    private String review;

    @SerializedName("discount")
    private String discount;*/

    private boolean isFooter;
   /* public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }*/

    public String getCart_quantity() {
        return cart_quantity;
    }

    public void setCart_quantity(String cart_quantity) {
        this.cart_quantity = cart_quantity;
    }

    public String getCart_date_added() {
        return cart_date_added;
    }

    public void setCart_date_added(String cart_date_added) {
        this.cart_date_added = cart_date_added;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getProduct_sku() {
        return product_sku;
    }

    public void setProduct_sku(String product_sku) {
        this.product_sku = product_sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

   /* public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }*/

    public boolean getIsFooter() {
        return isFooter;
    }

    public void setIsFooter(boolean isFooter) {
        this.isFooter = isFooter;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
