package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class ProductDetailDataItemsResponse {

    @SerializedName("weight")
    private String weight;

    @SerializedName("description")
    private String description;

    @SerializedName("discount")
    private String discount;

    @SerializedName("discount_off")
    private String discount_off;

    @SerializedName("image")
    private String image;

    @SerializedName("manufacturer")
    private String manufacturer;

    @SerializedName("manufacturer_id")
    private String manufacturer_id;

    @SerializedName("manufacturer_image")
    private String manufacturer_image;

    @SerializedName("minimum")
    private String minimum;

    @SerializedName("model")
    private String model;

    @SerializedName("name")
    private String name;

    @SerializedName("offers")
    private String offers;

    @SerializedName("pack_quantity")
    private String pack_quantity;

    @SerializedName("points")
    private String points;

    @SerializedName("price")
    private String price;

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("product_sku")
    private String product_sku;

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("rating")
    private String rating;

    @SerializedName("reviews")
    private String reviews;

    @SerializedName("reward")
    private String reward;

    @SerializedName("sku")
    private String sku;

    @SerializedName("special")
    private String special;

    @SerializedName("status")
    private String status;

    @SerializedName("stock_status")
    private String stock_status;

    @SerializedName("tag")
    private String tag;

    @SerializedName("viewed")
    private String viewed;

    @SerializedName("delivery_text")
    private String delivery_text;

    public String getDelivery_text() {
        return delivery_text;
    }

    public void setDelivery_text(String delivery_text) {
        this.delivery_text = delivery_text;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_off() {
        return discount_off;
    }

    public void setDiscount_off(String discount_off) {
        this.discount_off = discount_off;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer_id() {
        return manufacturer_id;
    }

    public void setManufacturer_id(String manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }

    public String getManufacturer_image() {
        return manufacturer_image;
    }

    public void setManufacturer_image(String manufacturer_image) {
        this.manufacturer_image = manufacturer_image;
    }

    public String getMinimum() {
        return minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOffers() {
        return offers;
    }

    public void setOffers(String offers) {
        this.offers = offers;
    }

    public String getPack_quantity() {
        return pack_quantity;
    }

    public void setPack_quantity(String pack_quantity) {
        this.pack_quantity = pack_quantity;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_sku() {
        return product_sku;
    }

    public void setProduct_sku(String product_sku) {
        this.product_sku = product_sku;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStock_status() {
        return stock_status;
    }

    public void setStock_status(String stock_status) {
        this.stock_status = stock_status;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getViewed() {
        return viewed;
    }

    public void setViewed(String viewed) {
        this.viewed = viewed;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
