package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class GetUserProfileResponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String statusMessage;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("data")
    private UserProfileDataBean userProfileDataBean;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public UserProfileDataBean getUserProfileDataBean() {
        return userProfileDataBean;
    }

    public void setUserProfileDataBean(UserProfileDataBean userProfileDataBean) {
        this.userProfileDataBean = userProfileDataBean;
    }
}
