package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ManageAddressResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private int statusCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }



    @SerializedName("data")
    private List<ManageAddressItemResponse> addressList;


    public List<ManageAddressItemResponse> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<ManageAddressItemResponse> addressList) {
        this.addressList = addressList;
    }
}
