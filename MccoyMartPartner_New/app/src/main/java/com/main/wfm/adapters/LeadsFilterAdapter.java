package com.main.wfm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.main.wfm.R;
import com.main.wfm.activities.LeadsActivity;
import com.main.wfm.apiresponsemodel.LeadsFilterDataResponse;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.utils.LoadLeadsEvent;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class LeadsFilterAdapter extends BaseAdapter {

    private static final String TAG = LeadsFilterAdapter.class.getSimpleName();
    private Context context;
    private List<LeadsFilterDataResponse> leadsFilterDataResponses;
    private LayoutInflater infalter;
    private String leadType,search;

    int selectedPosition = 0;
    public LeadsFilterAdapter(Context context, List<LeadsFilterDataResponse> leadsFilterDataResponses,String leadType,String search) {
        this.context = context;
        this.leadsFilterDataResponses = leadsFilterDataResponses;
        this.leadType=leadType;
        this.search=search;
        infalter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return leadsFilterDataResponses.size();
    }

    @Override
    public Object getItem(int position) {

        return leadsFilterDataResponses.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = infalter.inflate(R.layout.row_leads_filter, null);
            holder = new ViewHolder();
            holder.radioLatest = (RadioButton) convertView.findViewById(R.id.radioLatest);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            holder.radioLatest.setText(leadsFilterDataResponses.get(position).getTitle());
        holder.radioLatest.setChecked(position == selectedPosition);
        holder.radioLatest.setTag(position);

        holder.radioLatest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = (Integer)holder.radioLatest.getTag();
                notifyDataSetChanged();
                ((LeadsActivity)context).disableFilterDialog();

                LoadLeadsEvent loadLeadsEvent = new LoadLeadsEvent(leadType,leadsFilterDataResponses.get(position).getValue(), search);
                EventBus.getDefault().post(loadLeadsEvent);
            }
        });
        return convertView;
    }

    public class ViewHolder {
        RadioButton radioLatest;
    }
}
