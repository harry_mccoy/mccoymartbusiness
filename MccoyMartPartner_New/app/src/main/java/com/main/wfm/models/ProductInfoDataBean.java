package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductInfoDataBean {
    @SerializedName("company_id")
    private String companyId;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("is_completed")
    private int isCompleted;
    @SerializedName("Categories")
    private List<CityListDataBean> categoriesList;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }



    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<CityListDataBean> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<CityListDataBean> categoriesList) {
        this.categoriesList = categoriesList;
    }
}
