package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class SendOtpResponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String statusMessage;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("data")
    private SendOtpDataBean sendOtpDataBean;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public SendOtpDataBean getSendOtpDataBean() {
        return sendOtpDataBean;
    }

    public void setSendOtpDataBean(SendOtpDataBean sendOtpDataBean) {
        this.sendOtpDataBean = sendOtpDataBean;
    }
}
