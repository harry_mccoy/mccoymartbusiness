package com.main.wfm.adapters;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.main.wfm.R;
import com.main.wfm.apiresponsemodel.LeadsDetailsDataObjectResponse;
import com.main.wfm.apiresponsemodel.SideMenuDataItemsResponse;

import java.util.ArrayList;
import java.util.List;

public class LeadsDetailAdapter extends BaseAdapter {
    private Context context;
    private List<LeadsDetailsDataObjectResponse> arlEnquiry;

    public LeadsDetailAdapter() {
        super();
    }

    public LeadsDetailAdapter(Context context, List<LeadsDetailsDataObjectResponse> arlEnquiry) {
        this.context = context;
        this.arlEnquiry = arlEnquiry;
        System.out.println("SIZE OF LIST: "+ arlEnquiry.size());
        //this.originalItem = item;
    }

    @Override
    public int getCount() {
        return arlEnquiry.size();
    }

    @Override
    public Object getItem(int position) {
        return arlEnquiry.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            // if section header
            convertView = inflater.inflate(R.layout.row_leads_details, parent, false);
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvValue = (TextView) convertView.findViewById(R.id.tvValue);


            tvName.setText(arlEnquiry.get(position).getText());
        tvValue.setText(arlEnquiry.get(position).getValue());

        return convertView;
    }
}