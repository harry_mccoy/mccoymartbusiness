package com.main.wfm.utils;

import com.google.gson.Gson;

public class AppConstants {

	public static final int PERMISSION_REQUEST_CODE_LOCATION = 5000;
	public static final int PERMISSION_REQUEST_CODE_STORAGE = 5001;
	public static final int PERMISSION_REQUEST_CODE_CAMERA = 5002;
	public static final int PERMISSION_REQUEST_CODE_ACCOUNT= 5003;
	public static final int PERMISSION_REQUEST_CODE_CALENDAR = 5004;
	public static final int PERMISSION_REQUEST_CODE_CONTACT = 5005;
	public static final int PERMISSION_REQUEST_CODE_CALL = 5006;
	public static final int PERMISSION_REQUEST_CODE_CALL_LOG = 5007;
	public static final int PERMISSION_REQUEST_CODE_READ_SMS = 5008;

	public static final String REGULAR_FONT = "fonts/roboto.regular.ttf";
	public static final String BOLD_FONT = "fonts/roboto.bold.ttf";

	public static Gson GSON = new Gson();
	}
