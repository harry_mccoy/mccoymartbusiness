package com.main.wfm.models;

/**
 * Created on 22-05-2019.
 */
public class ProductInfoRequestBean {

    private String product_id;
    private String action;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
