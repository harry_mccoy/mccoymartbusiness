package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class CompanyListDataBean {
    @SerializedName("id")
    private int companyId;
    @SerializedName("status")
    private int companyStatus;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("company_slug")
    private String companySlug;
    @SerializedName("company_logo")
    private String companyLogo;
    @SerializedName("business_type")
    private String businessType;

    @SerializedName("company_type")
    private String companyType;

    @SerializedName("location")
    private String location;

    @SerializedName("year")
    private String year;
    @SerializedName("video_status")
    private int videoStatus;
    @SerializedName("browser_status")
    private int browserStatus;
    @SerializedName("cover_status")
    private int coverStatus;
    @SerializedName("reviews")
    private String reviewsCount;

    @SerializedName("views")
    private String viewsCount;
    @SerializedName("avg_rating")
    private String avgRating;
    @SerializedName("total_rating")
    private String totalRating;
    @SerializedName("profile_complate")
    private String profileComplate;
    private String status_code;
    private String status_color;
    private boolean selected;

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(int companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getVideoStatus() {
        return videoStatus;
    }

    public void setVideoStatus(int videoStatus) {
        this.videoStatus = videoStatus;
    }

    public int getBrowserStatus() {
        return browserStatus;
    }

    public void setBrowserStatus(int browserStatus) {
        this.browserStatus = browserStatus;
    }

    public int getCoverStatus() {
        return coverStatus;
    }

    public void setCoverStatus(int coverStatus) {
        this.coverStatus = coverStatus;
    }

    public String getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(String reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public String getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(String viewsCount) {
        this.viewsCount = viewsCount;
    }

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    public String getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }

    public String getProfileComplate() {
        return profileComplate;
    }

    public void setProfileComplate(String profileComplate) {
        this.profileComplate = profileComplate;
    }

    public String getCompanySlug() {
        return companySlug;
    }

    public void setCompanySlug(String companySlug) {
        this.companySlug = companySlug;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus_color() {
        return status_color;
    }

    public void setStatus_color(String status_color) {
        this.status_color = status_color;
    }
}
