package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class LeadStatusUpdateRequestBean {
    @SerializedName("lead_id")
    private String leadId;

    @SerializedName("marked")
    private String marked;

    @SerializedName("company_id")
    private String companyId;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getMarked() {
        return marked;
    }

    public void setMarked(String marked) {
        this.marked = marked;
    }


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
