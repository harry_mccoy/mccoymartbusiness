package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryProductDataResponse {
    @SerializedName("image")
    private String icon;

    @SerializedName("category_id")
    private String category_id;

    @SerializedName("category_name")
    private String category_name;

    @SerializedName("parent_id")
    private String parent_id;

    @SerializedName("sub_category")
    private List<CategoryProductSubCategoryDataResponse> sub_category;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public List<CategoryProductSubCategoryDataResponse> getSub_category() {
        return sub_category;
    }

    public void setSub_category(List<CategoryProductSubCategoryDataResponse> sub_category) {
        this.sub_category = sub_category;
    }
}
