package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadDetailsDataBean {

    @SerializedName("objClosed")
    private List<LeadStatusSubPopUpDataBean> leadStatusPopUpDataBeanList;

    @SerializedName("objStatus")
    private List<LeadStatusSubPopUpDataBean> leadStatusDataBeanList;

    @SerializedName("objBuyer")
    private LeadBuyerDataBean leadBuyerDataBean;

    @SerializedName("objEnquiry")
    private List<LeadStatusSubPopUpDataBean> enquiryDataBeanList;

    public List<LeadStatusSubPopUpDataBean> getLeadStatusPopUpDataBeanList() {
        return leadStatusPopUpDataBeanList;
    }

    public void setLeadStatusPopUpDataBeanList(List<LeadStatusSubPopUpDataBean> leadStatusPopUpDataBeanList) {
        this.leadStatusPopUpDataBeanList = leadStatusPopUpDataBeanList;
    }

    public List<LeadStatusSubPopUpDataBean> getLeadStatusDataBeanList() {
        return leadStatusDataBeanList;
    }

    public void setLeadStatusDataBeanList(List<LeadStatusSubPopUpDataBean> leadStatusDataBeanList) {
        this.leadStatusDataBeanList = leadStatusDataBeanList;
    }

    public LeadBuyerDataBean getLeadBuyerDataBean() {
        return leadBuyerDataBean;
    }

    public void setLeadBuyerDataBean(LeadBuyerDataBean leadBuyerDataBean) {
        this.leadBuyerDataBean = leadBuyerDataBean;
    }

    public List<LeadStatusSubPopUpDataBean> getEnquiryDataBeanList() {
        return enquiryDataBeanList;
    }

    public void setEnquiryDataBeanList(List<LeadStatusSubPopUpDataBean> enquiryDataBeanList) {
        this.enquiryDataBeanList = enquiryDataBeanList;
    }
}
