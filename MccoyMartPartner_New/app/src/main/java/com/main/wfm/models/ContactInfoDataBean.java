package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactInfoDataBean {
    @SerializedName("contact_person")
    private String personName;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("address_type")
    private String addressType;
    @SerializedName("address")
    private String address;
    @SerializedName("pin_code")
    private String pinCode;
    @SerializedName("city_id")
    private List<CityListDataBean> cityItem;


    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public List<CityListDataBean> getCityItem() {
        return cityItem;
    }

    public void setCityItem(List<CityListDataBean> cityItem) {
        this.cityItem = cityItem;
    }
}
