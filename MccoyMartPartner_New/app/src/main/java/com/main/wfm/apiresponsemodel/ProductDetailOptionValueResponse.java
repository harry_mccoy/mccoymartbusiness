package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class ProductDetailOptionValueResponse {

    @SerializedName("option_value_id")
    private String option_value_id;

    @SerializedName("price_prefix")
    private String price_prefix;

    @SerializedName("product_option_value_id")
    private String product_option_value_id;

    @SerializedName("image")
    private String image;

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("name")
    private String name;

    @SerializedName("subtract")
    private String subtract;

    @SerializedName("weight")
    private String weight;

    @SerializedName("weight_prefix")
    private String weight_prefix;

   @SerializedName("price")
    private String price;

    public String getOption_value_id() {
        return option_value_id;
    }

    public void setOption_value_id(String option_value_id) {
        this.option_value_id = option_value_id;
    }

    public String getPrice_prefix() {
        return price_prefix;
    }

    public void setPrice_prefix(String price_prefix) {
        this.price_prefix = price_prefix;
    }

    public String getProduct_option_value_id() {
        return product_option_value_id;
    }

    public void setProduct_option_value_id(String product_option_value_id) {
        this.product_option_value_id = product_option_value_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtract() {
        return subtract;
    }

    public void setSubtract(String subtract) {
        this.subtract = subtract;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight_prefix() {
        return weight_prefix;
    }

    public void setWeight_prefix(String weight_prefix) {
        this.weight_prefix = weight_prefix;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
