package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadsDataResponse {
    @SerializedName("objNew")
    private LeadsDataObjectResponse objNew;

    @SerializedName("objActive")
    private LeadsDataObjectResponse objActive;

    @SerializedName("objClosed")
    private LeadsDataObjectResponse objClosed;

    @SerializedName("ObjData")
    private List<LeadsDataListResponse> ObjData;

    public LeadsDataObjectResponse getObjNew() {
        return objNew;
    }

    public void setObjNew(LeadsDataObjectResponse objNew) {
        this.objNew = objNew;
    }

    public LeadsDataObjectResponse getObjActive() {
        return objActive;
    }

    public void setObjActive(LeadsDataObjectResponse objActive) {
        this.objActive = objActive;
    }

    public LeadsDataObjectResponse getObjClosed() {
        return objClosed;
    }

    public void setObjClosed(LeadsDataObjectResponse objClosed) {
        this.objClosed = objClosed;
    }


    public List<LeadsDataListResponse> getObjData() {
        return ObjData;
    }

    public void setObjData(List<LeadsDataListResponse> objData) {
        ObjData = objData;
    }
}
