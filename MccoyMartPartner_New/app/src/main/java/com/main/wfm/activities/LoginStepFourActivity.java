package com.main.wfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.models.SignUpRequestBean;
import com.main.wfm.models.SignUpResponse;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginStepFourActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvSignUp;
    private EditText etEmail;
    private AppSession appSession;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_stepfour);
        appSession=new AppSession(LoginStepFourActivity.this);
        getIds();
    }

    private void getIds() {
        tvSignUp=findViewById(R.id.tvSignUp);
        etEmail=findViewById(R.id.etEmail);
        tvSignUp.setOnClickListener(this);

        etEmail.addTextChangedListener(mTextEditorWatcher);

        ((ImageView)findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tvSignUp:
                if(CommonUtils.isValidEmail(etEmail.getText().toString().trim())){
                  /*  Intent intent = new Intent(LoginStepFourActivity.this, DashBoardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();*/
                    signUpAPI(getIntent().getStringExtra("username"),etEmail.getText().toString().trim(),
                            getIntent().getStringExtra("fullname"));

                }else{
                    CommonUtils.showAlertOk("Please enter valid email id.", LoginStepFourActivity.this);
                }

                break;
        }
    }

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //This sets a textview to the current length
            if(s.length()==0){
                ((TextInputLayout)findViewById(R.id.tvHint)).setHintEnabled(false);
                etEmail.setHint("Enter your Email Id");
            }else{
                ((TextInputLayout)findViewById(R.id.tvHint)).setHintEnabled(true);
            }
        }

        public void afterTextChanged(Editable s) {
        }
    };

    private void signUpAPI(String mobile,String email,String name) {

        //final ProgressDialog dialog = ProgressDialog.show(SelectOptionActivity.this, null, getString(R.string.loading));
        final ViewDialog viewDialog = new ViewDialog(this);
        viewDialog.showDialog();
        SignUpRequestBean request = new SignUpRequestBean();


        request.setDeviceType("2");
        request.setMobile(mobile);
        request.setName(name);
        request.setEmail(email);
        request.setDeviceId(appSession.getDeviceId());
        request.setPassword("");
        request.setFcmId("");
        request.setDeviceType("2"); ////device_type for web=1,Android=2,IOS=3
        Call<SignUpResponse> call = ApiExecutor.getApiService(LoginStepFourActivity.this).SignUpApi(request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<SignUpResponse>() {
                         @Override
                         public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Send Device Token " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     appSession.setAuthToken(response.body().getSignUpDataBean()
                                             .getToken());
                                     appSession.setUserId(response.body().getSignUpDataBean().getUserId());
                                     appSession.setUserData(response.body().getSignUpDataBean().getName(),
                                             "","","",response.body().getSignUpDataBean().getPhone());
                                     Intent intent = new Intent(LoginStepFourActivity.this, DashBoardActivity.class);
                                     intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                     startActivity(intent);
                                     finish();
                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(),LoginStepFourActivity.this);
                                     break;
                                 case 401:
                                     error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(),LoginStepFourActivity.this);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<SignUpResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }
}
