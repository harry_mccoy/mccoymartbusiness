package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class DashboardCPItemDataBean {
    @SerializedName("title")
    private String title;
    @SerializedName("action")
    private String action;
    @SerializedName("status")
    private String status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
