package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordRequestBean {
    @SerializedName("password")
    private String password;
    @SerializedName("new_password")
    private String new_password;
    @SerializedName("confirm_password")
    private String confirm_password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }
}
