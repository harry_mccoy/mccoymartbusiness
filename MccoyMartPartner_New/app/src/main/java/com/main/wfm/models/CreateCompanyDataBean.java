package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class CreateCompanyDataBean {
    @SerializedName("company_id")
    private int companyId;

    @SerializedName("company_name")
    private String companyName;

    @SerializedName("company_slug")
    private String companySlug;

    @SerializedName("business_type")
    private String businessType;


    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanySlug() {
        return companySlug;
    }

    public void setCompanySlug(String companySlug) {
        this.companySlug = companySlug;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }
}
