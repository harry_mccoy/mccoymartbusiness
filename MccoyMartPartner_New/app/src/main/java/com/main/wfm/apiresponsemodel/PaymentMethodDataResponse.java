package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMethodDataResponse {
    @SerializedName("offers")
    private List<String> offers;

    @SerializedName("payment_methods")
    private List<PaymentMethodItemsResponse> payment_methods;

    public List<String> getOffers() {
        return offers;
    }

    public void setOffers(List<String> offers) {
        this.offers = offers;
    }

    public List<PaymentMethodItemsResponse> getPayment_methods() {
        return payment_methods;
    }

    public void setPayment_methods(List<PaymentMethodItemsResponse> payment_methods) {
        this.payment_methods = payment_methods;
    }
}
