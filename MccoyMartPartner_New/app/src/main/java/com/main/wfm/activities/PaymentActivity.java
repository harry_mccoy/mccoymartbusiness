package com.main.wfm.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.CartListAdapter;
import com.main.wfm.adapters.PaymentMethodsAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apirequestmodel.AddToCartRequest;
import com.main.wfm.apiresponsemodel.CartListDataResponse;
import com.main.wfm.apiresponsemodel.CartListResponse;
import com.main.wfm.apiresponsemodel.CartTotalResponse;
import com.main.wfm.apiresponsemodel.PaymentMethodItemsResponse;
import com.main.wfm.apiresponsemodel.PaymentMethodResponse;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppConstants;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.List;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private AppSession appSession;
    private List<PaymentMethodItemsResponse> paymentMethodList;
    private List<CartListDataResponse> cartTotalList;
    private List<String> offers;
    private PaymentMethodsAdapter adapter;
    private ListView lvPaymentList;
    private TextView tvCartContinue;
    private FrameLayout flCart;
    private LinearLayout llTotal;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_methods);
        appSession=new AppSession(PaymentActivity.this);
        getIds();
    }

    private void getIds() {

        lvPaymentList=findViewById(R.id.lvPaymentList);
        tvCartContinue=findViewById(R.id.tvCartContinue);
        flCart=findViewById(R.id.flCart);

        flCart.setOnClickListener(this);
        tvCartContinue.setOnClickListener(this);

        paymentMethodList=new ArrayList<PaymentMethodItemsResponse>();
        adapter=new PaymentMethodsAdapter(PaymentActivity.this,paymentMethodList);
        lvPaymentList.setAdapter(adapter);

        //Adding Header To List Vie;w
        LayoutInflater inflaterHeader = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflaterHeader.inflate(R.layout.payment_method_header, lvPaymentList, false);
        lvPaymentList.addHeaderView(header);

        //Adding Footer To List Vie;w
        LayoutInflater inflaterFooter = getLayoutInflater();
        ViewGroup footer = (ViewGroup) inflaterFooter.inflate(R.layout.cart_footer, lvPaymentList, false);
        lvPaymentList.addFooterView(footer);
        llTotal=footer.findViewById(R.id.llTotal);

        getPaymentMethodApi();
        getCartTotalApi();
        if (appSession.getCartCount() > 0) {
            ((TextView) findViewById(R.id.tvCartCount)).setText("" + appSession.getCartCount());
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.flCart:
                Intent intent = new Intent(PaymentActivity.this, CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.tvCartContinue:
                CartActivity.shippingAddress=null;
                CartActivity.billingAddress=null;
                intent = new Intent(PaymentActivity.this, DashBoardActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void getPaymentMethodApi() {
        final ViewDialog viewDialog = new ViewDialog(PaymentActivity.this);
        viewDialog.showDialog();

        AddToCartRequest request=new AddToCartRequest();
        request.setSource("2");
        request.setDevice_id(appSession.getDeviceId());
        NetworkAPI.paymentMethodsApi(PaymentActivity.this, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<PaymentMethodResponse>() {
                    @Override
                    public void apiSuccess(PaymentMethodResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.getPaymentMethodData()!=null){

                                offers=body.getPaymentMethodData().getOffers();
                                paymentMethodList=body.getPaymentMethodData().getPayment_methods();
                                adapter=new PaymentMethodsAdapter(PaymentActivity.this,paymentMethodList);
                                lvPaymentList.setAdapter(adapter);
                                setOffer();

                            } else {

                            }

                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(PaymentActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    private void setOffer() {
        if(offers!=null&&offers.size()>0){
            if(offers.get(0)!=null){
                ((TextView)findViewById(R.id.tvOffer1)).setText(offers.get(0));
            }
            if (offers.size()>1){
                if(offers.get(1)!=null){
                    ((TextView)findViewById(R.id.tvOffer2)).setText(offers.get(1));
                }
            }else{
                ((TextView)findViewById(R.id.tvOffer2)).setVisibility(View.GONE);
            }
        }
        if(offers!=null&&offers.size()>2){
            ((TextView)findViewById(R.id.tvAllOffer)).setVisibility(View.VISIBLE);
        }else{
            ((TextView)findViewById(R.id.tvAllOffer)).setVisibility(View.GONE);
        }
    }

    public void getCartTotalApi() {
        final ViewDialog viewDialog = new ViewDialog(PaymentActivity.this);
        viewDialog.showDialog();

        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setSource("2");
        request.setDevice_id(appSession.getDeviceId());
        NetworkAPI.getCartTotalApi(PaymentActivity.this, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<CartTotalResponse>() {
                    @Override
                    public void apiSuccess(CartTotalResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();

                                cartTotalList = body.getCartData().getCart_total();
                                setCartTotalList();

                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(PaymentActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    private void setCartTotalList() {

        for(int i=0;i<cartTotalList.size();i++){
            FrameLayout llPersonal = new FrameLayout(this);
            //llPersonal.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams paramsPersonalValue = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            paramsPersonalValue.setMargins(10 , 10, 10, 10);
            llPersonal.setLayoutParams(paramsPersonalValue);

            TextView tvName = new TextView(this);
            tvName.setText(cartTotalList.get(i).getTitle());
            tvName.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvName.setTextColor(getResources().getColor(R.color.text_color));
            tvName.setGravity(Gravity.LEFT|Gravity.CENTER);
            tvName.setPadding(20, 20, 20, 20);
            tvName.setTypeface(CommonUtils.setCustomFont(PaymentActivity.this, AppConstants.REGULAR_FONT));
            tvName.setTextSize(12);
            tvName.setId(i);


            TextView tvValue = new TextView(this);
            tvValue.setText(cartTotalList.get(i).getValue());
            tvValue.setTextColor(getResources().getColor(R.color.text_color));
            //tvValue.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvValue.setGravity(Gravity.RIGHT|Gravity.CENTER);
            tvValue.setTypeface(CommonUtils.setCustomFont(PaymentActivity.this, AppConstants.REGULAR_FONT));
            tvValue.setPadding(20, 20, 20, 20);
            tvValue.setTextSize(12);
            tvValue.setId(i);


            if(cartTotalList.get(i).getCode().equalsIgnoreCase("total")){
                View view=new View(this);
                view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2));
                view.setBackgroundColor(getResources().getColor(R.color.grey));
                tvValue.setTextSize(14);
                tvName.setTextSize(14);
                tvValue.setTypeface(CommonUtils.setCustomFont(PaymentActivity.this, AppConstants.BOLD_FONT));
                tvName.setTypeface(CommonUtils.setCustomFont(PaymentActivity.this, AppConstants.BOLD_FONT));
                llPersonal.addView(view);
            }

            llPersonal.addView(tvName);
            llPersonal.addView(tvValue);
            llTotal.addView(llPersonal);
        }
    }


    public void addOrderApi() {
        final ViewDialog viewDialog = new ViewDialog( PaymentActivity.this);
        viewDialog.showDialog();
        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setSource("2");
        request.setDevice_id(appSession.getDeviceId());

        NetworkAPI.addPaymentMethodApi(PaymentActivity.this, "Bearer " + appSession.getAuthToken(),request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                //CommonUtils.showToast(PaymentActivity.this,body.message);
                                Intent intent = new Intent(PaymentActivity.this, MyOrdersSuccessActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(PaymentActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
}
