package com.main.wfm.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.main.wfm.R;
import com.main.wfm.activities.LoginStepOneActivity;
import com.main.wfm.activities.LoginStepTwoActivity;
import com.main.wfm.activities.ProductsCommonActivity;
import com.main.wfm.apiresponsemodel.CategoryProductDataResponse;
import com.main.wfm.apiresponsemodel.CategoryProductResponse;
import com.main.wfm.apiresponsemodel.CategoryProductSubCategoryDataResponse;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

public class CategoryProductExpendableAdapter extends BaseExpandableListAdapter {

    private Context context;
    //private List<CategoryProductDataResponse> expandableListTitle;
    private List<CategoryProductDataResponse> expandableListTitle;
    private HashMap<CategoryProductDataResponse, List<CategoryProductSubCategoryDataResponse>> expandableListDetail;

    public CategoryProductExpendableAdapter(Context context, List<CategoryProductDataResponse> expandableListTitle,
                                            HashMap<CategoryProductDataResponse, List<CategoryProductSubCategoryDataResponse>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public CategoryProductSubCategoryDataResponse getChild(int listPosition, int expandedListPosition) {
        return (CategoryProductSubCategoryDataResponse)this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
        //return (CategoryProductSubCategoryDataResponse) this.expandableListDetail.get(this.expandableListTitle.get(listPosition));
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        //getChild(listPosition, expandedListPosition).getName();


        final CategoryProductSubCategoryDataResponse obCategoryProductDataResponse = getChild(listPosition, expandedListPosition);

        System.out.println("ADAPTER: " + getChild(listPosition, expandedListPosition));
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_category_child, null);
        }
        ImageView ivProductPic1 = convertView.findViewById(R.id.ivProductPic1);
        TextView tvChildName1 = (TextView) convertView.findViewById(R.id.tvChildName1);
        ImageView ivProductPic2 = convertView.findViewById(R.id.ivProductPic2);
        TextView tvChildName2 = (TextView) convertView.findViewById(R.id.tvChildName2);
        LinearLayout llCategoryChildParent1=convertView.findViewById(R.id.llCategoryChildParent1);
        LinearLayout llCategoryChildParent2=convertView.findViewById(R.id.llCategoryChildParent2);
        System.out.println("LIST POSITION: "+expandedListPosition);

            tvChildName1.setText(obCategoryProductDataResponse.getCategory_name());
            Picasso.with(context).load(obCategoryProductDataResponse.getIcon())
                    .placeholder(context.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivProductPic1);

            tvChildName2.setText(obCategoryProductDataResponse.getCategory_name2());
            Picasso.with(context).load(obCategoryProductDataResponse.getIcon2())
                    .placeholder(context.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivProductPic2);


        llCategoryChildParent1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductsCommonActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("category_id", obCategoryProductDataResponse.getCategory_id());
                intent.putExtra("category_name", obCategoryProductDataResponse.getCategory_name());
                context.startActivity(intent);
            }
        });

        llCategoryChildParent2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductsCommonActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("category_id", obCategoryProductDataResponse.getCategory_id2());
                intent.putExtra("category_name", obCategoryProductDataResponse.getCategory_name2());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {

        //return this.expandableListTitle.get(listPosition).getSub_category().size();
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final CategoryProductDataResponse obCategoryProductDataResponse = (CategoryProductDataResponse) getGroup(listPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_category_parent, null);
        }


        final LinearLayout llParent = convertView.findViewById(R.id.llParent);
        ImageView imgBtnDownArrow = convertView.findViewById(R.id.imgBtnDownArrow);
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.tvParentName);
        ImageView ivProductPic = convertView.findViewById(R.id.ivProductPic);
        listTitleTextView.setText(obCategoryProductDataResponse.getCategory_name());
        Picasso.with(context).load(obCategoryProductDataResponse.getIcon())
                .placeholder(context.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivProductPic);
        if(obCategoryProductDataResponse.getSub_category().size()==0){
            imgBtnDownArrow.setVisibility(View.GONE);
        }else{
            imgBtnDownArrow.setVisibility(View.VISIBLE);
        }

        if(obCategoryProductDataResponse.getSub_category().size()==0){
            llParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("CATEGORY SUB CATEGORY: "+obCategoryProductDataResponse.getSub_category().size());

                        Intent intent = new Intent(context, ProductsCommonActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("category_id", obCategoryProductDataResponse.getCategory_id());
                        intent.putExtra("category_name", obCategoryProductDataResponse.getCategory_name());
                        context.startActivity(intent);


                }
            });
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
