package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateBusinessInfoResponseBean {
    @SerializedName("company_ceo")
    private String companyCEOName;
    @SerializedName("company_id")
    private String companyId;
    @SerializedName("website")
    private String website;
    @SerializedName("year_establishment")
    private String yearOfEstablishment;
    @SerializedName("no_of_employee")
    private String noOfEmployee;
    @SerializedName("turnover")
    private String turnover;
    @SerializedName("company_logo")
    private String companyLogo;
    @SerializedName("cover_photo")
    private String cover_photo;
    @SerializedName("company_profile")
    private String companyProfile;
    @SerializedName("cities_serve")
    private List<CityListDataBean> citiesServeList;
    private String gst_no;
    private String pan_no;

    @SerializedName("company_name")
    private String companyName;
    @SerializedName("company_type")
    private List<BusinessRoleDataBean> businessRoleDataBeanList;

    @SerializedName("legal_status")
    private List<BusinessRoleDataBean> businessLegalDataBeanList;

    public String getCompanyCEOName() {
        return companyCEOName;
    }

    public void setCompanyCEOName(String companyCEOName) {
        this.companyCEOName = companyCEOName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getYearOfEstablishment() {
        return yearOfEstablishment;
    }

    public void setYearOfEstablishment(String yearOfEstablishment) {
        this.yearOfEstablishment = yearOfEstablishment;
    }

    public String getNoOfEmployee() {
        return noOfEmployee;
    }

    public void setNoOfEmployee(String noOfEmployee) {
        this.noOfEmployee = noOfEmployee;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    public String getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(String companyProfile) {
        this.companyProfile = companyProfile;
    }

    public List<CityListDataBean> getCitiesServeList() {
        return citiesServeList;
    }

    public void setCitiesServeList(List<CityListDataBean> citiesServeList) {
        this.citiesServeList = citiesServeList;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getGst_no() {
        return gst_no;
    }

    public void setGst_no(String gst_no) {
        this.gst_no = gst_no;
    }

    public String getPan_no() {
        return pan_no;
    }

    public void setPan_no(String pan_no) {
        this.pan_no = pan_no;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public List<BusinessRoleDataBean> getBusinessRoleDataBeanList() {
        return businessRoleDataBeanList;
    }

    public void setBusinessRoleDataBeanList(List<BusinessRoleDataBean> businessRoleDataBeanList) {
        this.businessRoleDataBeanList = businessRoleDataBeanList;
    }

    public List<BusinessRoleDataBean> getBusinessLegalDataBeanList() {
        return businessLegalDataBeanList;
    }

    public void setBusinessLegalDataBeanList(List<BusinessRoleDataBean> businessLegalDataBeanList) {
        this.businessLegalDataBeanList = businessLegalDataBeanList;
    }

    public String getCover_photo() {
        return cover_photo;
    }

    public void setCover_photo(String cover_photo) {
        this.cover_photo = cover_photo;
    }
}
