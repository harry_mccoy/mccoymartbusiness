package com.main.wfm.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.CategoryProductExpendableAdapter;
import com.main.wfm.adapters.ProductCommonGridAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apiresponsemodel.CategoryProductDataResponse;
import com.main.wfm.apiresponsemodel.CategoryProductResponse;
import com.main.wfm.apiresponsemodel.CategoryProductSubCategoryDataResponse;
import com.main.wfm.apiresponsemodel.ProductCommonResponse;
import com.main.wfm.models.GetProductOfferRequestBean;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryProductsActivity extends AppCompatActivity {

    private ExpandableListView lvExpCategory;
    final int[] prevExpandPosition = {-1};

    private AppSession appSession;
    private CategoryProductExpendableAdapter adapter;
    //private List<CategoryProductDataResponse> expandableListTitle;
    private List<CategoryProductDataResponse> expandableListTitle;
    private List<CategoryProductSubCategoryDataResponse> arlCategoryProductDataResponses;
    private HashMap<CategoryProductDataResponse, List<CategoryProductSubCategoryDataResponse>> expandableListDetail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_product);
        lvExpCategory = findViewById(R.id.lvExpCategory);
        appSession = new AppSession(CategoryProductsActivity.this);
        ((ImageView) findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((TextView) findViewById(R.id.tvTitle)).setText(getIntent().getStringExtra("category_name"));
        getCategoryProductsAPI(getIntent().getStringExtra("category_id"));

        lvExpCategory.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                //Toast.makeText(mContext, expandableListTitle.get(groupPosition) + " List Expanded.",Toast.LENGTH_SHORT).show();
                if (prevExpandPosition[0] >= 0 && prevExpandPosition[0] != groupPosition) {
                    lvExpCategory.collapseGroup(prevExpandPosition[0]);
                }
                prevExpandPosition[0] = groupPosition;
            }
        });

        lvExpCategory.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                //Toast.makeText(mContext,expandableListTitle.get(groupPosition) + " List Collapsed.",Toast.LENGTH_SHORT).show();

            }
        });

        lvExpCategory.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {


                return false;
            }
        });
    }

    private void getCategoryProductsAPI(final String categoryId) {
        final ViewDialog viewDialog = new ViewDialog(CategoryProductsActivity.this);
        viewDialog.showDialog();

        NetworkAPI.getCategoryProductsApi(CategoryProductsActivity.this, "Bearer " + appSession.getAuthToken(), categoryId,
                new Dispatch<CategoryProductResponse>() {

                    @Override
                    public void apiSuccess(CategoryProductResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));
                        viewDialog.hideDialog();
                        if (body != null) {
                            if (body.getData() != null && body.getData().size() > 0) {
                                viewDialog.hideDialog();

                                setCategoryListData(body.getData());

                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(CategoryProductsActivity.this, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();

                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    private void setCategoryListData(List<CategoryProductDataResponse> expendablelListData) {

        expandableListTitle = new ArrayList<CategoryProductDataResponse>();
        expandableListDetail = new HashMap<CategoryProductDataResponse, List<CategoryProductSubCategoryDataResponse>>();
        for (int i = 0; i < expendablelListData.size(); i++) {
            CategoryProductDataResponse obCategoryProductDataResponse = new CategoryProductDataResponse();
            obCategoryProductDataResponse.setCategory_id(expendablelListData.get(i).getCategory_id());
            obCategoryProductDataResponse.setCategory_name(expendablelListData.get(i).getCategory_name());
            obCategoryProductDataResponse.setIcon(expendablelListData.get(i).getIcon());
            obCategoryProductDataResponse.setSub_category(expendablelListData.get(i).getSub_category());
            expandableListTitle.add(obCategoryProductDataResponse);
        }
        for (int i = 0; i < expandableListTitle.size(); i++) {
            arlCategoryProductDataResponses = new ArrayList<CategoryProductSubCategoryDataResponse>();

            if (expandableListTitle.get(i).getSub_category() != null && expandableListTitle.get(i).getSub_category().size() > 0) {

                for (int j = 0; j < expandableListTitle.get(i).getSub_category().size(); j++) {
                    CategoryProductSubCategoryDataResponse obCategoryProductDataResponse = new CategoryProductSubCategoryDataResponse();
                    if (expandableListTitle.get(i).getCategory_id().equalsIgnoreCase(expandableListTitle.get(i).getSub_category().get(j).getParent_id())) {


                        obCategoryProductDataResponse.setCategory_id(expandableListTitle.get(i).getSub_category().get(j).getCategory_id());
                        obCategoryProductDataResponse.setCategory_name(expandableListTitle.get(i).getSub_category().get(j).getCategory_name());
                        obCategoryProductDataResponse.setIcon(expandableListTitle.get(i).getSub_category().get(j).getIcon());
                        obCategoryProductDataResponse.setParent_id(expandableListTitle.get(i).getSub_category().get(j).getParent_id());


                    }

                        j++;

                    if(j<expandableListTitle.get(i).getSub_category().size()){
                        if(expandableListTitle.get(i).getCategory_id().equalsIgnoreCase(expandableListTitle.get(i).getSub_category().get(j).getParent_id())) {

                            obCategoryProductDataResponse.setCategory_id2(expandableListTitle.get(i).getSub_category().get(j).getCategory_id());
                            obCategoryProductDataResponse.setCategory_name2(expandableListTitle.get(i).getSub_category().get(j).getCategory_name());
                            obCategoryProductDataResponse.setIcon2(expandableListTitle.get(i).getSub_category().get(j).getIcon());
                            obCategoryProductDataResponse.setParent_id2(expandableListTitle.get(i).getSub_category().get(j).getParent_id());
                            arlCategoryProductDataResponses.add(obCategoryProductDataResponse);
                        }
                    }

                }


            }
            expandableListDetail.put(expandableListTitle.get(i), arlCategoryProductDataResponses);

        }


        adapter = new CategoryProductExpendableAdapter(CategoryProductsActivity.this, this.expandableListTitle, expandableListDetail);
        lvExpCategory.setAdapter(adapter);


    }


}
