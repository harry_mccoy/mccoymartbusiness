package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartListResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private int statusCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }



    @SerializedName("data")
    private CartDataResponse cartData;

    public CartDataResponse getCartData() {
        return cartData;
    }

    public void setCartData(CartDataResponse cartData) {
        this.cartData = cartData;
    }
}
