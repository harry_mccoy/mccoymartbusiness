package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class LeadsTypeItemDataBean {
    @SerializedName("id")
    private int id;

    @SerializedName("company_id")
    private int companyId;

    @SerializedName("name")
    private String name;
    @SerializedName("mobile")
    private String mobile;

    @SerializedName("email")
    private String email;
    @SerializedName("Category")
    private String category;

    @SerializedName("date")
    private String date;
    @SerializedName("glv")
    private String glv;

    @SerializedName("description")
    private String description;
    @SerializedName("marked")
    private String marked;

    @SerializedName("location")
    private String location;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGlv() {
        return glv;
    }

    public void setGlv(String glv) {
        this.glv = glv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMarked() {
        return marked;
    }

    public void setMarked(String marked) {
        this.marked = marked;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }
}
