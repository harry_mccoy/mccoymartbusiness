package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrderDataResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private String statusCode;

    @SerializedName("data")
    private List<MyOrderDataItemResponse> myOrderDataList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<MyOrderDataItemResponse> getMyOrderDataList() {
        return myOrderDataList;
    }

    public void setMyOrderDataList(List<MyOrderDataItemResponse> myOrderDataList) {
        this.myOrderDataList = myOrderDataList;
    }
}
