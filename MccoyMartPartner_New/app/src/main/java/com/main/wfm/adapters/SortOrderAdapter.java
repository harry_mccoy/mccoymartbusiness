package com.main.wfm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.main.wfm.R;
import com.main.wfm.activities.LeadsActivity;
import com.main.wfm.activities.ProductsCommonActivity;
import com.main.wfm.apiresponsemodel.LeadsFilterDataResponse;
import com.main.wfm.utils.LoadLeadsEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class SortOrderAdapter extends BaseAdapter {

    private static final String TAG = SortOrderAdapter.class.getSimpleName();
    private Context context;
    private List<LeadsFilterDataResponse> leadsFilterDataResponses;
    private LayoutInflater infalter;
    private String category_id;
    private int totalCount;
    private String moduleType;

    int selectedPosition = 0;
    public SortOrderAdapter(Context context, List<LeadsFilterDataResponse> leadsFilterDataResponses,String category_id,int totalCount,String moduleType) {
        this.context = context;
        this.leadsFilterDataResponses = leadsFilterDataResponses;
        this.category_id=category_id;
        this.totalCount=totalCount;
        this.moduleType=moduleType;
        infalter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return leadsFilterDataResponses.size();
    }

    @Override
    public Object getItem(int position) {

        return leadsFilterDataResponses.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = infalter.inflate(R.layout.row_leads_filter, null);
            holder = new ViewHolder();
            holder.radioLatest = (RadioButton) convertView.findViewById(R.id.radioLatest);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            holder.radioLatest.setText(leadsFilterDataResponses.get(position).getText());
        holder.radioLatest.setChecked(position == selectedPosition);
        holder.radioLatest.setTag(position);

        holder.radioLatest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = (Integer)holder.radioLatest.getTag();
                notifyDataSetChanged();
                ((ProductsCommonActivity)context).disableFilterDialog();
                String []sortKeys=leadsFilterDataResponses.get(position).getValue().split("-");
                if(moduleType.equalsIgnoreCase("b2btodaydeal")){
                    ((ProductsCommonActivity)context). getProductsAPI(moduleType);
                }else if(moduleType.equalsIgnoreCase("latestproduct")){
                    ((ProductsCommonActivity)context).getLatestProductsAPI();
                }else if(moduleType.equalsIgnoreCase("topBrands")){
                    ((ProductsCommonActivity)context).getTopBrandsProductsAPI();;
                }else {
                    ((ProductsCommonActivity)context).getProductsByCategoryAPI(category_id,sortKeys[0],sortKeys[1],totalCount,"0");
                }


                //LoadLeadsEvent loadLeadsEvent = new LoadLeadsEvent(leadType,leadsFilterDataResponses.get(position).getValue(), search);
                //EventBus.getDefault().post(loadLeadsEvent);
            }
        });
        return convertView;
    }

    public class ViewHolder {
        RadioButton radioLatest;
    }
}
