package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class MyOrderDataItemResponse {
    @SerializedName("order_id")
    private String order_id;

    @SerializedName("order_status")
    private String order_status;

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
