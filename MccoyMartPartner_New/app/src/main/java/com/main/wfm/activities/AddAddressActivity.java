package com.main.wfm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.CartListAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apirequestmodel.AddEditAddressRequest;
import com.main.wfm.apiresponsemodel.CartListResponse;
import com.main.wfm.models.SignUpRequestBean;
import com.main.wfm.models.SignUpResponse;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvAddAddress;
    private EditText etFirstName, etLastName, etBusinessName, etEmail, etPhone, etAddress, etCountry, etState, etCity, etLandMark, etGst, etPinCode;
    private AppSession appSession;
    private String addressId = "", zoneId = "", countryId = "", customerId = "",action="add";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        appSession = new AppSession(AddAddressActivity.this);
        getIds();
    }

    private void getIds() {
        tvAddAddress = findViewById(R.id.tvAddAddress);
        etEmail = findViewById(R.id.etEmail);
        etFirstName = findViewById(R.id.etFirstName);
        etBusinessName = findViewById(R.id.etBusinessName);
        etCity = findViewById(R.id.etCity);
        etGst = findViewById(R.id.etGst);
        etLandMark = findViewById(R.id.etLandMark);
        etAddress = findViewById(R.id.etAddress);
        etLastName = findViewById(R.id.etLastName);
        etState = findViewById(R.id.etState);
        etPhone = findViewById(R.id.etPhone);
        etCountry = findViewById(R.id.etCountry);
        etPinCode = findViewById(R.id.etPinCode);

        etCountry.setEnabled(false);
        etState.setEnabled(false);

        tvAddAddress.setOnClickListener(this);

        if(getIntent().getStringExtra("addressId")!=null){
            addressId=getIntent().getStringExtra("addressId");
        }

        if(getIntent().getStringExtra("action")!=null){
            action=getIntent().getStringExtra("action");
        }

        if(getIntent().getStringExtra("customerId")!=null){
            customerId=getIntent().getStringExtra("customerId");
        }

        ((ImageView) findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(getIntent().getStringExtra("addressType")!=null){
            ((TextView)findViewById(R.id.tvTitle)).setText(getIntent().getStringExtra("addressType"));
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvAddAddress:

                if(isValidate()){
                    addEditAddressApi(action,addressId, zoneId, countryId, customerId);
                }


                break;
        }
    }




    public void addEditAddressApi(String action, String addressId, String zoneId, String countryId, String customerId) {
        final ViewDialog viewDialog = new ViewDialog(AddAddressActivity.this);
        viewDialog.showDialog();

        AddEditAddressRequest request = new AddEditAddressRequest();
        request.setAction(action);
        request.setFirst_name(etFirstName.getText().toString().trim());
        request.setLast_name(etLastName.getText().toString().trim());
        request.setAddress1(etAddress.getText().toString().trim());
        request.setAddress2(etLandMark.getText().toString().trim());
        request.setGstin(etGst.getText().toString().trim());
        request.setCity(etCity.getText().toString().trim());
        request.setCompany(etBusinessName.getText().toString().trim());
        request.setAddress_id(addressId);
        request.setZone_id(zoneId);
        request.setCountry_id(countryId);
        request.setCustomer_id(customerId);
        request.setPostcode(etPinCode.getText().toString().trim());
        NetworkAPI.addEditAddressApi(AddAddressActivity.this, "Bearer " + appSession.getAuthToken(), request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Add Address Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.statusCode.equalsIgnoreCase("200")) {
                                CommonUtils.showToast(AddAddressActivity.this, body.message);
                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(AddAddressActivity.this, getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }

    private boolean isValidate() {
        if (etFirstName.getText().length() == 0) {
            CommonUtils.showAlertOk("Please enter your first name.", AddAddressActivity.this);
            etFirstName.requestFocus();
            return false;
        } else if (etLastName.getText().length() == 0) {
            CommonUtils.showAlertOk("Please enter your last name.", AddAddressActivity.this);
            etLastName.requestFocus();
            return false;
        } else if (!(CommonUtils.isValidEmail(etEmail.getText().toString().trim()))) {
            CommonUtils.showAlertOk("Please enter your email.", AddAddressActivity.this);
            etEmail.requestFocus();
            return false;
        } else if (etBusinessName.getText().length() == 0) {
            CommonUtils.showAlertOk("Please enter your business name.", AddAddressActivity.this);
            etBusinessName.requestFocus();
            return false;
        } else if (etPhone.getText().length() == 0) {
            CommonUtils.showAlertOk("Please enter your phone number.", AddAddressActivity.this);
            etPhone.requestFocus();
            return false;
        } else if (etAddress.getText().length() == 0) {
            CommonUtils.showAlertOk("Please enter your address.", AddAddressActivity.this);
            etAddress.requestFocus();
            return false;
        } else if (etPinCode.getText().length() == 0) {
            CommonUtils.showAlertOk("Please enter zip code.", AddAddressActivity.this);
            etPinCode.requestFocus();
            return false;
        } else if (etCity.getText().length() == 0) {
            CommonUtils.showAlertOk("Please enter city.", AddAddressActivity.this);
            etCity.requestFocus();
            return false;
        }
        return true;
    }
}
