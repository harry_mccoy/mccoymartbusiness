package com.main.wfm.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.LeadsFilterAdapter;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.LeadsRequestModel;
import com.main.wfm.apiresponsemodel.LeadsDataResponse;
import com.main.wfm.apiresponsemodel.LeadsFilterDataResponse;
import com.main.wfm.apiresponsemodel.LeadsFilterResponse;
import com.main.wfm.apiresponsemodel.LeadsResponse;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.LoadLeadsEvent;
import com.main.wfm.utils.LoadLeadsInterface;
import com.main.wfm.utils.ViewDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadsHomeFragment extends Fragment  {
private Context mContext;

private int position;

    private LinearLayout llEmptyLeadsFrame, llLeadsFrame;
    private BottomSheetDialog filterDialog;
    private ListView lvLeadsFilter;
    private EditText etSearch;
    private List<LeadsFilterDataResponse> leadsFilterDataResponses;
    private LeadsFilterAdapter adapter;
    private AppSession appSession;
    private LoadLeadsInterface obLoadLeadsInterface;
    private ViewPager vpPager;
    private TabLayout tabLayout;
    private String search = "", filter = "", leadType = "new";
    private String[] arTypes = {"new", "active", "closed"};
    private LeadsDataResponse ObjData;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_leads, container, false);
        getIds(root);
        return root;
    }


    public LeadsHomeFragment() {


    }

    public static LeadsHomeFragment newInstance(int position) {
        LeadsHomeFragment f = new LeadsHomeFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext=context;

    }

    private void getIds(View root) {
        appSession = new AppSession(mContext);
        tabLayout = (TabLayout) root.findViewById(R.id.tabLayout);
        etSearch = root.findViewById(R.id.etSearch);
        llEmptyLeadsFrame = root.findViewById(R.id.llEmptyLeadsFrame);
        llLeadsFrame = root.findViewById(R.id.llLeadsFrame);
        vpPager = (ViewPager) root.findViewById(R.id.vpPager);

        /*((ImageView) root.findViewById(R.id.ivBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        ((ImageView) root.findViewById(R.id.ivFilter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilterDialog();
            }
        });


        /*((TextView) findViewById(R.id.tvContinueShopping)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

        //obLoadLeadsInterface=(LoadLeadsInterface)this;
        //EventBus.getDefault().register(mContext);
        createViewPager(vpPager);
        tabLayout.setupWithViewPager(vpPager);

        getLeadsDataAPI(leadType, filter, search);
        //createTabIcons();

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {

                    search = etSearch.getText().toString().trim();
                    //getLeadsDataAPI(leadType,filter,search);
                    LoadLeadsEvent loadLeadsEvent = new LoadLeadsEvent(leadType, filter, search);
                    EventBus.getDefault().post(loadLeadsEvent);
                }
                return false;
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 0) {
                    search = "";
                    //getLeadsDataAPI(leadType,filter,search);
                    LoadLeadsEvent loadLeadsEvent = new LoadLeadsEvent(leadType, filter, search);
                    EventBus.getDefault().post(loadLeadsEvent);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void createTabIcons(String tabOneText, String tabTwoText, String tabThreeText) {

        TextView tabOne = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabOne.setText(tabOneText);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabTwo.setText(tabTwoText);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabThree.setText(tabThreeText);
        tabLayout.getTabAt(2).setCustomView(tabThree);

    }

    private void createViewPager(final ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new LeadsNewFragment(), "New");
        // adapter.addFrag(new DemoFragment(), "Reviews");
        adapter.addFrag(new LeadsActiveFragment(), "Active");
        //adapter.addFrag(new DemoFragment(), "Reports");
        adapter.addFrag(new LeadsClosedFragment(), "Closed");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                //obLoadLeadsInterface.loadLeadData(arTypes[position],filter,search);
               /* leadType = arTypes[position];
                etSearch.setText("");
                LoadLeadsEvent loadLeadsEvent = new LoadLeadsEvent(arTypes[position], filter, search);
                EventBus.getDefault().post(loadLeadsEvent);*/


                appSession.setLeadsPagerPosition(position);
            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);
                System.out.println("POSITION SWIPE: "+position);
                leadType = arTypes[position];
                etSearch.setText("");
                LoadLeadsEvent loadLeadsEvent = new LoadLeadsEvent(arTypes[position], filter, search);
                EventBus.getDefault().post(loadLeadsEvent);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Subscribe
    public void getSearch(LoadLeadsEvent events) {

    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    /*class ViewPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 3;

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override

        public int getCount() {
            return NUM_ITEMS;
        }
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            //return LeadsNewFragment.newInstance(position);
            //return new LeadsNewFragment(position);
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    fragment=LeadsNewFragment.newInstance(position);
                    break;
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    fragment=LeadsActiveFragment.newInstance(position);
                    break;

                case 2: // Fragment # 1 - This will show SecondFragment
                    fragment=LeadsClosedFragment.newInstance(position);
                    break;

                default:
                    fragment=LeadsNewFragment.newInstance(position);
                    break;
            }
            return fragment;
        }


        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return arTypes[position];
        }


    }
*/
    private void getLeadsDataAPI(String leadsType, String filter, String searchText) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();

        LeadsRequestModel request = new LeadsRequestModel();
        request.setFilter(filter);
        request.setSearch(searchText);
        request.setType(leadsType);
        Call<LeadsResponse> call = ApiExecutor.getApiService(mContext).getLeadsApi("Bearer " + appSession.getAuthToken(), request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<LeadsResponse>() {
                         @Override
                         public void onResponse(Call<LeadsResponse> call, Response<LeadsResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Dashboard Data " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     if (response.body().getLeadsData() != null) {
                                         ObjData = response.body().getLeadsData();
                                         if (ObjData.getObjNew().getCount().equalsIgnoreCase("0") &&
                                                 ObjData.getObjActive().getCount().equalsIgnoreCase("0") &&
                                                 ObjData.getObjClosed().getCount().equalsIgnoreCase("0")) {
                                             llEmptyLeadsFrame.setVisibility(View.VISIBLE);
                                             llLeadsFrame.setVisibility(View.GONE);
                                             //((ImageView) findViewById(R.id.ivFilter)).setVisibility(View.GONE);

                                         }
                                         else {
                                             llEmptyLeadsFrame.setVisibility(View.GONE);
                                             //((ImageView) findViewById(R.id.ivFilter)).setVisibility(View.VISIBLE);
                                             llLeadsFrame.setVisibility(View.VISIBLE);

                                             //adapter.notifyDataSetChanged();
                                             createTabIcons(ObjData.getObjNew().getCount() + "\n" + ObjData.getObjNew().getTitle(),
                                                     ObjData.getObjActive().getCount() + "\n" + ObjData.getObjActive().getTitle(),
                                                     ObjData.getObjClosed().getCount() + "\n" + ObjData.getObjClosed().getTitle());

                                         }
                                     }
                                     break;
                                 case 422:

                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                     break;
                                 case 401:
                                     error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<LeadsResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }

    private void openFilterDialog() {
        filterDialog = new BottomSheetDialog(mContext);
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_sort_leads, null);
        //filterDialog.setContentView(R.layout.dialog_sort_leads);
        filterDialog.setContentView(sheetView);
        // set the custom dialog components - text, image and button
        filterDialog.setCancelable(true);

        lvLeadsFilter = filterDialog.findViewById(R.id.lvLeadsFilter);

        leadsFilterDataResponses = new ArrayList<LeadsFilterDataResponse>();
        adapter = new LeadsFilterAdapter(mContext,
                leadsFilterDataResponses, leadType, search);
        lvLeadsFilter.setAdapter(adapter);


        getLeadsFilterData();
        filterDialog.show();

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    /*
    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void getLeadsFilterData() {
        NetworkAPI.getLeadsFilters(mContext, "Bearer " + appSession.getAuthToken(),
                new Dispatch<LeadsFilterResponse>() {

                    @Override
                    public void apiSuccess(LeadsFilterResponse body) {

                        System.out.println("HomeFragment " + "API Data " + new Gson().toJson(body));


                        if (body != null) {
                            if (body.getLeadsData() != null && body.getLeadsData().size() > 0) {

                                leadsFilterDataResponses = body.getLeadsData();
                                adapter = new LeadsFilterAdapter(mContext,
                                        leadsFilterDataResponses, leadType, search);
                                lvLeadsFilter.setAdapter(adapter);
                            } else {

                            }
                        } else {
                            CommonUtils.showToast(mContext, getString(R.string.server_not_responding));

                        }

                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {


                    }

                    @Override
                    public void error(String error) {

                        System.out.println("API Data Error : " + error);
                    }
                });


    }

    public void disableFilterDialog() {
        if (filterDialog != null) {
            filterDialog.dismiss();
        }
    }
}