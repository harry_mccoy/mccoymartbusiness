package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class PaymentMethodResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("statusCode")
    private int statusCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @SerializedName("data")
    private PaymentMethodDataResponse paymentMethodData;

    public PaymentMethodDataResponse getPaymentMethodData() {
        return paymentMethodData;
    }

    public void setPaymentMethodData(PaymentMethodDataResponse paymentMethodData) {
        this.paymentMethodData = paymentMethodData;
    }
}
