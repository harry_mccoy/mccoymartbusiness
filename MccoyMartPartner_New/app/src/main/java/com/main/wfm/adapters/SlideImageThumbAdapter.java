package com.main.wfm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.main.wfm.R;
import com.main.wfm.apiresponsemodel.ProductImagesResponse;

import java.util.List;

public class SlideImageThumbAdapter extends PagerAdapter {
    Context context;
    private List<ProductImagesResponse> productImages;
    LayoutInflater layoutInflater;


    public SlideImageThumbAdapter(Context context, List<ProductImagesResponse> productImages) {
        this.context = context;
        this.productImages = productImages;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.slider_image_item_thumb, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.ivSlidThumb);
        //imageView.setImageResource(images[position]);
        Glide.with(context)
                .load(productImages.get(position).getImage())
                .into(imageView);

        container.addView(itemView);

        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
