package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class LeadsInfoRequestBean {
    @SerializedName("type")
    private String type;

    @SerializedName("search")
    private String search;

    @SerializedName("filter")
    private String filter;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
}
