package com.main.wfm.models;

/**
 * Created on 15-05-2019.
 */
public class PDFDataBean {

    private String pdf_name;
    private String pdf_key;
    private String pdf_value;

    public String getPdf_name() {
        return pdf_name;
    }

    public void setPdf_name(String pdf_name) {
        this.pdf_name = pdf_name;
    }

    public String getPdf_key() {
        return pdf_key;
    }

    public void setPdf_key(String pdf_key) {
        this.pdf_key = pdf_key;
    }

    public String getPdf_value() {
        return pdf_value;
    }

    public void setPdf_value(String pdf_value) {
        this.pdf_value = pdf_value;
    }
}
