package com.main.wfm.adapters;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.main.wfm.R;
import com.main.wfm.activities.LeadsDetailsActivity;
import com.main.wfm.activities.LoginStepOneActivity;
import com.main.wfm.activities.LoginStepTwoActivity;
import com.main.wfm.apiresponsemodel.LeadsDataListResponse;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.utils.Permissions;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.MyViewHolder> {
    private List<LeadsDataListResponse> ObjData;
    private Context mContext;
    public LeadsAdapter(Context mContext, List<LeadsDataListResponse> ObjData) {
        this.ObjData = ObjData;
        this.mContext=mContext;
    }

    @Override
    public LeadsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_leads, parent, false);
        LeadsAdapter.MyViewHolder holder = new LeadsAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final LeadsAdapter.MyViewHolder holder, final int position) {
        holder.tvname.setText(ObjData.get(position).getName());
        holder.tvAddress.setText(ObjData.get(position).getLocation());
        holder.tvType.setText(ObjData.get(position).getCategory());
        holder.tvRequirement.setText(ObjData.get(position).getDescription());
        holder.tvPrice.setText("");
        holder.tvDate.setText(ObjData.get(position).getDate());

        holder.ivCall.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                if (Permissions.checkPermissionCall((Activity) mContext)) {
                    String uri = "tel:" + ObjData.get(position).getMobile() ;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    mContext.startActivity(intent);
                }else {
                    Permissions.requestPermissionCall((Activity) mContext);
                }
            }
        });

        holder.llLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, LeadsDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("lead_id",ObjData.get(position).getId());
                intent.putExtra("company_id",ObjData.get(position).getCompany_id());
                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return ObjData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvname,tvAddress,tvType,tvRequirement,tvPrice,tvDate;
        public ImageView ivCall;
        public LinearLayout llLeads;

        public MyViewHolder(View v) {
            super(v);
            tvname = (TextView) v.findViewById(R.id.tvName);
            tvAddress = (TextView) v.findViewById(R.id.tvAddress);
            tvType = (TextView) v.findViewById(R.id.tvType);
            tvRequirement = (TextView) v.findViewById(R.id.tvRequirement);
            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            ivCall=(ImageView)v.findViewById(R.id.ivCall);
            llLeads=v.findViewById(R.id.llLeads);

        }
    }
}