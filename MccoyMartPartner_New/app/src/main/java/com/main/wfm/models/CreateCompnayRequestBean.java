package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateCompnayRequestBean {
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("business_type")
    private String businessType;
    @SerializedName("company_type")
    private List<String> companyTypeList;
    @SerializedName("package_id")
    private String packageId;
    @SerializedName("legal_status")
    private String legalStatus;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }


    public List<String> getCompanyTypeList() {
        return companyTypeList;
    }

    public void setCompanyTypeList(List<String> companyTypeList) {
        this.companyTypeList = companyTypeList;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getLegalStatus() {
        return legalStatus;
    }

    public void setLegalStatus(String legalStatus) {
        this.legalStatus = legalStatus;
    }
}
