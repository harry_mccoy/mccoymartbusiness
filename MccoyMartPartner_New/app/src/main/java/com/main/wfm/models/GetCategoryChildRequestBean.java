package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class GetCategoryChildRequestBean {
    @SerializedName("business_type")
    private String businessType;
    @SerializedName("parent_id")
    private String parentId;
    @SerializedName("company_id")
    private String companyId;
    private String device_name;

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }
}
