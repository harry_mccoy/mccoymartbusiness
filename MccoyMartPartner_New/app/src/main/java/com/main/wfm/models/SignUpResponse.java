package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class SignUpResponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String statusMessage;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("data")
    private SignUpDataBean signUpDataBean;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public SignUpDataBean getSignUpDataBean() {
        return signUpDataBean;
    }

    public void setSignUpDataBean(SignUpDataBean signUpDataBean) {
        this.signUpDataBean = signUpDataBean;
    }
}
