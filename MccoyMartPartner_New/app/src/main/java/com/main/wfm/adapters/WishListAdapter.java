package com.main.wfm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.MyWishListActivity;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apiresponsemodel.ReviewItemResponse;
import com.main.wfm.apiresponsemodel.WishListDataResponse;
import com.main.wfm.apiresponsemodel.WishListResponse;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.MyViewHolder> {
    private List<WishListDataResponse> wishListResponses;
    private Context mContext;
    private AppSession appSession;
    public WishListAdapter(Context mContext, List<WishListDataResponse> wishListResponses) {
        this.wishListResponses = wishListResponses;
        this.mContext = mContext;
        appSession=new AppSession(this.mContext);
    }

    @Override
    public WishListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_wishlist, parent, false);
        WishListAdapter.MyViewHolder holder = new WishListAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final WishListAdapter.MyViewHolder holder, final int position) {
        holder.tvProductName.setText(wishListResponses.get(position).getName());
        holder.tvRatingAndReview.setText(wishListResponses.get(position).getTotal_rating()+" Ratings & Reviews");
        holder.tvRatingQty.setText(wishListResponses.get(position).getRating());
        holder.tvOff.setText(wishListResponses.get(position).getDiscount()+"% off");

        Picasso.with(mContext).load(wishListResponses.get(position).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(holder.ivItemImage);

        if (wishListResponses.get(position).getSpecial_price() != null) {
            holder.tvNewPrice.setVisibility(View.VISIBLE);
            holder.tvNewPrice.setText("Rs. "+wishListResponses.get(position).getSpecial_price());
        } else {
            holder.tvNewPrice.setVisibility(View.GONE);
        }
        if (wishListResponses.get(position).getPrice() != null) {
            holder.tvOldPrice.setText("Rs. "+wishListResponses.get(position).getPrice());
            if(wishListResponses.get(position).getSpecial_price()!=null&&
                    wishListResponses.get(position).getSpecial_price().length()>0){
                holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

        }

        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("product_id",wishListResponses.get(position).getProduct_id());
                intent.putExtra("product_name",wishListResponses.get(position).getName());
                mContext.startActivity(intent);
            }
        });



        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAddItemToWishListApi(wishListResponses.get(position).getProduct_id());
            }
        });

    }
    public void deleteAddItemToWishListApi(String productId) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setAction("delete");
        request.setProduct_id(productId);
        NetworkAPI.deleteAddItemToWishListApi(mContext, "Bearer " + appSession.getAuthToken(),request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.showToast(mContext,body.message);
                                ((MyWishListActivity)mContext).getWishListApi();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, mContext.getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
    @Override
    public int getItemCount() {
        return wishListResponses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvOldPrice, tvRatingAndReview, tvNewPrice, tvProductName,tvRatingQty,tvOff;

        private RatingBar rbProductRating;
        private ImageView ivItemImage,ivDelete;
        private LinearLayout llParent;


        public MyViewHolder(View v) {
            super(v);
            tvOldPrice = (TextView) v.findViewById(R.id.tvOldPrice);
            tvRatingAndReview = (TextView) v.findViewById(R.id.tvRatingAndReview);
            tvNewPrice = (TextView) v.findViewById(R.id.tvNewPrice);
            tvOff= (TextView) v.findViewById(R.id.tvOff);
            tvProductName = (TextView) v.findViewById(R.id.tvProductName);
            tvRatingQty= (TextView) v.findViewById(R.id.tvRatingQty);
            rbProductRating=v.findViewById(R.id.rbProductRating);
            ivItemImage=v.findViewById(R.id.ivItemImage);
            ivDelete=v.findViewById(R.id.ivDelete);
            llParent=v.findViewById(R.id.llParent);

        }
    }
}