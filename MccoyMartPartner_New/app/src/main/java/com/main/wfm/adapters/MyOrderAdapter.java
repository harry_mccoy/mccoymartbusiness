package com.main.wfm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.MyWishListActivity;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.apiresponsemodel.MyOrderDataItemResponse;
import com.main.wfm.apiresponsemodel.WishListDataResponse;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyViewHolder> {
    private List<MyOrderDataItemResponse> myOrderDataList;
    private Context mContext;
    private AppSession appSession;
    public MyOrderAdapter(Context mContext, List<MyOrderDataItemResponse> myOrderDataList) {
        this.myOrderDataList = myOrderDataList;
        this.mContext = mContext;
        appSession=new AppSession(this.mContext);
    }

    @Override
    public MyOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_myorder, parent, false);
        MyOrderAdapter.MyViewHolder holder = new MyOrderAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyOrderAdapter.MyViewHolder holder, final int position) {
        holder.tvProductName.setText(myOrderDataList.get(position).getName());
        if(myOrderDataList.get(position).getOrder_status()!=null){
            holder.tvDeliveryTime.setText(myOrderDataList.get(position).getOrder_status());
        }



        Picasso.with(mContext).load(myOrderDataList.get(position).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(holder.ivItemImage);




    }

    @Override
    public int getItemCount() {
        return myOrderDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDeliveryTime, tvProductName;

        private RatingBar rbProductRating;
        private ImageView ivItemImage,ivDelete;
        private LinearLayout llParent;


        public MyViewHolder(View v) {
            super(v);
            tvDeliveryTime = (TextView) v.findViewById(R.id.tvDeliveryTime);

            tvProductName = (TextView) v.findViewById(R.id.tvProductName);

            rbProductRating=v.findViewById(R.id.rbProductRating);
            ivItemImage=v.findViewById(R.id.ivItemImage);



        }
    }
}