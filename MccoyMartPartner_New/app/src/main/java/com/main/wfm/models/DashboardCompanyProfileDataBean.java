package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashboardCompanyProfileDataBean {
    @SerializedName("title")
    private String title;
    @SerializedName("status")
    private String status;

    @SerializedName("company_slug")
    private String companySlug;
    @SerializedName("business_type")
    private String businessType;

    @SerializedName("profile")
    private List<DashboardCPItemDataBean> dashboardCPItemDataBeanList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompanySlug() {
        return companySlug;
    }

    public void setCompanySlug(String companySlug) {
        this.companySlug = companySlug;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public List<DashboardCPItemDataBean> getDashboardCPItemDataBeanList() {
        return dashboardCPItemDataBeanList;
    }

    public void setDashboardCPItemDataBeanList(List<DashboardCPItemDataBean> dashboardCPItemDataBeanList) {
        this.dashboardCPItemDataBeanList = dashboardCPItemDataBeanList;
    }
}
