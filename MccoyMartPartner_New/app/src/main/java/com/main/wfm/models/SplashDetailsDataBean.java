package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class SplashDetailsDataBean {
    @SerializedName("ObjForceUpdate")
    private ForceupdateDataBean forceupdateDataBean;

    public ForceupdateDataBean getForceupdateDataBean() {
        return forceupdateDataBean;
    }

    public void setForceupdateDataBean(ForceupdateDataBean forceupdateDataBean) {
        this.forceupdateDataBean = forceupdateDataBean;
    }
}
