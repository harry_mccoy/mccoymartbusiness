package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;
import com.main.wfm.models.ProductDataDashboardShopBean;

import java.util.List;

public class ProductDataCommonResponse {
    @SerializedName("total_count")
    private String total_count;


    @SerializedName("productData")
    private List<ProductDataDashboardShopBean> productDataDashboardShopBeans;

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }


    public List<ProductDataDashboardShopBean> getProductDataDashboardShopBeans() {
        return productDataDashboardShopBeans;
    }

    public void setProductDataDashboardShopBeans(List<ProductDataDashboardShopBean> productDataDashboardShopBeans) {
        this.productDataDashboardShopBeans = productDataDashboardShopBeans;
    }
}
