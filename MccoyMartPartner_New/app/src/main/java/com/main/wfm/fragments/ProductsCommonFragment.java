package com.main.wfm.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.adapters.ProductsAdapter;
import com.main.wfm.models.DashBoardShopRequestBean;
import com.main.wfm.models.DashboardDataShopResponse;
import com.main.wfm.models.DashboardDetailsShopResponse;
import com.main.wfm.models.GetProductOfferRequestBean;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.models.ProductInfoResponseBean;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.LoadOfferProductEvent;
import com.main.wfm.utils.ViewDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsCommonFragment extends Fragment {
    // Store instance variables


    private List<ProductDataDashboardShopBean> arlProductDatas;
    private String title;
    private int page;
    private GridView simpleGridView;
    private ProductsAdapter adapter;
    private Context mContext;
    private RecyclerView rvDoctorsGrid;
    private List<ProductDataDashboardShopBean> dashboardDetailsDataBean;
    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopBestSellingBeans;


    public ProductsCommonFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        EventBus.getDefault().register(this);


    }

    // newInstance constructor for creating fragment with arguments
    public static ProductsCommonFragment newInstance(int page, String title) {
        ProductsCommonFragment fragmentFirst = new ProductsCommonFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);

        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");

    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prducts, container, false);
        simpleGridView = view.findViewById(R.id.simpleGridView);
        rvDoctorsGrid = (RecyclerView) view.findViewById(R.id.rvDoctorsGrid);

        arlProductDatas = new ArrayList<ProductDataDashboardShopBean>();
        adapter = new ProductsAdapter(mContext, arlProductDatas);
        simpleGridView.setAdapter(adapter);
        setGridViewHeightBasedOnChildren(simpleGridView, 2);
        System.out.println("Title: " + title);

      /*  int numberOfColumns = 2;
        rvDoctorsGrid.setLayoutManager(new GridLayoutManager(mContext, numberOfColumns));
        adapter=new ProductsAdapter(mContext,arlProductDatas);
        rvDoctorsGrid.setAdapter(adapter);
        //doctorId=arlDoctorId.get(0).getId();
        adapter.notifyDataSetChanged();*/

        return view;
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }

    public static void  loadDataFromApi(String data) {
        System.out.println("Title: " + data);

        //dashboardProductsAPI(data.replace(" ","").replace("%",""));

    }

    private void dashboardProductsAPI(String productType) {
        //final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        //viewDialog.showDialog();
        GetProductOfferRequestBean request = new GetProductOfferRequestBean();


        request.setModule_type(productType);
        request.setSource("2");

        Call<ProductInfoResponseBean> call = ApiExecutor.getApiService(mContext).getProductOffersApi(request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<ProductInfoResponseBean>() {
                         @Override
                         public void onResponse(Call<ProductInfoResponseBean> call, Response<ProductInfoResponseBean> response) {
                             //viewDialog.hideDialog();
                             System.out.println("Dashboard Data " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     arlProductDataDashboardShopBestSellingBeans = new ArrayList<ProductDataDashboardShopBean>();
                                     arlProductDatas = response.body().getProductData();
                                     //adapter.notifyDataSetChanged();
                                     adapter = new ProductsAdapter(mContext, arlProductDatas);
                                     simpleGridView.setAdapter(adapter);
                                     setGridViewHeightBasedOnChildren(simpleGridView, 2);
                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     //CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                     break;
                                 case 401:
                                     error = ErrorUtils.parseError(response);
                                     //CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<ProductInfoResponseBean> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             //viewDialog.hideDialog();
                         }
                     }
        );

    }

    @Subscribe
    public void getMessage(LoadOfferProductEvent events) {

        String strMsg = events.getMessage();
        if (strMsg.equalsIgnoreCase("Best Selling")) {
            //Toast.makeText(getActivity(),strMsg,Toast.LENGTH_SHORT).show();
            dashboardProductsAPI("bestseller");
        } else {
            //Toast.makeText(getActivity(),strMsg.replace(" ","").replace("%","").toLowerCase(),Toast.LENGTH_SHORT).show();
            dashboardProductsAPI(strMsg.replace(" ", "").replace("%", "").toLowerCase());
        }

    }


}
