package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCategoryChildInfoDataBean {

    @SerializedName("categories")
    private List<CategoryChildDataBean> categoryChildDataBeanList;

    @SerializedName("registered")
    private List<CategoryChildDataBean> selectedChildDataBeanList;


    public List<CategoryChildDataBean> getCategoryChildDataBeanList() {
        return categoryChildDataBeanList;
    }

    public void setCategoryChildDataBeanList(List<CategoryChildDataBean> categoryChildDataBeanList) {
        this.categoryChildDataBeanList = categoryChildDataBeanList;
    }

    public List<CategoryChildDataBean> getSelectedChildDataBeanList() {
        return selectedChildDataBeanList;
    }

    public void setSelectedChildDataBeanList(List<CategoryChildDataBean> selectedChildDataBeanList) {
        this.selectedChildDataBeanList = selectedChildDataBeanList;
    }
}
