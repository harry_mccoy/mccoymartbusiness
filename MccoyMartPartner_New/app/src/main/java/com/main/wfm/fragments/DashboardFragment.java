package com.main.wfm.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.LoginStepOneActivity;
import com.main.wfm.activities.LoginStepTwoActivity;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.activities.ProductsCommonActivity;
import com.main.wfm.adapters.HorizontalAdapter;
import com.main.wfm.adapters.TodayOfferAdapter;
import com.main.wfm.models.DashBoardShopRequestBean;
import com.main.wfm.models.DashboardDataShopResponse;
import com.main.wfm.models.DashboardDetailsShopResponse;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.retrofitApi.ApiExecutor;
import com.main.wfm.retrofitApi.ErrorUtils;
import com.main.wfm.retrofitApi.JsonError;
import com.main.wfm.utils.AppConstants;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.GlobalBus;
import com.main.wfm.utils.LoadOfferProductEvent;
import com.main.wfm.utils.ViewDialog;
import com.main.wfm.utils.WrapContentHeightViewPager;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment implements View.OnClickListener {
    private Context mContext;
    private FragmentPagerAdapter adapterViewPager;
    private WrapContentHeightViewPager vpPager;
    private TabLayout tabLayout;
    private TextView tvViewAllTodayProduct,tvNewProductsViewAll,tvTopBrandsViewAll;

    private ImageView ivTopBrand0,ivTopBrand1,ivTopBrand2,ivTopBrand3,ivTopBrand4,ivTopBrand5;
    private ImageView ivNewLaunch1,ivNewLaunch2,ivNewLaunch3,ivNewLaunch4;
    private TextView tvNewLaunch1,tvNewLaunch2,tvNewLaunch3,tvNewLaunch4;
    private TextView tvOldPrice1,tvOldPrice2,tvOldPrice3,tvOldPrice4;
    private TextView tvNewPrice1,tvNewPrice2,tvNewPrice3,tvNewPrice4;
    private TextView tvBrandName1,tvBrandName2,tvBrandName3,tvBrandName4;

    private LinearLayout llNewLaunch1,llNewLaunch2,llNewLaunch3,llNewLaunch4;
    private SliderLayout sliderLayout;
    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopBeans;
    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopNewLaunchBeans;
    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopCarouselBeans;
    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopBannerBeans;
    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopFeaturedCatBeans;
        private List<ProductDataDashboardShopBean> arlProductDataDashboardShopTodayDealBeans;
    private List<DashboardDataShopResponse> dashboardDetailsDataBean;

    private String arProductCategory[]={"Best Selling","Under 199","Upto 80% off","Upto 60% off","Upto 50% off"};
    private RecyclerView cardView,cardViewToday;
    private ImageView ivCarousel,ivBanner;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        getIds(root);
        return root;
    }

    private void getIds(View root) {

        sliderLayout = root.findViewById(R.id.sliderLayout);
        //sliderLayoutCarosoule = root.findViewById(R.id.sliderLayoutCarosoule);

        ivCarousel=root.findViewById(R.id.ivCarousel);
        ivBanner=root.findViewById(R.id.ivBanner);
        cardView=root.findViewById(R.id.cardView);
        cardViewToday=root.findViewById(R.id.cardViewToday);
        ivTopBrand0=root.findViewById(R.id.ivTopBrand0);
        ivTopBrand1=root.findViewById(R.id.ivTopBrand1);
        ivTopBrand2=root.findViewById(R.id.ivTopBrand2);
        ivTopBrand3=root.findViewById(R.id.ivTopBrand3);
        ivTopBrand4=root.findViewById(R.id.ivTopBrand4);
        ivTopBrand5=root.findViewById(R.id.ivTopBrand5);
        tvNewLaunch1=root.findViewById(R.id.tvNewLaunch1);
        tvNewLaunch2=root.findViewById(R.id.tvNewLaunch2);
        tvNewLaunch3=root.findViewById(R.id.tvNewLaunch3);
        tvNewLaunch4=root.findViewById(R.id.tvNewLaunch4);

        tvBrandName1=root.findViewById(R.id.tvBrandName1);
        tvBrandName2=root.findViewById(R.id.tvBrandName2);
        tvBrandName3=root.findViewById(R.id.tvBrandName3);
        tvBrandName4=root.findViewById(R.id.tvBrandName4);

        tvOldPrice1=root.findViewById(R.id.tvOldPrice1);
        tvOldPrice2=root.findViewById(R.id.tvOldPrice2);
        tvOldPrice3=root.findViewById(R.id.tvOldPrice3);
        tvOldPrice4=root.findViewById(R.id.tvOldPrice4);

        tvNewPrice1=root.findViewById(R.id.tvNewPrice1);
        tvNewPrice2=root.findViewById(R.id.tvNewPrice2);
        tvNewPrice3=root.findViewById(R.id.tvNewPrice3);
        tvNewPrice4=root.findViewById(R.id.tvNewPrice4);

        ivNewLaunch1=root.findViewById(R.id.ivNewLaunch1);
        ivNewLaunch2=root.findViewById(R.id.ivNewLaunch2);
        ivNewLaunch3=root.findViewById(R.id.ivNewLaunch3);
        ivNewLaunch4=root.findViewById(R.id.ivNewLaunch4);

        llNewLaunch1=root.findViewById(R.id.llNewLaunch1);
        llNewLaunch2=root.findViewById(R.id.llNewLaunch2);
        llNewLaunch3=root.findViewById(R.id.llNewLaunch3);
        llNewLaunch4=root.findViewById(R.id.llNewLaunch4);

        llNewLaunch1.setOnClickListener(this);
        llNewLaunch2.setOnClickListener(this);
        llNewLaunch3.setOnClickListener(this);
        llNewLaunch4.setOnClickListener(this);

        tvViewAllTodayProduct=root.findViewById(R.id.tvViewAllTodayProduct);
        tvNewProductsViewAll =root.findViewById(R.id.tvNewProductsViewAll);
        tvTopBrandsViewAll=root.findViewById(R.id.tvTopBrandsViewAll);

        ((TextView)root.findViewById(R.id.tvShopCatTitle)).setTypeface(CommonUtils.setCustomFont( mContext, AppConstants.REGULAR_FONT));
        tvNewProductsViewAll.setOnClickListener(this);
        tvViewAllTodayProduct.setOnClickListener(this);
        tvTopBrandsViewAll.setOnClickListener(this);

        tabLayout = (TabLayout)root.findViewById(R.id.tabLayout);
        vpPager = (WrapContentHeightViewPager) root.findViewById(R.id.vpPager);
        //adapterViewPager = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        //vpPager.setAdapter(adapterViewPager);
        setViewPager();
        tabLayout.setupWithViewPager(vpPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        //createTabIcons();

        //tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        //tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#ffffff"));
        dashboardDetailAPI();
        //setSliderImage(sliderLayout);
        //setShopByCategoryList();
        //setTodayDeal();

    }
    private void createTabIcons() {

        //TextView tabOne = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabLayout.getTabAt(0).setText("Dashboard");
        tabLayout.getTabAt(1).setText("Report");
        tabLayout.getTabAt(2).setText("Campaign");
        //tabLayout.getTabAt(0).setCustomView(tabOne);

        /*TextView tabThree = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabThree.setText("Campaign");
        tabLayout.getTabAt(1).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabFour.setText("Reports");
        tabLayout.getTabAt(2).setCustomView(tabFour);*/
    }
    private void setViewPager() {
        vpPager.setAdapter(new MyPagerAdapter( getChildFragmentManager()));

        vpPager.setOffscreenPageLimit(1);

        /*After setting the adapter use the timer */

        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //System.out.println("Title: "+arProductCategory[position]+" Page: "+position);

                //ProductsCommonFragment obProductsCommonFragment = null;

                LoadOfferProductEvent loadOfferProductEvent=new LoadOfferProductEvent(arProductCategory[position]);
                GlobalBus.getBus().post(loadOfferProductEvent);

            }

            @Override
            public void onPageSelected(int position) {
                ProductsCommonFragment.loadDataFromApi(arProductCategory[position]);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.llNewLaunch1:
                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("product_id",arlProductDataDashboardShopNewLaunchBeans.get(0).getProduct_id());
                intent.putExtra("product_name",arlProductDataDashboardShopNewLaunchBeans.get(0).getManufacturer());
                startActivity(intent);
                break;
            case R.id.llNewLaunch2:
                intent = new Intent(mContext, ProductDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("product_id",arlProductDataDashboardShopNewLaunchBeans.get(1).getProduct_id());
                intent.putExtra("product_name",arlProductDataDashboardShopNewLaunchBeans.get(1).getManufacturer());
                startActivity(intent);
                break;
            case R.id.llNewLaunch3:
                intent = new Intent(mContext, ProductDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("product_id",arlProductDataDashboardShopNewLaunchBeans.get(2).getProduct_id());
                intent.putExtra("product_name",arlProductDataDashboardShopNewLaunchBeans.get(2).getManufacturer());
                startActivity(intent);
                break;
            case R.id.llNewLaunch4:
                intent = new Intent(mContext, ProductDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("product_id",arlProductDataDashboardShopNewLaunchBeans.get(3).getProduct_id());
                intent.putExtra("product_name",arlProductDataDashboardShopNewLaunchBeans.get(3).getManufacturer());
                startActivity(intent);
                break;
            case R.id.tvViewAllTodayProduct:
                intent = new Intent(mContext, ProductsCommonActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("module_type","b2btodaydeal");
                startActivity(intent);
                break;

            case R.id.tvNewProductsViewAll:
                intent = new Intent(mContext, ProductsCommonActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("module_type","latestproduct");
                startActivity(intent);
                break;
            case R.id.tvTopBrandsViewAll:
                intent = new Intent(mContext, ProductsCommonActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("module_type","topBrands");
                startActivity(intent);
                break;


        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter

                (FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return ProductsCommonFragment.newInstance(0,arProductCategory[0]);
                case 1: return ProductsCommonFragment.newInstance(1,arProductCategory[1]);
                case 2: return ProductsCommonFragment.newInstance(2, arProductCategory[2]);
                case 3: return ProductsCommonFragment.newInstance(3, arProductCategory[3]);
                case 4: return ProductsCommonFragment.newInstance(4, arProductCategory[4]);


                default: return PageOneFragment.newInstance();
            }
        }
        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return arProductCategory[position];
        }
        @Override
        public int getCount() {
            return 5;
        }
    }
    private void setTodayDeal(List<ProductDataDashboardShopBean> arlProductDataDashboardShopTodayDealBeans) {

        TodayOfferAdapter adapter=new TodayOfferAdapter(mContext,arlProductDataDashboardShopTodayDealBeans);

        //llHorizontalListView.setAdapter(adapter);


        cardViewToday.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        if (arlProductDataDashboardShopFeaturedCatBeans.size() > 0 & cardViewToday != null) {
            cardViewToday.setAdapter(adapter);
        }
        cardViewToday.setLayoutManager(MyLayoutManager);

    }

    private void setShopByCategoryList(List<ProductDataDashboardShopBean> arlProductDataDashboardShopFeaturedCatBeans) {

        HorizontalAdapter adapter=new HorizontalAdapter(mContext,arlProductDataDashboardShopFeaturedCatBeans);
        //llHorizontalListView.setAdapter(adapter);


        cardView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        if (arlProductDataDashboardShopFeaturedCatBeans.size() > 0 & cardView != null) {
            cardView.setAdapter(adapter);
        }
        cardView.setLayoutManager(MyLayoutManager);

    }

    private void setSliderImage(SliderLayout sliderLayout, List<ProductDataDashboardShopBean> arlProductDataDashboardShopBeans) {

        HashMap sliderImages = new HashMap<>();
        for (int i = 0; i < arlProductDataDashboardShopBeans.size(); i++) {
            sliderImages.put(arlProductDataDashboardShopBeans.get(i).getName(), arlProductDataDashboardShopBeans.get(i).getImage());
        }
        for (int i = 0; i < arlProductDataDashboardShopBeans.size(); i++) {

            DefaultSliderView textSliderView = new DefaultSliderView(mContext);
            textSliderView
                    //.description("")
                    .image(arlProductDataDashboardShopBeans.get(i).getImage())
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.getPagerIndicator().setDefaultIndicatorSize(30, 30, null);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.getPagerIndicator().setDefaultIndicatorColor(mContext.getResources().getColor(R.color.blue),
                mContext.getResources().getColor(R.color.white));

        sliderLayout.startAutoCycle(2, 2000, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        EventBus.getDefault().register(this);
        //GlobalBus.getBus().register(this);

        //actionCallback=(ProductLoadInterface) getActivity();
    }
    @Subscribe
    public void getMessage(LoadOfferProductEvent events) {


        //Toast.makeText(getActivity(),events.getMessage(),Toast.LENGTH_SHORT).show();
    }
    private void dashboardDetailAPI() {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        DashBoardShopRequestBean request = new DashBoardShopRequestBean();
        String[] arRequestConstant = {"latest", "slideshow", "featured", "featuredcategory", "carousel", "banner", "bestseller"};
        List<String> arlRequestConstant = new ArrayList<String>();
        for (int i = 0; i < arRequestConstant.length; i++) {
            arlRequestConstant.add(arRequestConstant[i]);
        }

        request.setModulesTypeList(arlRequestConstant);
        request.setSource("2");

        Call<DashboardDetailsShopResponse> call = ApiExecutor.getApiService(mContext).dashBoardShopApi(request);
        System.out.println("API url ---" + call.request().url());
        System.out.println("API request  ---" + new Gson().toJson(request));

        call.enqueue(new Callback<DashboardDetailsShopResponse>() {
                         @Override
                         public void onResponse(Call<DashboardDetailsShopResponse> call, Response<DashboardDetailsShopResponse> response) {
                             viewDialog.hideDialog();
                             System.out.println("Dashboard Data " + "API Data" + new Gson().toJson(response.body()));
                             switch (response.code()) {
                                 case 200:
                                     arlProductDataDashboardShopBeans = new ArrayList<ProductDataDashboardShopBean>();
                                     arlProductDataDashboardShopCarouselBeans = new ArrayList<ProductDataDashboardShopBean>();
                                     arlProductDataDashboardShopBannerBeans=new ArrayList<ProductDataDashboardShopBean>();
                                     arlProductDataDashboardShopFeaturedCatBeans = new ArrayList<ProductDataDashboardShopBean>();
                                     arlProductDataDashboardShopTodayDealBeans= new ArrayList<ProductDataDashboardShopBean>();
                                     arlProductDataDashboardShopNewLaunchBeans= new ArrayList<ProductDataDashboardShopBean>();

                                     dashboardDetailsDataBean = new ArrayList<DashboardDataShopResponse>();
                                     dashboardDetailsDataBean = response.body().getDashboardDetailsDataBean();

                                     for (int i = 0; i < dashboardDetailsDataBean.size(); i++) {
                                         if (dashboardDetailsDataBean.get(i).getCode().equalsIgnoreCase("latest")) {
                                             arlProductDataDashboardShopNewLaunchBeans = dashboardDetailsDataBean.get(i).getProductData();
                                         }


                                         if (dashboardDetailsDataBean.get(i).getCode().equalsIgnoreCase("slideshow")) {
                                             arlProductDataDashboardShopBeans = dashboardDetailsDataBean.get(i).getProductData();
                                         }
                                         if (dashboardDetailsDataBean.get(i).getCode().equalsIgnoreCase("carousel")) {
                                             arlProductDataDashboardShopCarouselBeans = dashboardDetailsDataBean.get(i).getProductData();
                                         }

                                         if (dashboardDetailsDataBean.get(i).getCode().equalsIgnoreCase("featuredcategory")) {
                                             arlProductDataDashboardShopFeaturedCatBeans = dashboardDetailsDataBean.get(i).getProductData();
                                         }
                                         if (dashboardDetailsDataBean.get(i).getCode().equalsIgnoreCase("banner")) {
                                             arlProductDataDashboardShopBannerBeans = dashboardDetailsDataBean.get(i).getProductData();
                                         }
                                         if (dashboardDetailsDataBean.get(i).getCode().equalsIgnoreCase("featured")) {
                                             arlProductDataDashboardShopTodayDealBeans = dashboardDetailsDataBean.get(i).getProductData();
                                         }

                                     }
                                     //setCarouselSlider(sliderLayoutCarosoule, arlProductDataDashboardShopCarouselBeans);
                                     Picasso.with(mContext).load(arlProductDataDashboardShopBannerBeans.get(1).getImage())
                                             .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivCarousel);

                                     Picasso.with(mContext).load(arlProductDataDashboardShopBannerBeans.get(0).getImage())
                                             .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivBanner);

                                     setSliderImage(sliderLayout, arlProductDataDashboardShopBeans);
                                     setShopByCategoryList(arlProductDataDashboardShopFeaturedCatBeans);
                                     setTodayDeal(arlProductDataDashboardShopTodayDealBeans);
                                     setTopBrands(arlProductDataDashboardShopCarouselBeans);
                                     setNewLaunches(arlProductDataDashboardShopNewLaunchBeans);
                                     break;
                                 case 422:
                                     JsonError error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                     break;
                                 case 401:
                                     error = ErrorUtils.parseError(response);
                                     CommonUtils.showAlertOk(error.getMessage(), (Activity) mContext);
                                     break;
                             }
                         }

                         @Override
                         public void onFailure(Call<DashboardDetailsShopResponse> call, Throwable t) {
                             System.out.println("API Data Error : " + t.getMessage());
                             viewDialog.hideDialog();
                         }
                     }
        );

    }

    private void setNewLaunches(List<ProductDataDashboardShopBean> arlProductDataDashboardShopNewLaunchBeans) {

        //tvNewLaunch1.setCompoundDrawablesWithIntrinsicBounds(0,this.getResources(). getDrawable(convertUrlToDrawable(arlProductDataDashboardShopNewLaunchBeans.get(0).getImage())), 0, 0);
        Picasso.with(mContext).load(arlProductDataDashboardShopNewLaunchBeans.get(0).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivNewLaunch1);
        tvNewLaunch1.setText(arlProductDataDashboardShopNewLaunchBeans.get(0).getName());
        tvBrandName1.setText(arlProductDataDashboardShopNewLaunchBeans.get(0).getManufacturer());
        if(arlProductDataDashboardShopNewLaunchBeans.get(0).getPrice()!=null&&arlProductDataDashboardShopNewLaunchBeans.get(0).getSpecial()!=null){
            tvNewPrice1.setVisibility(View.VISIBLE);
          tvOldPrice1.setVisibility(View.VISIBLE);
           tvOldPrice1.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(0).getPrice());
            tvOldPrice1.setPaintFlags(tvOldPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvNewPrice1.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(0).getSpecial());
        }else if(arlProductDataDashboardShopNewLaunchBeans.get(0).getPrice()!=null){
            tvOldPrice1.setVisibility(View.VISIBLE);
            tvOldPrice1.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(0).getPrice());

        }else if(arlProductDataDashboardShopNewLaunchBeans.get(0).getSpecial()!=null){
            tvNewPrice1.setVisibility(View.VISIBLE);
            tvNewPrice1.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(0).getSpecial());
        }
        else {
            tvNewPrice1.setVisibility(View.GONE);
            tvOldPrice1.setVisibility(View.GONE);
        }


        Picasso.with(mContext).load(arlProductDataDashboardShopNewLaunchBeans.get(1).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivNewLaunch2);
        tvNewLaunch2.setText(arlProductDataDashboardShopNewLaunchBeans.get(1).getName());
        tvBrandName2.setText(arlProductDataDashboardShopNewLaunchBeans.get(1).getManufacturer());
        if(arlProductDataDashboardShopNewLaunchBeans.get(1).getPrice()!=null&&arlProductDataDashboardShopNewLaunchBeans.get(1).getSpecial()!=null){
            tvNewPrice2.setVisibility(View.VISIBLE);
            tvOldPrice2.setVisibility(View.VISIBLE);
            tvOldPrice2.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(1).getPrice());
            tvOldPrice2.setPaintFlags(tvOldPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvNewPrice2.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(1).getSpecial());
        }else if(arlProductDataDashboardShopNewLaunchBeans.get(1).getPrice()!=null){
            tvOldPrice2.setVisibility(View.VISIBLE);
            tvOldPrice2.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(1).getPrice());

        }else if(arlProductDataDashboardShopNewLaunchBeans.get(1).getSpecial()!=null){
            tvNewPrice2.setVisibility(View.VISIBLE);
            tvNewPrice2.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(1).getSpecial());
        }
        else {
            tvNewPrice2.setVisibility(View.GONE);
            tvOldPrice2.setVisibility(View.GONE);
        }


        Picasso.with(mContext).load(arlProductDataDashboardShopNewLaunchBeans.get(2).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivNewLaunch3);
        tvNewLaunch3.setText(arlProductDataDashboardShopNewLaunchBeans.get(2).getName());
        tvBrandName3.setText(arlProductDataDashboardShopNewLaunchBeans.get(2).getManufacturer());
        if(arlProductDataDashboardShopNewLaunchBeans.get(2).getPrice()!=null&&arlProductDataDashboardShopNewLaunchBeans.get(2).getSpecial()!=null){
            tvNewPrice3.setVisibility(View.VISIBLE);
            tvOldPrice3.setVisibility(View.VISIBLE);
            tvOldPrice3.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(2).getPrice());
            tvOldPrice3.setPaintFlags(tvOldPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvNewPrice3.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(2).getSpecial());
        }else if(arlProductDataDashboardShopNewLaunchBeans.get(2).getPrice()!=null){
            tvOldPrice3.setVisibility(View.VISIBLE);
            tvOldPrice3.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(2).getPrice());

        }else if(arlProductDataDashboardShopNewLaunchBeans.get(2).getSpecial()!=null){
            tvNewPrice3.setVisibility(View.VISIBLE);
            tvNewPrice3.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(2).getSpecial());
        }
        else {
            tvNewPrice3.setVisibility(View.GONE);
            tvOldPrice3.setVisibility(View.GONE);
        }



        Picasso.with(mContext).load(arlProductDataDashboardShopNewLaunchBeans.get(3).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivNewLaunch4);
        tvNewLaunch4.setText(arlProductDataDashboardShopNewLaunchBeans.get(3).getName());
        tvBrandName4.setText(arlProductDataDashboardShopNewLaunchBeans.get(3).getManufacturer());

        if(arlProductDataDashboardShopNewLaunchBeans.get(3).getPrice()!=null&&arlProductDataDashboardShopNewLaunchBeans.get(3).getSpecial()!=null){
            tvNewPrice4.setVisibility(View.VISIBLE);
            tvOldPrice4.setVisibility(View.VISIBLE);
            tvOldPrice4.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(3).getPrice());
            tvOldPrice4.setPaintFlags(tvOldPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvNewPrice4.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(3).getSpecial());
        }else if(arlProductDataDashboardShopNewLaunchBeans.get(3).getPrice()!=null){
            tvOldPrice4.setVisibility(View.VISIBLE);
            tvOldPrice4.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(3).getPrice());

        }else if(arlProductDataDashboardShopNewLaunchBeans.get(3).getSpecial()!=null){
            tvNewPrice4.setVisibility(View.VISIBLE);
            tvNewPrice4.setText("Rs. "+arlProductDataDashboardShopNewLaunchBeans.get(3).getSpecial());
        }
        else {
            tvNewPrice4.setVisibility(View.GONE);
            tvOldPrice4.setVisibility(View.GONE);
        }

    }

    private void setTopBrands(List<ProductDataDashboardShopBean> arlProductDataDashboardShopCarouselBeans) {

        if(arlProductDataDashboardShopCarouselBeans!=null &&arlProductDataDashboardShopCarouselBeans.size()>0){
            if(arlProductDataDashboardShopCarouselBeans.get(0).getImage()!=null&&arlProductDataDashboardShopCarouselBeans.get(0).getImage().length()>0){
                Picasso.with(mContext).load(arlProductDataDashboardShopCarouselBeans.get(0).getImage())
                        .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivTopBrand0);
            }

            if(arlProductDataDashboardShopCarouselBeans.get(1).getImage()!=null&&arlProductDataDashboardShopCarouselBeans.get(1).getImage().length()>0){
                Picasso.with(mContext).load(arlProductDataDashboardShopCarouselBeans.get(1).getImage())
                        .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivTopBrand1);
            }
            if(arlProductDataDashboardShopCarouselBeans.get(2).getImage()!=null&&arlProductDataDashboardShopCarouselBeans.get(2).getImage().length()>0){
                Picasso.with(mContext).load(arlProductDataDashboardShopCarouselBeans.get(2).getImage())
                        .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivTopBrand2);
            }

            if(arlProductDataDashboardShopCarouselBeans.get(3).getImage()!=null&&arlProductDataDashboardShopCarouselBeans.get(3).getImage().length()>0){
                Picasso.with(mContext).load(arlProductDataDashboardShopCarouselBeans.get(3).getImage())
                        .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivTopBrand3);
            }
        }
        if(arlProductDataDashboardShopCarouselBeans.get(4).getImage()!=null&&arlProductDataDashboardShopCarouselBeans.get(4).getImage().length()>0){
            Picasso.with(mContext).load(arlProductDataDashboardShopCarouselBeans.get(4).getImage())
                    .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivTopBrand4);
        }

        if(arlProductDataDashboardShopCarouselBeans.get(5).getImage()!=null&&arlProductDataDashboardShopCarouselBeans.get(5).getImage().length()>0){
            Picasso.with(mContext).load(arlProductDataDashboardShopCarouselBeans.get(5).getImage())
                    .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(ivTopBrand5);
        }




    }

    @Override
    public void onStop() {
        //sliderLayout.stopAutoCycle();
        //sliderLayoutCarosoule.stopAutoCycle();
        super.onStop();

    }
}