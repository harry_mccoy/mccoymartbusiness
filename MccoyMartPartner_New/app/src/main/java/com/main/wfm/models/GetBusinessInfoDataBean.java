package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetBusinessInfoDataBean {
    @SerializedName("objCompanyData")
    private UpdateBusinessInfoResponseBean alreadyFilledData;
    @SerializedName("objNoEmployee")
    private List<String> noOfEmployeeList;
    @SerializedName("objTurnOver")
    private List<String> turnOverList;
    @SerializedName("ObjAreas")
    private List<ObjAreaDataBean> serveAreaList;
    private List<String> youTube;
    private List<PDFDataBean> objPDF;
    private String objNextPDFKey;


    public UpdateBusinessInfoResponseBean getAlreadyFilledData() {
        return alreadyFilledData;
    }

    public void setAlreadyFilledData(UpdateBusinessInfoResponseBean alreadyFilledData) {
        this.alreadyFilledData = alreadyFilledData;
    }

    public List<String> getNoOfEmployeeList() {
        return noOfEmployeeList;
    }

    public void setNoOfEmployeeList(List<String> noOfEmployeeList) {
        this.noOfEmployeeList = noOfEmployeeList;
    }

    public List<String> getTurnOverList() {
        return turnOverList;
    }

    public void setTurnOverList(List<String> turnOverList) {
        this.turnOverList = turnOverList;
    }

    public List<ObjAreaDataBean> getServeAreaList() {
        return serveAreaList;
    }

    public void setServeAreaList(List<ObjAreaDataBean> serveAreaList) {
        this.serveAreaList = serveAreaList;
    }

    public List<String> getYoutube() {
        return youTube;
    }

    public void setYoutube(List<String> youtube) {
        this.youTube = youtube;
    }

    public List<PDFDataBean> getObjPDF() {
        return objPDF;
    }

    public void setObjPDF(List<PDFDataBean> objPDF) {
        this.objPDF = objPDF;
    }

    public String getObjNextPDFKey() {
        return objNextPDFKey;
    }

    public void setObjNextPDFKey(String objNextPDFKey) {
        this.objNextPDFKey = objNextPDFKey;
    }
}
