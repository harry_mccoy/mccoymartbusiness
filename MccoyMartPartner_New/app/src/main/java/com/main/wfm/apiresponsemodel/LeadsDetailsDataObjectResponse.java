package com.main.wfm.apiresponsemodel;

import com.google.gson.annotations.SerializedName;

public class LeadsDetailsDataObjectResponse {
    @SerializedName("text")
    private String text;

    @SerializedName("action")
    private String action;

    @SerializedName("value")
    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



    
}
