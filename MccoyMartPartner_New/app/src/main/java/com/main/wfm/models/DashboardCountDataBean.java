package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class DashboardCountDataBean {

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("count")
    private String count;

    @SerializedName("action")
    private String action;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
