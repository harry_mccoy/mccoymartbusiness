package com.main.wfm.adapters;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.main.wfm.R;
import com.main.wfm.activities.ProductDetailActivity;
import com.main.wfm.apimodel.Dispatch;
import com.main.wfm.apimodel.ErrorDTO;
import com.main.wfm.apimodel.NetworkAPI;
import com.main.wfm.apirequestmodel.AddDeleteWishListRequest;
import com.main.wfm.models.ProductDataDashboardShopBean;
import com.main.wfm.retrofitApi.JsonResponse;
import com.main.wfm.utils.AppSession;
import com.main.wfm.utils.CommonUtils;
import com.main.wfm.utils.ViewDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.graphics.Color.GRAY;
import static android.graphics.Color.RED;

public class TodayOfferAdapter extends RecyclerView.Adapter<TodayOfferAdapter.MyViewHolder> {

    private List<ProductDataDashboardShopBean> arlProductDataDashboardShopFeaturedCatBeans;
    private Context mContext;
    private boolean []isSelected;
    private AppSession appSession;

    public TodayOfferAdapter(Context mContext, List<ProductDataDashboardShopBean> arlProductDataDashboardShopFeaturedCatBeans) {
        this.arlProductDataDashboardShopFeaturedCatBeans = arlProductDataDashboardShopFeaturedCatBeans;
        this.mContext=mContext;
        appSession=new AppSession(this.mContext);
        isSelected=new boolean[this.arlProductDataDashboardShopFeaturedCatBeans.size()];
        for (int i=0;i<this.arlProductDataDashboardShopFeaturedCatBeans.size();i++){
            isSelected[i]=true;
        }
    }

    @Override
    public TodayOfferAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lv_todaysdeal_row, parent, false);
        TodayOfferAdapter.MyViewHolder holder = new TodayOfferAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final TodayOfferAdapter.MyViewHolder holder, final int position) {

        holder.tvBrandName.setVisibility(View.VISIBLE);

        if(arlProductDataDashboardShopFeaturedCatBeans.get(position).getPrice()!=null&&arlProductDataDashboardShopFeaturedCatBeans.get(position).getSpecial()!=null){
            holder.tvNewPrice.setVisibility(View.VISIBLE);
            holder.tvOldPrice.setVisibility(View.VISIBLE);
            holder.tvOldPrice.setText("Rs. "+arlProductDataDashboardShopFeaturedCatBeans.get(position).getPrice());
            holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvNewPrice.setText("Rs. "+arlProductDataDashboardShopFeaturedCatBeans.get(position).getSpecial());
        }else if(arlProductDataDashboardShopFeaturedCatBeans.get(position).getPrice()!=null){
            holder.tvOldPrice.setVisibility(View.VISIBLE);
            holder.tvOldPrice.setText("Rs. "+arlProductDataDashboardShopFeaturedCatBeans.get(position).getPrice());

        }else if(arlProductDataDashboardShopFeaturedCatBeans.get(position).getSpecial()!=null){
            holder.tvNewPrice.setVisibility(View.VISIBLE);
            holder.tvNewPrice.setText("Rs. "+arlProductDataDashboardShopFeaturedCatBeans.get(position).getSpecial());
        }
        else {
            holder.tvNewPrice.setVisibility(View.GONE);
            holder.tvOldPrice.setVisibility(View.GONE);
        }


        holder.tvname.setText(arlProductDataDashboardShopFeaturedCatBeans.get(position).getName());
        holder.tvBrandName.setText(arlProductDataDashboardShopFeaturedCatBeans.get(position).getManufacturer());

        //holder.iv.setImageResource(imageModelArrayList.get(position).getImage_drawable());

        Picasso.with(mContext).load(arlProductDataDashboardShopFeaturedCatBeans.get(position).getImage())
                .placeholder(mContext.getResources().getDrawable(R.drawable.upvc_hardware)).into(holder.iv);

        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("product_id",arlProductDataDashboardShopFeaturedCatBeans.get(position).getProduct_id());
                intent.putExtra("product_name",arlProductDataDashboardShopFeaturedCatBeans.get(position).getManufacturer());
                mContext.startActivity(intent);
            }
        });

        holder.ivAddToWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelected[position]){
                    addItemToWishListApi(arlProductDataDashboardShopFeaturedCatBeans.get(position).getProduct_id(),"add",holder);
                    isSelected[position]=false;
                }else{
                    addItemToWishListApi(arlProductDataDashboardShopFeaturedCatBeans.get(position).getProduct_id(),"delete",holder);
                    isSelected[position]=true;
                }

            }
        });

    }
    public void addItemToWishListApi(String productId, final String action, final MyViewHolder holder) {
        final ViewDialog viewDialog = new ViewDialog((Activity) mContext);
        viewDialog.showDialog();
        AddDeleteWishListRequest request=new AddDeleteWishListRequest();
        request.setAction(action);
        request.setProduct_id(productId);
        NetworkAPI.deleteAddItemToWishListApi(mContext, "Bearer " + appSession.getAuthToken(),request,
                new Dispatch<JsonResponse>() {
                    @Override
                    public void apiSuccess(JsonResponse body) {
                        System.out.println("Product Detail " + "API Data " + new Gson().toJson(body));
                        if (body != null) {
                            viewDialog.hideDialog();
                            if (body.status.equalsIgnoreCase("true")) {
                                CommonUtils.showToast(mContext,body.message);
                                if(action.equalsIgnoreCase("add")){
                                    holder.ivAddToWishList.setColorFilter(RED, PorterDuff.Mode.SRC_IN);
                                }else{
                                    holder.ivAddToWishList.setColorFilter(GRAY, PorterDuff.Mode.SRC_IN);
                                }

                                //DrawableCompat.setTint(holder.ivAddToWishList.getDrawable(), ContextCompat.getColor(mContext, R.color.red_google_btn));

                                notifyDataSetChanged();
                            } else {

                            }
                        } else {
                            viewDialog.hideDialog();
                            CommonUtils.showToast(mContext, mContext.getString(R.string.server_not_responding));
                        }
                    }

                    @Override
                    public void apiError(ErrorDTO errorDTO) {
                        viewDialog.hideDialog();
                    }

                    @Override
                    public void error(String error) {
                        viewDialog.hideDialog();
                        System.out.println("API Data Error : " + error);
                    }
                });

    }
    @Override
    public int getItemCount() {
        return arlProductDataDashboardShopFeaturedCatBeans.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvname,tvBrandName,tvOldPrice,tvNewPrice;
        public ImageView iv,ivAddToWishList;
        public LinearLayout llParent;

        public MyViewHolder(View v) {
            super(v);
            llParent=v.findViewById(R.id.llParent);
            tvname = (TextView) v.findViewById(R.id.name);
            iv = (ImageView) v.findViewById(R.id.imgView);
            ivAddToWishList=v.findViewById(R.id.ivAddToWishList);
            tvBrandName=v.findViewById(R.id.tvBrandName);
            tvOldPrice=v.findViewById(R.id.tvOldPrice);
            tvNewPrice=v.findViewById(R.id.tvNewPrice);


        }
    }
}