package com.main.wfm.models;

import com.google.gson.annotations.SerializedName;

public class GetProjectInfoResponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String statusMessage;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("data")
    private GetProjectInfoDataBean getBusinessInfoDataBean;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public GetProjectInfoDataBean getGetBusinessInfoDataBean() {
        return getBusinessInfoDataBean;
    }

    public void setGetBusinessInfoDataBean(GetProjectInfoDataBean getBusinessInfoDataBean) {
        this.getBusinessInfoDataBean = getBusinessInfoDataBean;
    }
}
