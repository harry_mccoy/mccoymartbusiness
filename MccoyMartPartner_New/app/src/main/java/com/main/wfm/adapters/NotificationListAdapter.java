package com.main.wfm.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.main.wfm.R;
import com.main.wfm.activities.LeadsDetailsActivity;
import com.main.wfm.apiresponsemodel.LeadsDataListResponse;
import com.main.wfm.apiresponsemodel.NotificationListDataResponse;
import com.main.wfm.utils.Permissions;

import java.util.List;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.MyViewHolder> {
    private List<NotificationListDataResponse> notificationList;
    private Context mContext;

    public NotificationListAdapter(Context mContext, List<NotificationListDataResponse> notificationList) {
        this.notificationList = notificationList;
        this.mContext = mContext;
    }

    @Override
    public NotificationListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification, parent, false);
        NotificationListAdapter.MyViewHolder holder = new NotificationListAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final NotificationListAdapter.MyViewHolder holder, final int position) {
        holder.tvNotificationTitle.setText(notificationList.get(position).getTitle());
        holder.tvNameAddress.setText(notificationList.get(position).getMessage());
        holder.tvDateTime.setText(notificationList.get(position).getDate());


        if (notificationList.get(position).getStatus().equalsIgnoreCase("0")){
            holder.tvReadStatus.setVisibility(View.VISIBLE);
        }else{
            holder.tvReadStatus.setVisibility(View.GONE);
        }
        holder.llNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notificationList.get(position).getNotification_type().equalsIgnoreCase("1")){
                    Intent intent = new Intent(mContext, LeadsDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("lead_id",notificationList.get(position).getId());
                    intent.putExtra("company_id",notificationList.get(position).getCompany_id());
                    mContext.startActivity(intent);
                }else if(notificationList.get(position).getNotification_type().equalsIgnoreCase("2")){

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNotificationTitle, tvNameAddress, tvDateTime, tvReadStatus;
        public ImageView ivNotificationImage;
        public LinearLayout llNotification;

        public MyViewHolder(View v) {
            super(v);
            tvNotificationTitle = (TextView) v.findViewById(R.id.tvNotificationTitle);
            tvNameAddress = (TextView) v.findViewById(R.id.tvNameAddress);
            tvDateTime = (TextView) v.findViewById(R.id.tvDateTime);
            tvReadStatus = (TextView) v.findViewById(R.id.tvReadStatus);

            llNotification = v.findViewById(R.id.llNotification);

        }
    }
}