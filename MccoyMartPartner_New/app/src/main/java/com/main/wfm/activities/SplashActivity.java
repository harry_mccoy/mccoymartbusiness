package com.main.wfm.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.main.wfm.R;
import com.main.wfm.utils.AppSession;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by RSShah on 07-05-2018.
 */

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_DISPLAY_LENGHT = 2000;
    ///ViewDialog viewDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        printKeyHash(SplashActivity.this);
        final AppSession appSession=new AppSession(SplashActivity.this);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        appSession.setDeviceId(android_id);
        //viewDialog = new ViewDialog(this);
        //copyClipboard();
        //viewDialog.showDialog();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if(appSession.getUserId()!=null&&appSession.getUserId().length()>0){
                    intent = new Intent(SplashActivity.this, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    if(appSession.getDemoComplete()!=null&&appSession.getDemoComplete().length()>0){
                        intent = new Intent(SplashActivity.this, LoginStepOneActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        intent = new Intent(SplashActivity.this, DemoTutorialActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }


            }

        }, SPLASH_DISPLAY_LENGHT);


    }

    private String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                System.out.print("Key Hash="+ key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    /*private void copyClipboard(){
        try {
            // Gets a handle to the clipboard service.
            ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
            // Creates a new text clip to put on the clipboard
            ClipData clip = ClipData.newPlainText("simple text", appSession.getDeviceToken());
            clipboard.setPrimaryClip(clip);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
